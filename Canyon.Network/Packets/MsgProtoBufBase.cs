﻿using Canyon.Network.Sockets;
using ProtoBuf;

namespace Canyon.Network.Packets
{
    public abstract class MsgProtoBufBase<TActor, TData> : MsgBase<TActor> where TActor : TcpServerActor
    {
        public MsgProtoBufBase(PacketType packetType)
        {
            Type = packetType;
        }

        public TData Data { get; set; }

        public override byte[] Encode()
        {
            using var writer = new PacketWriter();
            writer.Write((ushort)Type);
            Serializer.SerializeWithLengthPrefix(writer.BaseStream, Data, PrefixStyle.Fixed32);
            return writer.ToArray();
        }

        public override void Decode(byte[] bytes)
        {
            using var reader = new PacketReader(bytes);
            Length = reader.ReadUInt16();
            Type = (PacketType)reader.ReadUInt16();
            Data = Serializer.DeserializeWithLengthPrefix<TData>(reader.BaseStream, PrefixStyle.Fixed32);
        }
    }
}
