﻿namespace Canyon.Network.Packets
{
    /// <summary>
    ///     Packet types for the Conquer Online game client across all server projects.
    ///     Identifies packets by an unsigned short from offset 2 of every packet sent to
    ///     the server.
    /// </summary>
    public enum PacketType : ushort
    {
        /*
         * Piglet is Canyon GM Service!
         * Those messages generate interaction between the GM Panel in the web interface and
         * the game servers. Packets are shared between Game <-> Piglet <-> GM Panel
         */ 
        MsgPigletStart = 33000,

        MsgPigletHandshake,
        MsgPigletLogin,
        MsgPigletLoginEx,
        MsgPigletPing,
        MsgPigletUserBan,
        MsgPigletUserMail,
        MsgPigletUserMassMail,
        MsgPigletUserLogin,
        MsgPigletItemSuspicious,
        MsgPigletUserCount,
        MsgPigletRealmStatus,
        MsgPigletRealmAnnounceMaintenance,
        MsgPigletUserCreditInfo,
        MsgPigletUserCreditInfoEx,
        MsgPigletRealmUpdateCheck,
        MsgPigletClaimFirstCredit,
        MsgPigletLogOutputRedirect,
        MsgPigletShutdown,
        MsgPigletRealmAction,

        MsgPigletEnd = 33999,
        
        MsgDiscordStart = 34000,
        MsgDiscordEnd = 34999
    }
}
