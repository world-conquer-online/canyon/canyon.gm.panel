﻿using Canyon.Network.Sockets;
using ProtoBuf;

namespace Canyon.Network.Packets.Piglet
{
    public abstract class MsgPigletRealmAction<TActor> : MsgProtoBufBase<TActor, MsgPigletRealmAction<TActor>.ActionData> where TActor : TcpServerActor
    {
        public MsgPigletRealmAction()
            : base(PacketType.MsgPigletRealmAction)
        {            
        }

        [ProtoContract]
        public struct ActionData
        {
            [ProtoMember(1)]
            public int Action { get; set; }
            [ProtoIgnore]
            public ActionDataType ActionType { get => (ActionDataType)Action; set => Action = (int)value; }
            [ProtoMember(2)]
            public int Data { get; set; }
            [ProtoMember(3)]
            public int Param { get; set; }
            [ProtoMember(4)]
            public List<string> Strings { get; set; }
        }

        public enum ActionDataType
        {
            None,
            MinActionDataType = 1_000,
            StartServer
        }
    }
}
