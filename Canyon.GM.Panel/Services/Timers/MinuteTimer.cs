﻿using Quartz;

namespace Canyon.GM.Panel.Services.Timers
{
    [DisallowConcurrentExecution]
    public class MinuteTimer : IJob
    {
        private readonly MaintenanceService maintenanceService;

        public MinuteTimer(MaintenanceService maintenanceService)
        {
            this.maintenanceService = maintenanceService;
        }

        public Task Execute(IJobExecutionContext context)
        {
            return maintenanceService.OnTimerAsync();
        }
    }
}
