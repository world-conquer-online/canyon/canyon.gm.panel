﻿using Quartz;

namespace Canyon.GM.Panel.Services.Timers
{
    [DisallowConcurrentExecution]
    public sealed class SocketPingTimer : IJob
    {
        private readonly ILogger<SocketPingTimer> logger;
        private readonly RealmSocketService realmSocketService;

        private static long lastPingTick = Environment.TickCount64;

        public SocketPingTimer(
            ILogger<SocketPingTimer> logger,
            RealmSocketService realmSocketService
            )
        {
            this.logger = logger;
            this.realmSocketService = realmSocketService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            long delta = Environment.TickCount64 - lastPingTick;
            if (delta >= 10000)
            {
                await realmSocketService.PingTimerAsync();
                lastPingTick = Environment.TickCount64;
            }
        }
    }
}
