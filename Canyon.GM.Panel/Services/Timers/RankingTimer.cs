﻿using Quartz;

namespace Canyon.GM.Panel.Services.Timers
{
    [DisallowConcurrentExecution]
    public class RankingTimer : IJob
    {
        private readonly RankingService rankingService;

        public RankingTimer(RankingService rankingService)
        {
            this.rankingService = rankingService;
        }

        public Task Execute(IJobExecutionContext context)
        {
            return rankingService.SynchronizeRankingsAsync();
        }
    }
}
