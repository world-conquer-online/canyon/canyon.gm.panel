﻿using Canyon.GM.Panel.Database.Entities.Game.TradeHistory;
using Canyon.GM.Panel.Database;
using MySqlConnector;
using Microsoft.EntityFrameworkCore;

namespace Canyon.GM.Panel.Services
{
    public class TradeHistoryService
    {
        private readonly RealmService realmService;

        public TradeHistoryService(
            RealmService realmService
            )
        {
            this.realmService = realmService;
        }

        public async Task<List<TradeHistory>> GetTradeHistoryAsync(Guid realmId, uint idUser, int page)
        {
            const int ipp = 25;
            var connectionString = realmService.GetRealmConnectionString(realmId);
            if (string.IsNullOrEmpty(connectionString))
            {
                return new List<TradeHistory>();
            }

            using var connection = new RealmDbContext(connectionString);
            var idUserParam = new MySqlParameter("@idUser", idUser);
            var fromIdxParam = new MySqlParameter("@fromIdx", page * ipp);
            var ippParam = new MySqlParameter("@ipp", ipp);
            return await connection.TradeHistories
                .FromSqlRaw("call QueryTradeHistoryByUserId(@idUser, @fromIdx, @ipp)", idUserParam, fromIdxParam, ippParam)
                .ToListAsync();
        }

        public async Task<TradeHistory> GetTradeAsync(Guid realmId, uint idTrade)
        {
            var connectionString = realmService.GetRealmConnectionString(realmId);
            if (string.IsNullOrEmpty(connectionString))
            {
                return null;
            }

            using var connection = new RealmDbContext(connectionString);
            var idTradeParam = new MySqlParameter("@idTrade", idTrade);
            return connection.TradeHistories
                .FromSqlRaw("call QueryTradeById(@idTrade)", idTradeParam)
                .ToList().FirstOrDefault();
        }

        public async Task<List<TradeHistoryDetail>> GetTradeHistoryDetailsAsync(Guid realmId, uint idTrade)
        {
            var connectionString = realmService.GetRealmConnectionString(realmId);
            if (string.IsNullOrEmpty(connectionString))
            {
                return null;
            }

            using var connection = new RealmDbContext(connectionString);
            var idTradeParam = new MySqlParameter("@idTrade", idTrade);
            return await connection.TradeHistoryDetails
                .FromSqlRaw("call QueryTradeHistoryById(@idTrade)", idTradeParam)
                .ToListAsync();
        }
    }
}
