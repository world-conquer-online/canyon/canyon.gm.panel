﻿using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Views;
using Canyon.GM.Panel.Models.Ranking;
using Microsoft.EntityFrameworkCore;
using MySqlConnector;
using System.Diagnostics;

namespace Canyon.GM.Panel.Services
{
    public class RankingService
    {
        private readonly RealmService realmService;
        private readonly ApplicationDbContext context;
        private readonly ILogger logger;

        public RankingService(
            ApplicationDbContext context,
            ILogger<RankingService> logger
,
            RealmService realmService)
        {
            this.context = context;
            this.logger = logger;
            this.realmService = realmService;
        }

        #region Update Rankings

        public DateTime? QueryLastUpdateStamp()
        {
            return context.RankBrushes.OrderByDescending(x => x.Id).FirstOrDefault()?.Start;
        }

        public async Task SynchronizeRankingsAsync()
        {
            var latestBrush = context.RankBrushes.OrderByDescending(x => x.Start).FirstOrDefault();
            if (latestBrush != null
                && (DateTime.Now - latestBrush.Start).TotalHours < 11)
            {
                return;
            }

            ConquerRankBrush brush = new ConquerRankBrush
            {
                ServerId = Guid.Empty,
                Start = DateTime.Now
            };

            Stopwatch sw = Stopwatch.StartNew();

            var realms = await realmService.QueryRealmsAsync(0, 50);

            await context.Database.BeginTransactionAsync();
            await context.Database.ExecuteSqlRawAsync("TRUNCATE `conquer_rank_user`;");
            await context.Database.ExecuteSqlRawAsync("TRUNCATE `conquer_rank_syndicate`;");
            await context.Database.ExecuteSqlRawAsync("TRUNCATE `conquer_rank_family`;");

            foreach (var realm in realms)
            {
#if !DEBUG
                if (!realm.Production)
                {
                    continue;
                }
#endif

                logger.LogInformation("Realm '{}' is being processed", realm.RealmName);
                await SynchronizeRankingAsync(realm.RealmID);
            }

            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();

            brush.End = DateTime.Now;
            sw.Stop();
            brush.Ticks = sw.ElapsedTicks;
            await context.SaveAsync(brush);
        }

        private async Task SynchronizeRankingAsync(Guid serverId)
        {
            string connectionString = realmService.GetRealmConnectionString(serverId);
            var serverDbContext = new RealmDbContext(connectionString);
            try
            {
                var userData = serverDbContext.UserReports.FromSqlRaw("call QueryUserRankInformation()").ToList();
                foreach (var user in userData.DistinctBy(x => x.Id))
                {
                    ConquerRankUser rankUser = new ConquerRankUser
                    {
                        UserId = user.Id,
                        Name = user.Name,
                        Mate = user.Mate,
                        Money = user.Money,
                        StorageMoney = user.StorageMoney,
                        ConquerPoints = user.ConquerPoints,
                        ConquerPointsBound = user.ConquerPointsBound,
                        Level = user.Level,
                        Experience = user.Experience,
                        Metempsychosis = user.Metempsychosis,
                        Profession = user.Profession,
                        PreviousProfession = user.PreviousProfession,
                        FirstProfession = user.FirstProfession,
                        Strength = user.Strength,
                        Agility = user.Agility,
                        Health = user.Health,
                        Soul = user.Soul,
                        AttributePoints = user.AttributePoints,
                        Gift1 = user.Gift1,
                        Gift2 = user.Gift2,
                        Gift3 = user.Gift3,
                        Gift4 = user.Gift4,
                        SyndicateId = user.SyndicateId,
                        FamilyId = user.FamilyId,
                        PeerageDonation = user.PeerageDonation,
                        SupermanCount = user.SupermanCount,
                        RealmId = serverId
                    };
                    context.RankUsers.Add(rankUser);
                }

                var syndicateData = serverDbContext.SyndicateReports.FromSqlRaw("call QuerySyndicateRankInformation()").ToList();
                foreach (var syndicate in syndicateData.DistinctBy(x => x.Id))
                {
                    ConquerRankSyndicate rankSyndicate = new ConquerRankSyndicate
                    {
                        SyndicateId = syndicate.Id,
                        Name = syndicate.Name,
                        Money = syndicate.Money,
                        ConquerPoints = syndicate.ConquerPoints,
                        Amount = syndicate.Amount,
                        LeaderId = syndicate.LeaderId,
                        Level = syndicate.Level,
                        DelFlag = syndicate.DelFlag,
                        RealmId = serverId
                    };
                    context.RankSyndicates.Add(rankSyndicate);
                }

                var familyData = serverDbContext.FamilyReports.FromSqlRaw("call QueryFamilyRankInformation()").ToList();
                foreach (var family in familyData.DistinctBy(x => x.Id))
                {
                    ConquerRankFamily rankFamily = new ConquerRankFamily
                    {
                        FamilyId = family.Id,
                        Name = family.Name,
                        Amount = family.Amount,
                        Money = family.Money,
                        LeaderId = family.LeaderId,
                        Level = family.Level,
                        FamilyMap = family.FamilyMap,
                        FamilyMapName = family.FamilyMapName,
                        OccupyDate = family.OccupyDate,
                        StarTower = family.StarTower,
                        RealmId = serverId
                    };
                    context.RankFamilies.Add(rankFamily);
                }
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "{}", ex.Message);
            }
        }

        #endregion

        public List<GetTopPlayers> QueryTopPlayers(int minLevel, int startIdx, int ipp)
        {
            var minLevelParam = new MySqlParameter("@minLevel", minLevel);
            var startIdxParam = new MySqlParameter("@startIdx", startIdx);
            var ippParam = new MySqlParameter("@ipp", ipp);
            return context.TopPlayers.FromSqlRaw("call GetTopPlayers(@minLevel, @startIdx, @ipp)", minLevelParam, startIdxParam, ippParam).ToList();
        }

        public List<GetTopPlayers> QueryTopProfession(int minProf, int minLevel, int startIdx, int ipp)
        {
            minProf *= 10;
            int maxProf = minProf + 9;

            var minProfParam = new MySqlParameter("@startProf", minProf);
            var maxProfParam = new MySqlParameter("@endProf", maxProf);
            var minLevelParam = new MySqlParameter("@minLevel", minLevel);
            var startIdxParam = new MySqlParameter("@startIdx", startIdx);
            var ippParam = new MySqlParameter("@ipp", ipp);
            return context.TopPlayers.FromSqlRaw("call GetTopProfessionPlayers(@startProf, @endProf, @minLevel, @startIdx, @ipp)",
                minProfParam,
                maxProfParam,
                minLevelParam,
                startIdxParam,
                ippParam).ToList();
        }

        public List<GetTopSyndicate> QueryTopSyndicates(int startIdx, int ipp)
        {
            var startIdxParam = new MySqlParameter("@startIdx", startIdx);
            var ippParam = new MySqlParameter("@ipp", ipp);
            return context.TopSyndicates.FromSqlRaw("call GetTopSyndicates(@startIdx, @ipp)", startIdxParam, ippParam).ToList();
        }

        public List<GetTopFamily> QueryTopFamilies(int startIdx, int ipp)
        {
            var startIdxParam = new MySqlParameter("@startIdx", startIdx);
            var ippParam = new MySqlParameter("@ipp", ipp);
            return context.TopFamilies.FromSqlRaw("call GetTopFamilies(@startIdx, @ipp)", startIdxParam, ippParam).ToList();
        }

        public List<GetTopNoble> QueryTopNoble()
        {
            return context.TopNobles.FromSqlRaw("call GetTopNoble()").ToList();
        }

        public List<GetTopSuperman> QueryTopSuperman()
        {
            return context.TopSupermen.FromSqlRaw("call GetTopSuperman()").ToList();
        }

        public List<GetTopMoneybag> QueryTopMoneybag()
        {
            return context.TopMoneybag.FromSqlRaw("call GetTopMoneybag()").ToList();
        }
    }
}
