﻿using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Helpers;
using System.Security.Claims;

namespace Canyon.GM.Panel.Services.Logging
{
    public sealed class EventLogger
    {
        private readonly ILogger<EventLogger> logger;
        private readonly ApplicationDbContext applicationDbContext;
        private readonly ClaimsPrincipal claimsPrincipal;
        private readonly IStringLocalizer stringLocalizer;
        private readonly IHttpContextAccessor httpContextAccessor;

        public EventLogger(
            ILogger<EventLogger> logger,
            ApplicationDbContext applicationDbContext,
            ClaimsPrincipal claimsPrincipal,
            IStringLocalizer stringLocalizer,
            IHttpContextAccessor httpContextAccessor
            )
        {
            this.logger = logger;
            this.applicationDbContext = applicationDbContext;
            this.claimsPrincipal = claimsPrincipal;
            this.stringLocalizer = stringLocalizer;
            this.httpContextAccessor = httpContextAccessor;
        }

        public Task LogAsync(LogType logType, string message, ApplicationUser applicationUser, bool success = true, object data = null)
        {
            logger.LogInformation("[{logType}] {message}", logType, message);

            LogGeneral logGeneral = new LogGeneral
            {
                LogType = logType,
                AccountId = applicationUser?.Id,
                IpAddress = httpContextAccessor.HttpContext.Request.GetClientIpAddress(),
                Message = message,
                Success = success,
                Timestamp = DateTime.UtcNow
            };
            logGeneral.SetData(data);
            applicationDbContext.LogGenerals.Add(logGeneral);
            return applicationDbContext.SaveChangesAsync();
        }

        public Task LogAsync(LogType logType, string message, ClaimsPrincipal claimsPrincipal, bool success = true, object data = null)
        {
            logger.LogInformation("[{logType}] {message}", logType, message);

            LogGeneral logGeneral = new LogGeneral
            {
                LogType = logType,
                AccountId = claimsPrincipal?.GetId(),
                IpAddress = httpContextAccessor.HttpContext.Request.GetClientIpAddress(),
                Message = message,
                Success = success,
                Timestamp = DateTime.UtcNow
            };
            logGeneral.SetData(data);
            applicationDbContext.LogGenerals.Add(logGeneral);
            return applicationDbContext.SaveChangesAsync();
        }

        public Task LogAsync(LogType logType, string message, bool success = true, object data = null)
        {
            logger.LogInformation("[{logType}] {message}", logType, message);

            LogGeneral logGeneral = new LogGeneral
            {
                LogType = logType,
                AccountId = claimsPrincipal?.GetId(),
                IpAddress = httpContextAccessor.HttpContext.Request.GetClientIpAddress(),
                Message = message,
                Success = success,
                Timestamp = DateTime.UtcNow
            };
            logGeneral.SetData(data);
            applicationDbContext.LogGenerals.Add(logGeneral);
            return applicationDbContext.SaveChangesAsync();
        }

        public Task LogAsync(LogType logType, bool success = true, object data = null)
        {
            logger.LogInformation("[{logType}] {message}", logType, stringLocalizer.GetString($"LogEvent{logType}"));

            LogGeneral logGeneral = new LogGeneral
            {
                LogType = logType,
                AccountId = claimsPrincipal?.GetId(),
                IpAddress = httpContextAccessor.HttpContext.Request.GetClientIpAddress(),
                Message = logType.ToString(),
                Success = success,
                Timestamp = DateTime.UtcNow
            };
            logGeneral.SetData(data);
            applicationDbContext.LogGenerals.Add(logGeneral);
            return applicationDbContext.SaveChangesAsync();
        }

        public enum LogType : uint
        {
            Invalid,
            AdminSignIn,
            UserListFilter,
            UserGeneralDataView,
            UserConquerAccountsView,
            UserBansView,
            UserPurchasesView,
            UserTradeHistoryView,
            UserTradeHistoryDetailView,
            RealmView,
            RealmCreation,
            RealmUpdate,
            UserBan,
            UserGameAccountBan,
            UserSubmitReward,
            UserSubmitMassReward,
            UserSubmitMail,
            UserSubmitMassMail,
            CreditCardCreationAttempt,
            CreditCardCreated,

            AdminEventLimit = 99_999,
        }
    }
}
