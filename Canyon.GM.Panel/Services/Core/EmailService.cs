﻿using MimeKit;
using MimeKit.Text;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace Canyon.GM.Panel.Services.Core
{
    public class EmailService : IEmailSender
    {
        private readonly ILogger<EmailService> logger;
        private readonly EmailSettings settings;

        public EmailService(ILogger<EmailService> logger, IConfiguration configuration)
        {
            settings = new EmailSettings();
            {
                settings.PrimaryDomain = configuration["DontReplyMail:PrimaryDomain"];
                settings.PrimaryPort = int.Parse(configuration["DontReplyMail:PrimaryPort"]);
                settings.UsernameEmail = configuration["DontReplyMail:UsernameEmail"];
                settings.UsernamePassword = configuration["DontReplyMail:UsernamePassword"];
                settings.FromEmail = configuration["DontReplyMail:FromEmail"];
                settings.EnableSsl = bool.Parse(configuration["DontReplyMail:EnableSsl"]);
                settings.FromName = configuration["DontReplyMail:FromName"];
            }

            this.logger = logger;
        }

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            logger.LogInformation("Starting email submit from {} to {}", settings.UsernameEmail, email);
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(settings.FromName, settings.FromEmail));
            message.To.Add(new MailboxAddress(email, email));
            message.Subject = subject;
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = htmlMessage
            };
            try
            {
                using var client = new SmtpClient();
                /*
                 * Keep this lines if using Selfsigned certificate (or no certificate at all)
                 */
                client.ServerCertificateValidationCallback = (s, c, h, e) => true; //NOSONAR
                client.CheckCertificateRevocation = false;                         //NOSONAR

                await client.ConnectAsync(settings.PrimaryDomain, settings.PrimaryPort);
                await client.AuthenticateAsync(settings.UsernameEmail, settings.UsernamePassword);
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
                logger.LogInformation("Email to \"{}\" sent successfully", email);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error when sending email: {}", ex.ToString());
            }
        }

        public class EmailSettings
        {
            public string PrimaryDomain { get; set; }
            public int PrimaryPort { get; set; }
            public string UsernameEmail { get; set; }
            public string UsernamePassword { get; set; }
            public string FromName { get; set; }
            public string FromEmail { get; set; }
            public bool EnableSsl { get; set; }
        }
    }
}
