﻿using Canyon.GM.Panel.Localization;
using Microsoft.AspNetCore.Mvc.Localization;
using System.Globalization;

namespace Canyon.GM.Panel.Services.Core
{
    public static class LanguageService
    {
        public static List<Languages> AvailableLanguages = new List<Languages>
        {
            new Languages {LanguageFullName = "Português Brasileiro", LanguageCultureName = "pt-BR"},
            new Languages {LanguageFullName = "English", LanguageCultureName = "en-US"}
        };

        public static IStringLocalizer GetLocalizer(this IStringLocalizerFactory factory, string culture)
        {
            var specifiedCulture = new CultureInfo(culture);
            CultureInfo.CurrentCulture = specifiedCulture;
            CultureInfo.CurrentUICulture = specifiedCulture;
            var localizer = factory.Create(typeof(Language));
            return localizer;
        }
    }

    public class StringLocalizer : IStringLocalizer<Language>
    {
        private readonly IStringLocalizer<Language> stringLocalizer;

        public StringLocalizer(IStringLocalizer<Language> stringLocalizer)
        {
            this.stringLocalizer = stringLocalizer;
        }

        public LocalizedString this[string name] => stringLocalizer[name];

        public LocalizedString this[string name, params object[] arguments] => stringLocalizer[name, arguments];

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            return stringLocalizer.GetAllStrings(includeParentCultures);
        }
    }

    public class HtmlLocalizer : IHtmlLocalizer<Language>
    {
        private readonly IHtmlLocalizer<Language> htmlLocalizer;

        public HtmlLocalizer(IHtmlLocalizer<Language> htmlLocalizer)
        {
            this.htmlLocalizer = htmlLocalizer;
        }

        public LocalizedHtmlString this[string name] => htmlLocalizer[name];

        public LocalizedHtmlString this[string name, params object[] arguments] => htmlLocalizer[name, arguments];

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            return htmlLocalizer.GetAllStrings(includeParentCultures);
        }

        public LocalizedString GetString(string name)
        {
            return htmlLocalizer.GetString(name);
        }

        public LocalizedString GetString(string name, params object[] arguments)
        {
            return htmlLocalizer.GetString(name, arguments);
        }
    }

    public class Languages
    {
        public string LanguageFullName { get; set; }
        public string LanguageCultureName { get; set; }
        public string ImageUrl { get; set; }
        public bool Rtl { get; set; } = false;
    }
}
