﻿using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Database;
using IdentityModel;
using System.Data;
using System.Security.Claims;
using Canyon.GM.Panel.Services.Identity;
using Microsoft.EntityFrameworkCore;
using Canyon.GM.Panel.Models.Articles;
using Canyon.GM.Panel.Helpers;
using Canyon.GM.Panel.Areas.Articles.Pages;

namespace Canyon.GM.Panel.Services
{
    public sealed class ArticleService
    {
        private const int CommentsIpp = 25;

        private readonly ApplicationDbContext context;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly SignInManager signInManager;
        private readonly UserManager userManager;
        private readonly IStringLocalizer stringLocalizer;
        private readonly ClaimsPrincipal currentPrincipal;

        public ArticleService(ApplicationDbContext context,
                              UserManager userManager,
                              SignInManager signInManager,
                              IHttpContextAccessor httpContextAccessor,
                              IStringLocalizer stringLocalizer,
                              ClaimsPrincipal currentPrincipal)
        {
            this.context = context;
            this.stringLocalizer = stringLocalizer;
            this.currentPrincipal = currentPrincipal;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.httpContextAccessor = httpContextAccessor;
        }

        public Task<List<Article>> GetAsync(int type, int page = 0, int ipp = 5)
        {
            return context
                   .Articles
                   .Where(x => x.Category.CategoryTypeId == type && x.PublishDate <= DateTime.Now
                                                                 && x.Flags == Article.ArticleFlags.Visible)
                   .Include(x => x.Category)
                   .Include(x => x.User)
                   .Include(x => x.Content).ThenInclude(x => x.Writer)
                   .Include(x => x.Content).ThenInclude(x => x.LastEditor)
                   .Include(x => x.Category).ThenInclude(x => x.CategoryType)
                   .OrderByDescending(x => x.Id)
                   .Skip(page * ipp)
                   .Take(ipp)
                   .ToListAsync();
        }

        public Task<int> CountByTypeAsync(int type)
        {
            return context
                   .Articles
                   .Where(x => x.Category.CategoryTypeId == type && x.PublishDate <= DateTime.Now
                                                                 && x.Flags == Article.ArticleFlags.Visible)
                   .CountAsync();
        }

        public Task<List<Article>> GetByCategoryAsync(int type, int page, int ipp = 5)
        {
            return context
                   .Articles
                   .Where(x => x.CategoryId == type && x.PublishDate <= DateTime.Now &&
                               x.Flags == Article.ArticleFlags.Visible)
                   .Include(x => x.Category)
                   .Include(x => x.User)
                   .Include(x => x.Content).ThenInclude(x => x.Writer)
                   .Include(x => x.Content).ThenInclude(x => x.LastEditor)
                   .Include(x => x.Category).ThenInclude(x => x.CategoryType)
                   .OrderBy(x => x.Order)
                   .ThenByDescending(x => x.Id)
                   .Skip(page * ipp)
                   .Take(ipp)
                   .ToListAsync();
        }

        public Task<Article> QueryArticleByIdAsync(int articleId)
        {
            return context.Articles.FirstOrDefaultAsync(x => x.Id == articleId);
        }

        public Task<Article> QueryFullArticleByIdAsync(int articleId)
        {
            return context.Articles
                           .Include(x => x.User)
                           .Include(x => x.Category)
                           .Include(x => x.Content).ThenInclude(x => x.Writer)
                           .Include(x => x.Content).ThenInclude(x => x.LastEditor)
                           .Include(x => x.Category).ThenInclude(x => x.CategoryType)
                           .FirstOrDefaultAsync(x => x.Id == articleId);
        }

        public Task<List<ArticleCategoryTypeModel>> QueryActiveArticleCategoryTypeAsync()
        {
            return context.ArticleCategoryTypes.Select(x => new ArticleCategoryTypeModel
            {
                Id = x.Id,
                Name = stringLocalizer.GetString(x.Name)
            }).ToListAsync();
        }

        public Task<List<ArticleCategoryModel>> QueryActiveArticleCategoriesByTypeAsync(int type)
        {
            return context.ArticleCategories.Where(x => x.CategoryTypeId == type && x.Flags == 0).Select(
                x => new ArticleCategoryModel
                {
                    Id = x.Id,
                    Name = stringLocalizer.GetString(x.Name),
                    RealName = x.Name
                }).ToListAsync();
        }

        public Task<List<ArticleCategory>> QueryActiveArticleCategoriesByTypeCompleteAsync(int type)
        {
            return context.ArticleCategories.Where(x => x.CategoryTypeId == type)
                           .OrderBy(x => x.Order)
                           .Include(x => x.Articles)
                           .ThenInclude(y => y.Content)
                           .ToListAsync();
        }

        public async Task<List<AdminArticleListModel>> QueryByCategoryAsync(
            int category, int page = 0, int ipp = 10)
        {
            List<AdminArticleListModel> result = new();
            foreach (var article in await context.Articles.Where(x => x.CategoryId == category)
                                                  .Include(x => x.User).ThenInclude(x => x.Claims)
                                                  .Include(x => x.Category)
                                                  .Include(x => x.Content).ThenInclude(x => x.Writer)
                                                  .Include(x => x.Content).ThenInclude(x => x.LastEditor)
                                                  .Include(x => x.Category).ThenInclude(x => x.CategoryType)
                                                  .OrderBy(x => x.Order)
                                                  .ThenByDescending(x => x.Id)
                                                  .Skip(Math.Max(0, page - 1))
                                                  .Take(ipp <= 0 ? int.MaxValue : ipp)
                                                  .ToListAsync())
            {
                ArticleContent content = article.Content.FirstOrDefault(x => x.Locale.Equals(currentPrincipal.GetCurrentLocale())) ??
                                         article.Content.FirstOrDefault();
                string writerName = content.Writer?.Claims.FirstOrDefault(x => x.ClaimType == JwtClaimTypes.Name)?.ClaimValue ?? stringLocalizer.GetString("UserNotFound");
                result.Add(new AdminArticleListModel
                {
                    Id = article.Id,
                    Writer = writerName,
                    Category = stringLocalizer.GetString(article.Category.Name),
                    Title = content.Title,
                    PublishDate = article.PublishDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    Locales = new List<string>(article.Content.Select(x => x.Locale)),
                    Order = article.Order,
                    Sortable = article.Category.CategoryTypeId == 2
                });

            }
            return result;
        }

        public async Task<List<AdminArticleListModel>> QueryByCategoryTypeAsync(
            int categoryType, int page = 0, int ipp = 10)
        {
            List<ArticleCategory> categories = await context
                                                     .ArticleCategories
                                                     .Where(x => x.CategoryTypeId == categoryType)
                                                     .Include(x => x.Articles).ThenInclude(x => x.Content)
                                                     .Include(x => x.Articles).ThenInclude(x => x.User)
                                                     .OrderBy(x => x.Order)
                                                     .ThenByDescending(x => x.Id)
                                                     .Skip(Math.Max(0, page - 1))
                                                     .Take(ipp <= 0 ? int.MaxValue : ipp)
                                                     .ToListAsync();

            List<AdminArticleListModel> result = new();
            foreach (ArticleCategory cat in categories)
            {
                foreach (Article article in cat.Articles)
                {
                    ArticleContent content =
                        article.Content.FirstOrDefault(x => x.Locale.Equals(currentPrincipal.GetCurrentLocale())) ??
                        article.Content.FirstOrDefault();

                    ApplicationUser writer = await userManager.FindByIdAsync(content.WriterId.ToString());
                    ClaimsPrincipal writerPrincipal = await signInManager.CreateUserPrincipalAsync(writer);

                    result.Add(new AdminArticleListModel
                    {
                        Id = article.Id,
                        Writer = writerPrincipal.FindFirstValue(JwtClaimTypes.Name) ?? stringLocalizer.GetString("UserNotFound"),
                        Category = stringLocalizer.GetString(cat.Name),
                        Title = content.Title,
                        PublishDate = article.PublishDate.ToString("O"),
                        Locales = new List<string>(article.Content.Select(x => x.Locale)),
                        Order = article.Order,
                        Sortable = article.Category.CategoryTypeId == 2
                    });
                }
            }

            return result;
        }

        //public async Task<ArticleCommentResultModel> SaveCommentAsync(int articleId, int senderId, string message,
        //                                                              string ipAddress = "0.0.0.0", string tokenId = "")
        //{
        //    Task<ApplicationUser> user = userManager.FindByIdAsync(senderId.ToString());
        //    if (user == null)
        //    {
        //        throw new UnauthorizedException();
        //    }

        //    Article article = await QueryArticleByIdAsync(articleId);
        //    if (article == null)
        //    {
        //        return new ArticleCommentResultModel();
        //    }

        //    if (string.IsNullOrEmpty(message) || message.Length < 6 || message.Length > 1024)
        //    {
        //        throw new BadRequestException(stringLocalizer.GetString("CommentMessageSize", 6, 1024));
        //    }

        //    var html = new HtmlDocument();
        //    html.LoadHtml(message);
        //    message = html.DocumentNode.InnerText;

        //    var comment = new ArticleComment
        //    {
        //        ArticleId = articleId,
        //        CreationDate = DateTime.Now,
        //        Dislikes = 0,
        //        Likes = 0,
        //        IpAddress = ipAddress,
        //        TokenId = tokenId,
        //        Flag = 0,
        //        Message = message,
        //        UserId = senderId
        //    };
        //    if (!await context.SaveAsync(comment))
        //        return null;

        //    return new ArticleCommentResultModel
        //    {
        //        Id = comment.Id,
        //        Message = message,
        //        CreatorName = "",
        //        Likes = 0,
        //        Dislikes = 0,
        //        CreationDate = comment.CreationDate,
        //        UpdateDate = comment.UpdateDate
        //    };
        //}

        //public async Task<int> RemoveCommentAsync(int commentId)
        //{
        //    ArticleComment comment = await context.ArticleComments.FirstOrDefaultAsync(x => x.Id == commentId);
        //    if (comment == null || (comment.Flag & 0x1) != 0)
        //        return StatusCodes.Status204NoContent;

        //    comment.Flag |= 1;
        //    await context.SaveAsync(comment);
        //    return StatusCodes.Status200OK;
        //}

        public async Task<(int Result, int ArticleId)> PostArticleAsync(ApplicationUser user, int articleId,
                                                                        IndexModel.InputModel model)
        {
            Article article = await QueryFullArticleByIdAsync(articleId);
            try
            {
                await context.Database.BeginTransactionAsync();

                ArticleContent content = null;
                if (article != null)
                {
                    content = article.Content.FirstOrDefault(x => x.Locale.Equals(model.Locale, StringComparison.InvariantCultureIgnoreCase));

                    article.PublishDate = model.PublishDate ?? DateTime.Now;

                    if (model.NotPublished)
                    {
                        article.Flags |= Article.ArticleFlags.NotPublished;
                    }
                    else if ((article.Flags & Article.ArticleFlags.NotPublished) != 0)
                    {
                        article.Flags &= ~Article.ArticleFlags.NotPublished;
                    }

                    if (model.NotListed)
                    {
                        article.Flags |= Article.ArticleFlags.NotListed;
                    }
                    else if ((article.Flags & Article.ArticleFlags.NotListed) != 0)
                    {
                        article.Flags &= ~Article.ArticleFlags.NotListed;
                    }
                }
                else
                {
                    article = new Article
                    {
                        CategoryId = model.ArticleCategoryId,
                        CreationDate = DateTime.Now,
                        Flags = Article.ArticleFlags.Visible,
                        UserId = user.Id,
                        PublishDate = model.PublishDate ?? DateTime.Now
                    };

                    if (model.NotPublished)
                    {
                        article.Flags |= Article.ArticleFlags.NotPublished;
                    }
                    else
                    {
                        article.PublishDate = model.PublishDate ?? DateTime.Now;
                    }
                }

                if (content != null)
                {
                    content.Title = model.Title;
                    content.Content = model.Content;
                    content.LastEditorId = user.Id;
                }
                else
                {
                    content = new ArticleContent
                    {
                        Title = model.Title,
                        Content = model.Content,
                        WriterId = user.Id,
                        Locale = model.Locale
                    };
                    article.Content ??= new List<ArticleContent>();
                    article.Content.Add(content);
                }

                await context.SaveAsync(article);
                await context.Database.CommitTransactionAsync();
            }
            catch
            {
                await context.Database.RollbackTransactionAsync();
                return (StatusCodes.Status500InternalServerError, 0);
            }

            return (StatusCodes.Status200OK, article.Id);
        }

        public Task SaveArticleAsync(Article article)
        {
            return context.SaveAsync(article);
        }

        public Task<ArticleReadToken> QueryUserReadTokenAsync(int articleId, string readToken)
        {
            return context.ArticleReadTokens.FirstOrDefaultAsync(
                x => x.ArticleId == articleId && x.TokenId.Equals(readToken));
        }

        public Task SaveReadUserTokenAsync(ArticleReadToken token)
        {
            return context.SaveAsync(token);
        }

        public Task<List<ArticleComment>> QueryArticleCommentsAsync(int articleId, int page)
        {
            return context.ArticleComments
                           .Include(x => x.User)
                           .Include(x => x.Article)
                           .Where(x => x.ArticleId == articleId)
                           .OrderBy(x => x.Id)
                           .Skip(page * CommentsIpp)
                           .Take(CommentsIpp)
                           .ToListAsync();
        }

        public async Task<List<Article>> QueryMostPopularArticlesAsync(int num)
        {
            DataTable articles =
                await context.SelectAsync(
                    $"SELECT COUNT(1), ArticleId FROM article_read_token GROUP BY ArticleId ORDER BY 1 DESC LIMIT {num};");
            List<Article> result = new();

            foreach (DataRow row in articles.Rows)
            {
                int idArticle = int.Parse(row["ArticleId"]?.ToString() ?? "0");
                Article article = await QueryFullArticleByIdAsync(idArticle);
                if (article != null)
                    result.Add(article);
            }

            if (result.Count < num)
            {
                int amount = num - result.Count;
                result.AddRange(await context.Articles
                                              .Where(x => result.Select(y => y.Id).All(y => y != x.Id))
                                              .Include(x => x.User)
                                              .Include(x => x.Category)
                                              .Include(x => x.Content).ThenInclude(x => x.Writer)
                                              .Include(x => x.Content).ThenInclude(x => x.LastEditor)
                                              .Include(x => x.Category).ThenInclude(x => x.CategoryType)
                                              .Take(amount)
                                              .OrderByDescending(x => x.Views).ToListAsync());
            }

            return result;
        }

        /// <remarks>0 UP 1 DOWN</remarks>
        public async Task<int> ChangeArticleOrderAsync(int articleId, int direction)
        {
            Article article = await QueryArticleByIdAsync(articleId);
            if (article == null)
                return StatusCodes.Status204NoContent;

            Article swap;
            if (direction == 0) // up
            {
                swap = context.Articles
                                     .Where(x => x.Id != article.Id && x.CategoryId == article.CategoryId && x.Order <= article.Order)
                                     .ToList()
                                     .Aggregate((x, y) => Math.Abs(x.Order - article.Order) < Math.Abs(y.Order - article.Order) ? x : y);
            }
            else // down
            {
                swap = context.Articles
                               .Where(x => x.Id != article.Id && x.CategoryId == article.CategoryId && x.Order >= article.Order)
                               .ToList()
                               .Aggregate((x, y) => Math.Abs(x.Order + article.Order) < Math.Abs(y.Order + article.Order) ? x : y);
            }

            if (swap.Id == 0 || swap.Id == article.Id)
                return StatusCodes.Status304NotModified;

            (article.Order, swap.Order) = (swap.Order, article.Order);
            await context.SaveAsync(article);
            await context.SaveAsync(swap);

            await RebuildArticleOrderAsync();
            return StatusCodes.Status200OK;
        }

        public async Task RebuildArticleOrderAsync()
        {
            foreach (var category in context.ArticleCategories
                                             .Include(x => x.Articles)
                                             .Where(x => x.CategoryTypeId == 2))
            {
                int order = 0;
                foreach (var article in category.Articles.OrderBy(x => x.Order).ThenBy(x => x.Id))
                {
                    article.Order = order++;
                }
                await context.SaveAsync(category);
            }
        }
    }
}
