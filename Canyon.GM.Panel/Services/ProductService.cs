﻿using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Local;
using Microsoft.EntityFrameworkCore;

namespace Canyon.GM.Panel.Services
{
    public class ProductService
    {
        private readonly ApplicationDbContext applicationDbContext;

        public ProductService(
            ApplicationDbContext applicationDbContext
            )
        {
            this.applicationDbContext = applicationDbContext;
        }

        public Task<Product> GetProductByIdAsync( Guid productId )
        {
            return applicationDbContext.Products.FirstOrDefaultAsync(x => x.Id == productId);
        }

        public Task<List<Product>> GetActiveProductsAsync(int pageIndex, int indexesPerPage) 
        {
            return applicationDbContext.Products
                .Skip(pageIndex * indexesPerPage)
                .Take(indexesPerPage)
                .ToListAsync();
        }
    }
}
