﻿using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Canyon.GM.Panel.Services
{
    public class RealmService
    {
        private readonly byte[] cipherKey;
        private readonly ApplicationDbContext dbContext;
        private readonly ILogger<RealmService> logger;

        public RealmService(
            ApplicationDbContext dbContext,
            IConfiguration configuration,
            ILogger<RealmService> logger
            )
        {
            this.dbContext = dbContext;
            this.logger = logger;
            string strKey = configuration["RealmSettings:AesKey"];
            cipherKey = new byte[strKey.Length / 2];
            for (var index = 0; index < cipherKey.Length; index++)
            {
                string byteValue = strKey.Substring(index * 2, 2);
                cipherKey[index] = Convert.ToByte(byteValue, 16);
            }
        }

        public Task<RealmData> FindByNameAsync(string name)
        {
            return dbContext.Realms.FirstOrDefaultAsync(x => x.Name.Equals(name));
        }

        public Task<RealmData> FindByIdAsync(Guid id)
        {
            return dbContext.Realms.FirstOrDefaultAsync(x => x.RealmID == id);
        }

        public string DecryptData(string data)
        {
            return AesCipherHelper.Decrypt(cipherKey, data);
        }

        public async Task<string> GetRealmConnectionStringAsync(string realmName)
        {
            RealmData realmData = await FindByNameAsync(realmName);
            if (realmData == null)
                return string.Empty;
            RealmPublicData realm = GetRealmData(realmData);
            return $"server={realm.DatabaseHostname};database={realm.DatabaseSchema};user={realm.DatabaseUsername};password={AesCipherHelper.Decrypt(cipherKey, realmData.DatabasePass)};port={realm.DatabasePort}";
        }

        public string GetRealmConnectionString(Guid realmId)
        {
            RealmData realmData = dbContext.Realms.Find(realmId);
            if (realmData == null)
            {
                return string.Empty;
            }
            RealmPublicData realm = GetRealmData(realmData);
            return $"server={realm.DatabaseHostname};database={realm.DatabaseSchema};user={realm.DatabaseUsername};password={AesCipherHelper.Decrypt(cipherKey, realmData.DatabasePass)};port={realm.DatabasePort}";
        }

        public async Task<List<RealmPublicListData>> QueryRealmsAsync(int page, int ipp)
        {
            List<RealmData> realmData = await dbContext.Realms
                .OrderBy(x => x.Name)
                .Skip(page * ipp)
                .Take(ipp)
                .ToListAsync();

            return realmData.Select(x =>
            {
                return new RealmPublicListData
                {
                    RealmID = x.RealmID,
                    RealmName = x.Name,
                    RealmStatus = x.Status,
                    Active = x.Active,
                    Production = x.ProductionRealm
                };
            }).ToList();
        }

        public RealmPublicData GetRealmData(RealmData realm)
        {
#if DEBUG && REMEMBER_PASSWORDS
            string realmDbPassword = AesCipherHelper.Decrypt(cipherKey, realm.DatabasePass);
            string realmPassword = AesCipherHelper.Decrypt(cipherKey, realm.Password);
#endif
            return new RealmPublicData
            {
                RealmID = realm.RealmID,
                RealmName = realm.Name,
                RealmStatus = realm.Status,

                GameIPAddress = realm.GameIPAddress,
                GamePort = realm.GamePort,
                RpcIPAddress = realm.RpcIPAddress,
                RpcPort = realm.RpcPort,

                RealmUsername = AesCipherHelper.Decrypt(cipherKey, realm.Username),
                DatabaseHostname = AesCipherHelper.Decrypt(cipherKey, realm.DatabaseHost),
                DatabaseUsername = AesCipherHelper.Decrypt(cipherKey, realm.DatabaseUser),
                DatabaseSchema = AesCipherHelper.Decrypt(cipherKey, realm.DatabaseSchema),
                DatabasePort = int.Parse(AesCipherHelper.Decrypt(cipherKey, realm.DatabasePort)),

                GmToolsAddress = realm.GmToolsHost,
                GmToolsPort = realm.GmToolsPort,
                GmToolsUsername = AesCipherHelper.Decrypt(cipherKey, realm.GmToolsUserName),

                Active = realm.Active,
                Production = realm.ProductionRealm
            };
        }

        public async Task<bool> CreateRealmAsync(CreateRealmModel realm)
        {
            return await dbContext.CreateAsync(new RealmData
            {
                RealmID = Guid.NewGuid(),
                Name = realm.RealmName,
                GameIPAddress = realm.GameIPAddress,
                RpcIPAddress = realm.RpcIPAddress,
                GamePort = realm.GamePort,
                RpcPort = realm.RpcPort,
                Username = AesCipherHelper.Encrypt(cipherKey, realm.Username),
                Password = AesCipherHelper.Encrypt(cipherKey, realm.Password),
                DatabaseHost = AesCipherHelper.Encrypt(cipherKey, realm.DatabaseHost),
                DatabaseUser = AesCipherHelper.Encrypt(cipherKey, realm.DatabaseUser),
                DatabasePass = AesCipherHelper.Encrypt(cipherKey, realm.DatabasePass),
                DatabaseSchema = AesCipherHelper.Encrypt(cipherKey, realm.DatabaseSchema),
                DatabasePort = AesCipherHelper.Encrypt(cipherKey, realm.DatabasePort),
                GmToolsHost = realm.GmToolsAddress,
                GmToolsPort = realm.GmToolsPort,
                GmToolsUserName = AesCipherHelper.Encrypt(cipherKey, realm.Username),
                GmToolsPassword = AesCipherHelper.Encrypt(cipherKey, realm.GmToolsPassword),
                Active = realm.Active,
                ProductionRealm = realm.Production
            });
        }

        public async Task<bool> UpdateRealmAsync(AlterRealmModel realm)
        {
            RealmData realmData = await FindByIdAsync(realm.RealmID);
            if (realmData == null)
                return false;

            realmData.GameIPAddress = realm.GameIPAddress;
            realmData.RpcIPAddress = realm.RpcIPAddress;
            realmData.GamePort = realm.GamePort;
            realmData.RpcPort = realm.RpcPort;
            realmData.Username = AesCipherHelper.Encrypt(cipherKey, realm.Username);

            if (!string.IsNullOrEmpty(realm.Password))
                realmData.Password = AesCipherHelper.Encrypt(cipherKey, realm.Password);

            realmData.DatabaseHost = AesCipherHelper.Encrypt(cipherKey, realm.DatabaseHost);
            realmData.DatabaseUser = AesCipherHelper.Encrypt(cipherKey, realm.DatabaseUser);

            if (!string.IsNullOrEmpty(realm.DatabasePass))
                realmData.DatabasePass = AesCipherHelper.Encrypt(cipherKey, realm.DatabasePass);

            realmData.DatabaseSchema = AesCipherHelper.Encrypt(cipherKey, realm.DatabaseSchema);
            realmData.DatabasePort = AesCipherHelper.Encrypt(cipherKey, realm.DatabasePort);

            realmData.GmToolsHost = realm.GmToolsAddress;
            realmData.GmToolsPort = realm.GmToolsPort;
            realmData.GmToolsUserName = AesCipherHelper.Encrypt(cipherKey, realm.GmToolsUserName);

            if (!string.IsNullOrEmpty(realm.GmToolsPassword))
                realmData.GmToolsPassword = AesCipherHelper.Encrypt(cipherKey, realm.GmToolsPassword);

            realmData.Active = realm.Active;
            realmData.ProductionRealm = realm.Production;
            await dbContext.SaveAsync(realmData);
            return true;
        }

        public Task ChangeEncryptionKeyAsync(byte[] newCypher)
        {
            using var transaction = dbContext.Database.BeginTransaction();
            try
            {
                var realms = dbContext.Realms.ToList();

                foreach (var realm in realms)
                {
                    string userName = AesCipherHelper.Decrypt(cipherKey, realm.Username);
                    string password = AesCipherHelper.Decrypt(cipherKey, realm.Password);
                    string databaseHost = AesCipherHelper.Decrypt(cipherKey, realm.DatabaseHost);
                    string databaseUser = AesCipherHelper.Decrypt(cipherKey, realm.DatabaseUser);
                    string databasePass = AesCipherHelper.Decrypt(cipherKey, realm.DatabasePass);
                    string databaseSchema = AesCipherHelper.Decrypt(cipherKey, realm.DatabaseSchema);
                    string databasePort = AesCipherHelper.Decrypt(cipherKey, realm.DatabasePort);
                    string gmToolsUsername = AesCipherHelper.Decrypt(cipherKey, realm.GmToolsUserName);
                    string gmToolPassword = AesCipherHelper.Decrypt(cipherKey, realm.GmToolsPassword);

                    userName = AesCipherHelper.Encrypt(newCypher, userName);
                    password = AesCipherHelper.Encrypt(newCypher, password);
                    databaseHost = AesCipherHelper.Encrypt(newCypher, databaseHost);
                    databaseUser = AesCipherHelper.Encrypt(newCypher, databaseUser);
                    databasePass = AesCipherHelper.Encrypt(newCypher, databasePass);
                    databaseSchema = AesCipherHelper.Encrypt(newCypher, databaseSchema);
                    databasePort = AesCipherHelper.Encrypt(newCypher, databasePort);
                    gmToolsUsername = AesCipherHelper.Encrypt(newCypher, gmToolsUsername);
                    gmToolPassword = AesCipherHelper.Encrypt(newCypher, gmToolPassword);

                    realm.Username = userName;
                    realm.Password = password;
                    realm.DatabaseHost = databaseHost;
                    realm.DatabaseUser = databaseUser;
                    realm.DatabasePass = databasePass;
                    realm.DatabaseSchema = databaseSchema;
                    realm.DatabasePort = databasePort;
                    realm.GmToolsUserName = gmToolsUsername;
                    realm.GmToolsPassword = gmToolPassword;

                    dbContext.Realms.Update(realm);
                }

                dbContext.SaveChanges();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "{Error}", ex.Message);
                transaction.Rollback();
                return Task.FromException(ex);
            }

            return Task.CompletedTask;
        }

        public struct CreateRealmModel
        {
            public string RealmName { get; set; }
            public string GameIPAddress { get; set; }
            public string RpcIPAddress { get; set; }
            public uint GamePort { get; set; }
            public uint RpcPort { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }

            public string DatabaseHost { get; set; }
            public string DatabaseUser { get; set; }
            public string DatabasePass { get; set; }
            public string DatabaseSchema { get; set; }
            public string DatabasePort { get; set; }

            public string GmToolsAddress { get; set; }
            public int GmToolsPort { get; set; }
            public string GmToolsUserName { get; set; }
            public string GmToolsPassword { get; set; }

            public bool Active { get; set; }
            public bool Production { get; set; }
        }

        public struct AlterRealmModel
        {
            public Guid RealmID { get; set; }
            public string RealmName { get; set; }
            public string GameIPAddress { get; set; }
            public string RpcIPAddress { get; set; }
            public uint GamePort { get; set; }
            public uint RpcPort { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }

            public string DatabaseHost { get; set; }
            public string DatabaseUser { get; set; }
            public string DatabasePass { get; set; }
            public string DatabaseSchema { get; set; }
            public string DatabasePort { get; set; }

            public string GmToolsAddress { get; set; }
            public int GmToolsPort { get; set; }
            public string GmToolsUserName { get; set; }
            public string GmToolsPassword { get; set; }

            public bool Active { get; set; }
            public bool Production { get; set; }
        }

        public struct RealmPublicListData
        {
            public Guid RealmID { get; set; }
            public string RealmName { get; set; }
            public int RealmStatus { get; set; }
            public bool Active { get; set; }
            public bool Production { get; set; }
        }

        public struct RealmPublicData
        {
            public Guid RealmID { get; set; }
            public string RealmName { get; set; }
            public int RealmStatus { get; set; }

            public string GameIPAddress { get; set; }
            public string RpcIPAddress { get; set; }
            public uint GamePort { get; set; }
            public uint RpcPort { get; set; }

            public string RealmUsername { get; set; }
            public string DatabaseHostname { get; set; }
            public string DatabaseUsername { get; set; }
            public string DatabaseSchema { get; set; }
            public int DatabasePort { get; set; }

            public bool Active { get; set; }
            public bool Production { get; set; }

            public string GmToolsAddress { get; set; }
            public int GmToolsPort { get; set; }
            public string GmToolsUsername { get; set; }
        }
    }
}
