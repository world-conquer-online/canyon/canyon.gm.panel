﻿using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Sockets.Piglet.Packets;
using System.Globalization;

namespace Canyon.GM.Panel.Services
{
    public class MaintenanceService
    {
        private readonly ILogger<MaintenanceService> logger;
        private readonly IServiceScopeFactory scopeFactory;
        private readonly RealmSocketService realmSocketService;
        private MaintenanceObject nextMaintenance;

        public MaintenanceService(
            ILogger<MaintenanceService> logger,
            IServiceScopeFactory scopeFactory,
            RealmService realmService,
            RealmSocketService realmSocketService
            )
        {
            this.logger = logger;
            this.scopeFactory = scopeFactory;
            this.realmSocketService = realmSocketService;
        }

        private static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
        {
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }

        public Task InitializeAsync()
        {
            using var scope = scopeFactory.CreateScope();
            var applicationDbContext = scope.ServiceProvider.GetService<ApplicationDbContext>();
            Dictionary<DateTime, ConquerMaintenanceSchedule> schedules = new();
            foreach (var schedule in applicationDbContext.ConquerMaintenanceSchedules)
            {
                DateTime nextSchedule = GetNextWeekday(DateTime.Now, schedule.Weekday);
                nextSchedule = DateTime.ParseExact($"{nextSchedule:yyyy-MM-dd} {schedule.StartTime}", "yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                schedules.Add(nextSchedule, schedule);
            }
            //
            var next = schedules.Where(x => x.Key > DateTime.Now.AddMinutes(60)).OrderBy(x => x.Key).First();
            nextMaintenance = new MaintenanceObject(next.Value.WarningMinutes, next.Key);

            logger.LogInformation("Next maintenance set to: {}", nextMaintenance);
            return Task.CompletedTask;
        }

        public async Task StartRealmAsync(Guid realmId)
        {

        }

        public async Task StopRealmAsync(Guid realmId)
        {

        }

        public async Task OnTimerAsync()
        {
            if (nextMaintenance != null)
            {
                int time = int.Parse(nextMaintenance.WarningTime.ToString("HHmm"));
                int timeNow = int.Parse(DateTime.Now.ToString("HHmm"));
                if (nextMaintenance.WarningTime.Date == DateTime.Now.Date
                    && time == timeNow)
                {
                    await realmSocketService.BroadcastAsync(new MsgPigletRealmAnnounceMaintenance
                    {
                        Data = new Network.Packets.Piglet.MsgPigletRealmAnnounceMaintenance<Sockets.Piglet.PigletActor>.AnnounceData
                        {
                            WarningMinutes = nextMaintenance.Minutes
                        }
                    });

                    await InitializeAsync();
                }
            }
        }

        internal class MaintenanceObject
        {
            private readonly int warningMinutes;
            private readonly DateTime nextSchedule;

            public MaintenanceObject(int warningMinutes, DateTime nextSchedule)
            {
                this.warningMinutes = warningMinutes;
                this.nextSchedule = nextSchedule;

                WarningTime = nextSchedule.AddMinutes(warningMinutes * -1);
            }

            public int Minutes => warningMinutes;
            public DateTime WarningTime { get; init; }
            public DateTime NextSchedule => nextSchedule;

            public override string ToString()
            {
                return NextSchedule.ToString();
            }
        }
    }
}
