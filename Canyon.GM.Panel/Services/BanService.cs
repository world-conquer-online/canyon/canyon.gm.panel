﻿using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Database;
using Microsoft.AspNetCore.Identity.UI.Services;
using System.Security.Claims;
using Canyon.GM.Panel.Services.Identity;
using Microsoft.EntityFrameworkCore;
using Canyon.GM.Panel.Models.Bans;
using Canyon.GM.Panel.Models.Rest.Bans;
using Canyon.GM.Panel.Models.Exceptions;
using static Canyon.GM.Panel.Services.GameAccountService;
using Canyon.GM.Panel.Models.Rest.Account;
using IdentityModel;
using Canyon.GM.Panel.Services.Logging;

namespace Canyon.GM.Panel.Services
{
    public class BanService
    {
        private readonly ApplicationDbContext applicationDbContext;
        private readonly IStringLocalizer stringLocalizer;
        private readonly UserManager userManager;
        private readonly IEmailSender emailSender;
        private readonly SignInManager signInManager;
        private readonly GameAccountService gameAccountService;
        private readonly ILogger<BanService> logger;
        private readonly RealmSocketService realmSocketService;
        private readonly EventLogger eventLogger;

        public BanService(
            ApplicationDbContext applicationDbContext,
            IStringLocalizer stringLocalizer,
            UserManager userManager,
            IEmailSender emailSender,
            SignInManager signInManager,
            GameAccountService gameAccountService,
            ILogger<BanService> logger,
            RealmSocketService realmSocketService,
            EventLogger eventLogger
        )
        {
            this.applicationDbContext = applicationDbContext;
            this.stringLocalizer = stringLocalizer;
            this.userManager = userManager;
            this.emailSender = emailSender;
            this.signInManager = signInManager;
            this.gameAccountService = gameAccountService;
            this.logger = logger;
            this.realmSocketService = realmSocketService;
            this.eventLogger = eventLogger;
        }

        public async Task<BanReport> GetBanInformationAsync(ApplicationUser user, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var banReport = new BanReport();
            List<ApplicationUserBan> userBans = await applicationDbContext.ApplicationUserBans.AsQueryable()
                                                              .Where(x => x.AccountId == user.Id)
                                                              .ToListAsync(cancellationToken);
            foreach (ApplicationUserBan dbBan in userBans)
            {
                var info = new BanReport.BanInformation();
                ApplicationUserBanReason banReason = await applicationDbContext.ApplicationUserBanReasons.AsQueryable()
                                                                   .FirstOrDefaultAsync(
                                                                       x => x.Id == dbBan.ReasonId, cancellationToken);
                if (banReason == null)
                    continue;

                if (banReason.Type == 0)
                {
                    info.Id = user.Id;
                    info.Name = user.UserName;

                    banReport.Account.Add(info);
                }
                else
                {
                    if (!dbBan.GameAccountId.HasValue)
                    {
                        continue;
                    }
                    var gameAccount = await gameAccountService.FindByIdAsync(dbBan.GameAccountId.Value);
                    if (gameAccount == null)
                    {
                        info.GameAccountId = dbBan.GameAccountId.Value;
                        info.Name = "Error";
                    }
                    else
                    {
                        info.GameAccountId = gameAccount.Id;
                        info.Name = gameAccount.UserName;
                    }

                    banReport.Game.Add(info);
                }

                info.BanIdentity = dbBan.Id;
                info.ReasonId = banReason.Id;
                info.Reason = stringLocalizer.GetString(banReason.Name);
                info.Description = dbBan.ReasonMessage;
                info.From = dbBan.BanTime;
                info.To = dbBan.ExpireTime;
                info.Flags = (BanFlags)dbBan.Flags;

                ApplicationUser bannedBy = await applicationDbContext.Users.AsQueryable()
                                                         .FirstOrDefaultAsync(
                                                             x => x.Id == dbBan.BannedBy, cancellationToken);
                if (bannedBy == null)
                {
                    info.BannedBy = stringLocalizer.GetString("FormerModerator");
                }
                else
                {
                    info.BannedBy = bannedBy.UserName;
                }
            }

            return banReport;
        }

        public async Task<BanReport.BanTerm> QueryNextBanTermAsync(ApplicationUser user, int type, int data = 0,
                                                                   CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            BanReport report = await GetBanInformationAsync(user, cancellationToken);

            ApplicationUserBanReason banType = await applicationDbContext.ApplicationUserBanReasons.FirstOrDefaultAsync(x => x.Id == type, cancellationToken);

            if (banType == null)
                return null;

            if (banType.Type == 1 && data == 0)
                return null;

            ApplicationUserBanReasonFactor factor;
            BanReport.BanTerm term = new();
            List<ApplicationUserBanReasonFactor> factors = await applicationDbContext.ApplicationUserBanReasonsFactors.Where(x => x.ReasonId == banType.Id).ToListAsync();

            term.Type = stringLocalizer.GetString(banType.Name);
            int banCount;
            if (banType.Type == 0)
            {
                banCount = report.Account.Count(x => x.ReasonId == type && (x.Flags & BanFlags.Forgiven) == 0 &&
                                                         (x.Flags & BanFlags.FalsePositive) == 0);

            }
            else
            {
                banCount = report.Game.Count(x => x.ReasonId == type && (x.Flags & BanFlags.Forgiven) == 0 &&
                                                      (x.Flags & BanFlags.FalsePositive) == 0);
            }

            if (banCount > 0 && factors.All(x => x.Ocurrences != banCount))
            {
                if (banCount >= factors.Max(x => x.Ocurrences))
                    factor = factors.FirstOrDefault(x => x.Ocurrences == factors.Max(y => y.Ocurrences));
                else
                    factor = factors.FirstOrDefault();
            }
            else
            {
                factor = factors.FirstOrDefault(x => x.Ocurrences == banCount);
            }

            TimeSpan termTime = TimeSpan.FromMinutes(factor.Minutes);
            string tempTimeReturn =
                stringLocalizer.GetString("BanTermsTimeDisplay", termTime.TotalDays, termTime.Hours, termTime.Minutes);

            term.IsPermanent = factor.Permanent;
            term.Minutes = factor.Minutes;
            if (factor.Permanent)
            {
                term.Terms = stringLocalizer.GetString("BanTermsAdmDisplayPermanent",
                                                   factor.AllowCancel
                                                       ? stringLocalizer.GetString("Yes")
                                                       : stringLocalizer.GetString("No"),
                                                   factor.CanBeForgiven
                                                       ? stringLocalizer.GetString("Yes")
                                                       : stringLocalizer.GetString("No"),
                                                   term.Type);
            }
            else
            {
                term.Terms = stringLocalizer.GetString("BanTermsAdmDisplay",
                                                   factor.AllowCancel
                                                       ? stringLocalizer.GetString("Yes")
                                                       : stringLocalizer.GetString("No"),
                                                   factor.CanBeForgiven
                                                       ? stringLocalizer.GetString("Yes")
                                                       : stringLocalizer.GetString("No"),
                                                   tempTimeReturn, term.Type);
            }
            return term;
        }

        public async Task<List<BanListResponse>> QueryBansListAsync(int page, int ipp = 25)
        {
            page = Math.Max(page - 1, 0);
            var tempResult = await applicationDbContext.ApplicationUserBans
                .AsQueryable()
                .OrderBy(x => x.Flags != 0)
                .Skip(page * ipp)
                .Take(ipp)
                .ToListAsync();

            List<BanListResponse> result = new();
            foreach (var item in tempResult)
            {
                var reason = await applicationDbContext.ApplicationUserBanReasons.AsQueryable().FirstOrDefaultAsync(x => x.Id == item.ReasonId);
                string userName = stringLocalizer.GetString("InvalidAccount");
                if (reason.Type == 0)
                {
                    var account = await applicationDbContext.Users.AsQueryable().FirstOrDefaultAsync(x => x.Id == item.AccountId);
                    if (account != null)
                        userName = account.UserName;
                }
                else
                {
                    var gameAccount = await gameAccountService.FindByIdAsync(item.GameAccountId.Value);
                    if (gameAccount != null)
                        userName = gameAccount.UserName;
                }

                string bannedBy = stringLocalizer.GetString("FormerModerator");
                var moderator = await applicationDbContext.Users.AsQueryable().FirstOrDefaultAsync(x => x.Id == item.BannedBy);
                if (moderator != null)
                    bannedBy = moderator.UserName;

                result.Add(new BanListResponse
                {
                    Type = reason.Type == 0 ? stringLocalizer.GetString("Account") : stringLocalizer.GetString("Game"),
                    UUID = item.Id,
                    UserName = userName,
                    BannedBy = bannedBy,
                    Reason = stringLocalizer.GetString(reason.Name),
                    From = item.BanTime,
                    To = item.ExpireTime
                });
            }
            return result;
        }

        public async Task<List<BanReport.BanInformation>> QueryAccountBansAsync(Guid uuid)
        {
            ApplicationUser applicationUser = await userManager.FindByIdAsync(uuid.ToString());
            if (applicationUser == null)
            {
                return new();
            }
            var bans = await GetBanInformationAsync(applicationUser);
            return bans.Account;
        }

        public async Task<BanReport> QueryBanReportAsync(Guid uuid)
        {
            ApplicationUser applicationUser = await userManager.FindByIdAsync(uuid.ToString());
            if (applicationUser == null)
            {
                return new();
            }
            return await GetBanInformationAsync(applicationUser);
        }

        public async Task<List<GameAccountResponse>> FillBanInformationAsync(List<GameAccountResponse> accounts, Guid parent)
        {
            var banInformation = await QueryBanReportAsync(parent);
            foreach (var account in accounts)
            {
                if (banInformation.Game.Any(x => x.GameAccountId == account.Id && x.IsActive))
                {
                    account.Flag |= GameAccountFlag.Banned;
                    continue;
                }
            }
            return accounts;
        }

        public async Task<BanReport.BanTerm> GetUserBanFactorAsync(Guid uuid, int reason, int gameAccountId = 0)
        {
            ApplicationUser user = await userManager.FindByIdAsync(uuid.ToString());
            if (user == null)
            {
                return null;
            }

            var factor = await QueryNextBanTermAsync(user, reason, gameAccountId);
            if (factor == null)
            {
                return null;
            }

            return factor;
        }

        public async Task<bool> DoBanAsync(ClaimsPrincipal userPrincipal, Guid userId, int gameAccountId, int reasonId, string description)
        {
            ApplicationUser user = await userManager.FindByIdAsync(userId.ToString());
            if (user == null)
            {
                throw new NotFoundException(stringLocalizer.GetString("UserNotFound"));
            }

            var reason = await applicationDbContext.ApplicationUserBanReasons.AsQueryable().FirstOrDefaultAsync(x => x.Id == reasonId);
            if (reason == null)
            {
                return false;
            }

            var info = await GetBanInformationAsync(user);
            if (info == null)
            {
                throw new BadRequestException(stringLocalizer.GetString("NoUserData"));
            }

            if (gameAccountId != 0 && reason.Type == 0)
            {
                throw new BadRequestException(stringLocalizer.GetString("InvalidBanType"));
            }

            var term = await QueryNextBanTermAsync(user, reasonId, gameAccountId);
            if (term == null)
            {
                throw new BadRequestException(stringLocalizer.GetString("InvalidBanType"));
            }

            if (reason.Type == 0)
            {
                if (info.IsAccountBanned)
                {
                    throw new BadRequestException(stringLocalizer.GetString("UserAlreadyBanned"));
                }
            }
            else
            {
                var gameAccountBanInfo = info.Game.FirstOrDefault(x => x.IsActive && x.GameAccountId == gameAccountId);
                if (gameAccountBanInfo != null)
                {
                    throw new BadRequestException(stringLocalizer.GetString("UserAlreadyBanned"));
                }
            }

            ApplicationUser admin = await userManager.GetUserAsync(userPrincipal);
            if (admin == null)
            {
                throw new UnauthorizedException();
            }

            if (!userPrincipal.IsInRole(ROLE_BAN_USERS))
            {
                throw new ForbiddenException();
            }

            var principal = await signInManager.CreateUserPrincipalAsync(user);

            if (!userPrincipal.IsInRole(ROLE_SUPER_ADMINISTRATOR) && principal.IsInRole(ROLE_SUPER_ADMINISTRATOR))
            {
                throw new ForbiddenException(stringLocalizer.GetString("UserCannotBeBanned"));
            }

            ConquerAccount gameAccount = null;
            if (gameAccountId != 0)
            {
                gameAccount = await gameAccountService.FindByIdAsync(gameAccountId);
                if (gameAccount == null)
                {
                    throw new BadRequestException(stringLocalizer.GetString("GameAccountNotFound"));
                }
            }

            DateTime expireTime;
            if (term.IsPermanent)
            {
                expireTime = new DateTime(2199, 12, 31, 23, 59, 59);
            }
            else
            {
                expireTime = DateTime.Now.AddMinutes(term.Minutes);
            }

            ApplicationUserBan ban = new()
            {
                AccountId = user.Id,
                BannedBy = admin.Id,
                BanTime = DateTime.Now,
                ExpireTime = expireTime,
                GameAccountId = gameAccountId != 0 ? gameAccountId : null,
                ReasonMessage = description,
                Reason = reason
            };

            applicationDbContext.ApplicationUserBans.Add(ban);
            try
            {
                int result = await applicationDbContext.SaveChangesAsync();
                if (result <= 0)
                {
                    throw new InternalServerErrorException(stringLocalizer.GetString("DatabaseError"));
                }
            }
            catch (Exception ex)
            {
                if (ex is not HttpResponseException)
                {
                    throw new InternalServerErrorException(ex);
                }

                throw;
            }

            List<ConquerAccount> lockAccounts = new();
            if (reason.Type == 0)
            {
                var gameAccounts = await gameAccountService.FindByParentAsync(userId);
                foreach (var account in gameAccounts)
                {
                    account.Flag |= (int)GameAccountFlag.Banned;
                    lockAccounts.Add(account);
                }
                applicationDbContext.GameAccounts.UpdateRange(gameAccounts);
                await applicationDbContext.SaveChangesAsync();

                await eventLogger.LogAsync(EventLogger.LogType.UserBan, true, ban);
            }
            else
            {
                gameAccount.Flag |= (int)GameAccountFlag.Banned;
                applicationDbContext.GameAccounts.Update(gameAccount);
                await applicationDbContext.SaveChangesAsync();

                lockAccounts.Add(gameAccount);
                await eventLogger.LogAsync(EventLogger.LogType.UserGameAccountBan, true, ban);
            }

            string subject;
            string message;
            if (reason.Type == 0)
            {
                subject = stringLocalizer.GetString("AccountBanEmailSubject");
                message = stringLocalizer.GetString("AccountBanEmailMessage", stringLocalizer.GetString(reason.Name), expireTime.ToString());
            }
            else
            {
                subject = stringLocalizer.GetString("GameAccountBanEmailSubject", gameAccount.UserName);
                message = stringLocalizer.GetString("GameAccountBanEmailMessage", gameAccount.UserName, stringLocalizer.GetString(reason.Name), expireTime.ToString());
            }

            if (!string.IsNullOrEmpty(subject) && !string.IsNullOrEmpty(message))
            {
                await emailSender.SendEmailAsync(user.Email, subject, message);
            }

            await realmSocketService.BroadcastBanAsync(lockAccounts, userPrincipal.FindFirst(JwtClaimTypes.GivenName)?.Value ?? "Game Master", stringLocalizer.GetString(reason.Name));
            return true;
        }

        public async Task<bool> CancelBanAsync(ClaimsPrincipal principal, Guid banUuid)
        {
            ApplicationUser admin = await userManager.GetUserAsync(principal);
            if (admin == null)
            {
                throw new UnauthorizedException();
            }

            if (!principal.IsInRole(ROLE_BAN_USERS) || !principal.IsInRole(ROLE_CANCEL_BAN))
            {
                throw new ForbiddenException();
            }

            return await RemoveBanAsync(banUuid);
        }

        public async Task<bool> ForgiveBanAsync(ClaimsPrincipal principal, Guid banUuid)
        {
            ApplicationUser admin = await userManager.GetUserAsync(principal);
            if (admin == null)
            {
                throw new UnauthorizedException();
            }

            if (!principal.IsInRole(ROLE_BAN_USERS) || !principal.IsInRole(ROLE_FORGIVE_BAN))
            {
                throw new ForbiddenException();
            }

            return await RemoveBanAsync(banUuid);
        }

        public async Task<bool> FalsePositiveBanAsync(ClaimsPrincipal principal, Guid banUuid)
        {
            ApplicationUser admin = await userManager.GetUserAsync(principal);
            if (admin == null)
            {
                throw new UnauthorizedException();
            }

            if (!principal.IsInRole(ROLE_BAN_USERS) || !principal.IsInRole(ROLE_FALSE_POSITIVE_BAN))
            {
                throw new ForbiddenException();
            }

            return await RemoveBanAsync(banUuid);
        }

        public async Task RemoveExpiredBanFromGameAccountAsync(Guid banUuid)
        {
            var ban = await applicationDbContext.ApplicationUserBans.AsQueryable().FirstOrDefaultAsync(x => x.Id == banUuid);
            if (ban == null)
            {
                throw new NotFoundException(stringLocalizer.GetString("BanNotFound"));
            }

            BanFlags flag = (BanFlags)ban.Flags;
            if (!flag.HasFlag(BanFlags.Removed)
                && !flag.HasFlag(BanFlags.Forgiven)
                && !flag.HasFlag(BanFlags.FalsePositive))
            {
                // ban has not been removed. removed bans should not get to this, but let's give it a chance
                if (ban.ExpireTime > DateTime.Now)
                {
                    throw new BadRequestException(stringLocalizer.GetString("BanNotExpired"));
                }
            }

            ApplicationUser targetUser = await userManager.FindByIdAsync(ban.AccountId.ToString());
            if (targetUser == null)
            {
                throw new NotFoundException(stringLocalizer.GetString("UserNotFound"));
            }

            if (!ban.GameAccountId.HasValue)
            {
                return;
            }

            var accountBanInformation = await GetBanInformationAsync(targetUser);
            if (accountBanInformation != null && accountBanInformation.IsAccountBanned)
            {
                throw new BadRequestException(stringLocalizer.GetString("SystemUserIsBannedCannotUnban"));
            }

            ConquerAccount gameAccount = await gameAccountService.FindByIdAsync(ban.GameAccountId.Value);
            if (gameAccount == null)
            {
                throw new BadRequestException(stringLocalizer.GetString("GameAccountNotFound"));
            }

            gameAccount.Flag &= ~((int)GameAccountService.GameAccountFlag.Banned);
            applicationDbContext.GameAccounts.Update(gameAccount);
            await applicationDbContext.SaveChangesAsync();
        }

        private async Task<bool> RemoveBanAsync(Guid banUuid)
        {
            var ban = await applicationDbContext.ApplicationUserBans.AsQueryable().FirstOrDefaultAsync(x => x.Id == banUuid);
            if (ban == null)
            {
                throw new NotFoundException(stringLocalizer.GetString("BanNotFound"));
            }

            ApplicationUser targetUser = await userManager.FindByIdAsync(ban.AccountId.ToString());
            if (targetUser == null)
            {
                throw new NotFoundException(stringLocalizer.GetString("UserNotFound"));
            }

            ConquerAccount gameAccount = null;
            if (ban.GameAccountId.HasValue)
            {
                gameAccount = await gameAccountService.FindByIdAsync(ban.GameAccountId.Value);
                if (gameAccount == null)
                {
                    throw new BadRequestException(stringLocalizer.GetString("GameAccountNotFound"));
                }
            }

            BanFlags flag = (BanFlags)ban.Flags;
            if (flag.HasFlag(BanFlags.Removed) || flag.HasFlag(BanFlags.Forgiven) || flag.HasFlag(BanFlags.FalsePositive))
            {
                throw new BadRequestException(stringLocalizer.GetString("AlreadyRemovedBan"));
            }

            ban.Flags |= (int)BanFlags.Removed;
            applicationDbContext.ApplicationUserBans.Update(ban);
            await applicationDbContext.SaveChangesAsync();

            if (!ban.GameAccountId.HasValue)
            {
                await RemoveSystemAccountBanAsync(targetUser);
            }
            else if (gameAccount != null)
            {
                gameAccount.Flag &= ~((int)GameAccountService.GameAccountFlag.Banned);
                applicationDbContext.GameAccounts.Update(gameAccount);
                await applicationDbContext.SaveChangesAsync();
            }

            return true;
        }

        private async Task RemoveSystemAccountBanAsync(ApplicationUser applicationUser)
        {
            var banInformation = await GetBanInformationAsync(applicationUser);
            if (banInformation == null)
            {
                // ??? should not happen
                return;
            }

            if (banInformation.IsAccountBanned)
            {
                // first remove the system account ban!
                return;
            }

            var gameAccounts = await gameAccountService.FindByParentAsync(applicationUser.Id);
            foreach (var gameAccount in gameAccounts)
            {
                // we should not remove account bans if they've been banned directly!!!
                var accountBan = banInformation.Game.FirstOrDefault(x => x.IsActive && x.GameAccountId == gameAccount.Id);
                if (accountBan != null)
                {
                    // this account has been banned by something else. keep current ban
                    continue;
                }

                gameAccount.Flag &= ~((int)GameAccountService.GameAccountFlag.Banned);
            }
            applicationDbContext.GameAccounts.UpdateRange(gameAccounts);
            await applicationDbContext.SaveChangesAsync();
        }

        public class UserBanModel
        {
            public Guid UserId { get; set; }
            public int GameAccountId { get; set; }
            public int Reason { get; set; }
            public string Description { get; set; }
        }
    }
}
