﻿using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Sockets.Piglet;
using Canyon.GM.Panel.Sockets.Piglet.Packets;
using Canyon.Network.Packets;
using Canyon.Network.Packets.Piglet;
using System.Collections.Concurrent;

namespace Canyon.GM.Panel.Services
{
    public sealed class RealmSocketService
    {
        private static ConcurrentDictionary<Guid, PigletActor> connectedRealms = new();
        private readonly ILogger<RealmSocketService> logger;

        public RealmSocketService(
            ILogger<RealmSocketService> logger
        )
        {
            this.logger = logger;
        }

        public bool AddServer(PigletActor actor)
        {
            return connectedRealms.TryAdd(actor.Guid, actor);
        }

        public void RemoveServer(Guid realmId)
        {
            connectedRealms.TryRemove(realmId, out _);
        }

        public bool IsRealmConnected(Guid realmId)
        {
            return connectedRealms.ContainsKey(realmId);
        }

        public PigletActor GetServer(Guid realmId)
        {
            return connectedRealms.TryGetValue(realmId, out var result) ? result : null;
        }

        public Task SendToRealmAsync(Guid realmId, IPacket msg)
        {
            if (connectedRealms.TryGetValue(realmId, out var realm))
            {
                return realm.SendAsync(msg);
            }
            return Task.CompletedTask;
        }

        public async Task BroadcastBanAsync(List<ConquerAccount> lockAccounts, string gameMaster, string reason)
        {
            foreach (var realm in connectedRealms.Values)
            {
                foreach (var account in lockAccounts)
                {
                    await SendToRealmAsync(realm.Guid, new MsgPigletUserBan
                    {
                        Data = new MsgPigletUserBan<PigletActor>.UserBanData
                        {
                            UserId = (uint)account.Id,
                            GameMaster = gameMaster,
                            Reason = reason
                        }
                    });
                }
            }
        }

        public async Task BroadcastAsync(IPacket msg)
        {
            foreach (var realm in connectedRealms.Values)
            {
                await SendToRealmAsync(realm.Guid, msg);
            }
        }

        public async Task PingTimerAsync()
        {
            foreach (var actor in connectedRealms.Values)
            {
                if (!actor.Socket.Connected) 
                {
                    RemoveServer(actor.Guid);
                    continue;
                }

                await actor.SendAsync(new MsgPigletPing());
            }
        }
    }
}
