﻿using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Database;
using MySqlConnector;
using Microsoft.EntityFrameworkCore;
using Canyon.GM.Panel.Models.Rest.Account;

namespace Canyon.GM.Panel.Services
{
    public class GameAccountService
    {
        public const int GAME_ACCOUNT_LIMIT = 5;

        private readonly ApplicationDbContext applicationDbContext;
        private readonly RealmService realmService;

        public GameAccountService(
            ApplicationDbContext applicationDbContext,
            RealmService realmService
            )
        {
            this.applicationDbContext = applicationDbContext;
            this.realmService = realmService;
        }

        public Task<ConquerAccount> FindByIdAsync(int id)
        {
            return applicationDbContext.GameAccounts
                .Include(x => x.Parent)
                .Include(x => x.Authority)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<ConquerAccount> FindByUserNameAsync(string userName)
        {
            return applicationDbContext.GameAccounts
                .Include(x => x.Parent)
                .Include(x => x.Authority)
                .FirstOrDefaultAsync(x => x.UserName.Equals(userName));
        }

        public Task<List<ConquerAccount>> FindByParentAsync(Guid guid)
        {
            return applicationDbContext.GameAccounts
                .Include(x => x.Parent)
                .Include(x => x.Authority)
                .Where(x => x.ParentId == guid)
                .ToListAsync();
        }

        public int CountByParent(Guid uuid)
        {
            return applicationDbContext.GameAccounts
                .Include(x => x.Parent)
                .Include(x => x.Authority)
                .Where(x => x.ParentId == uuid)
                .Count();
        }

        public Task<List<ConquerAccount>> FindByMultipleIdAsync(params int[] ids)
        {
            return applicationDbContext.GameAccounts.Where(x => ids.Any(y => y == x.Id)).ToListAsync();
        }

        public async Task<List<GameAccountResponse>> GetGameAccountsByParentAsync(Guid parent)
        {
            var accounts = (await FindByParentAsync(parent))
                .OrderBy(x => x.Id)
                .Select(account => new GameAccountResponse
                {
                    Id = account.Id,
                    Username = account.UserName,
                    Authority = account.AuthorityId,
                    AuthorityName = account.Authority.Name,
                    IpAddress = account.IpAddress,
                    MacAddress = account.MacAddress,
                    ParentId = account.ParentId
                })
                .Where(x => !x.Flag.HasFlag(GameAccountFlag.Deleted))
            .ToList();
            return accounts;
        }

        public async Task<List<GameAccountResponse>> FetchConquerAccountsAsync(List<GameAccountResponse> accounts)
        {
            var realms = await realmService.QueryRealmsAsync(0, int.MaxValue);
            var accountList = new MySqlParameter("@idList", string.Join(',', accounts.Select(x => x.Id.ToString())));
            foreach (var realm in realms.Where(x => x.Production))
            {
                var connectionString = await realmService.GetRealmConnectionStringAsync(realm.RealmName);
                using var serverDbContext = new RealmDbContext(connectionString);
                var userList = serverDbContext.ConquerGameCharacters.FromSqlRaw("call QueryUserByAccountIDs(@idList)", accountList).ToList();

                foreach (var account in accounts)
                {
                    var user = userList.FirstOrDefault(x => x.AccountIdentity == account.Id);
                    if (user != null)
                    {
                        account.Characters.Add(new GameAccountResponse.ConquerGameCharacterResponse
                        {
                            RealmID = realm.RealmID,
                            RealmName = realm.RealmName,
                            Identity = user.Identity,
                            Name = user.Name,
                            Level = user.Level,
                            Metempsychosis = user.Rebirths,
                            Money = user.Silver,
                            ConquerPoints = user.ConquerPoints,
                            ConquerPointsMono = user.ConquerPointsBound
                        });
                    }
                }
            }
            return accounts;
        }

        public Task SaveAsync(ConquerAccount conquerAccount)
        {
            applicationDbContext.Update(conquerAccount);
            return applicationDbContext.SaveChangesAsync();
        }

        [Flags]
        public enum GameAccountFlag
        {
            None,
            Locked = 0x1,
            Banned = 0x2,
            FlaggedForExclusion = 0x4,
            Deleted = 0x8
        }
    }
}
