﻿using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Local;
using Microsoft.EntityFrameworkCore;

namespace Canyon.GM.Panel.Services
{
    public sealed class PurchaseService
    {
        private readonly ILogger<PurchaseService> logger;
        private readonly ApplicationDbContext applicationDbContext;

        public PurchaseService(
            ILogger<PurchaseService> logger,
            ApplicationDbContext applicationDbContext
            )
        {
            this.logger = logger;
            this.applicationDbContext = applicationDbContext;
        }

        public Task<int> CountOrdersByUserAsync(Guid userId)
        {
            return applicationDbContext.PaymentsOrders.Where(x => x.UserId == userId).CountAsync();
        }

        public Task<decimal> TotalUserExpensesAsync(Guid userId)
        {
            return applicationDbContext.PaymentsOrders
                .Where(x => x.UserId == userId && (x.Status == PaymentsOrder.PaymentOrderStatus.Paid || x.Status == PaymentsOrder.PaymentOrderStatus.Delivered))
                .SumAsync(x => x.Price);
        }

        public Task<decimal> TotalUserPendingExpensesAsync(Guid userId)
        {
            return applicationDbContext.PaymentsOrders
                .Where(x => x.UserId == userId && (x.Status == PaymentsOrder.PaymentOrderStatus.Pending))
                .SumAsync(x => x.Price);
        }

        public Task<decimal> TotalUserRefundsAsync(Guid userId)
        {
            return applicationDbContext.PaymentsOrders
                .Where(x => x.UserId == userId && (x.Status == PaymentsOrder.PaymentOrderStatus.Refunded))
                .SumAsync(x => x.Price);
        }

        public Task<PaymentsOrder> GetOrderByIdAsync(Guid orderId)
        {
            return applicationDbContext.PaymentsOrders
                .Include(x => x.User)
                .Include(x => x.CreditCards).ThenInclude(x => x.CardType)
                .Include(x => x.Method)
                .Include(x => x.Items).ThenInclude(x => x.Product)
                .FirstOrDefaultAsync(x => x.Id == orderId);
        }

        public Task<List<PaymentsOrder>> QueryUserPurchasesAsync(Guid userId, int pageIndex = 0, int indexesPerPage = int.MaxValue)
        {
            pageIndex = Math.Max(0, pageIndex);
            return applicationDbContext.PaymentsOrders
                .Where(x => x.UserId == userId)
                .Include(x => x.User)
                .Include(x => x.CreditCards)
                .Include(x => x.Method)
                .Include(x => x.Items)
                .Skip(pageIndex * indexesPerPage)
                .Take(indexesPerPage)
                .ToListAsync();
        }

        public bool IsFirstCreditAvailable(int accountId, Guid realmId)
        {
            var creditedCards = applicationDbContext.CreditCards.Where(x => x.UsedByGameAccount == accountId).ToList();
            if (creditedCards.Count == 0) 
            {
                // nothing credited, no first credit!
                return false;
            }
            return applicationDbContext.ConquerAccountFirstCreditClaims.FirstOrDefault(x => x.AccountID == accountId && x.RealmID == realmId) == null;
        }

        public Task SetFirstCreditAsync(int accountId, Guid realmId)
        {
            applicationDbContext.ConquerAccountFirstCreditClaims.Add(new ConquerAccountFirstCreditClaim
            {
                AccountID = accountId,
                RealmID = realmId,
                ClaimDate = DateTime.Now
            });
            return applicationDbContext.SaveChangesAsync();
        }

        public async Task<List<PaymentsOrder>> QueryAccountPurchasesAsync(int accountId)
        {
            var applicationUser = applicationDbContext.GameAccounts.FirstOrDefault(x => x.Id == accountId);
            if (applicationUser == null)
            {
                return new();
            }
            return await applicationDbContext.PaymentsOrders.Where(x => x.UserId == applicationUser.ParentId && x.Status == PaymentsOrder.PaymentOrderStatus.Delivered).ToListAsync();
        }
    }
}
