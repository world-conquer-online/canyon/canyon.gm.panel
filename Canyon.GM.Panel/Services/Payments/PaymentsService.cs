﻿using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Services.Identity;
using IdentityModel;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using static Canyon.GM.Panel.Database.Entities.Local.PaymentsOrder;
using System.Text;
using System.Security.Claims;
using Canyon.GM.Panel.Helpers;
using Canyon.GM.Panel.Services.Core;
using Canyon.GM.Panel.Models.Exceptions;

namespace Canyon.GM.Panel.Services.Payments
{
    public class PaymentsService
    {
        private const int REFUND_BAN_TYPE = 24;

        private readonly ILogger<PaymentsService> logger;
        private readonly IEmailSender emailSender;
        private readonly ApplicationDbContext applicationDbContext;
        private readonly UserManager userManager;
        private readonly BanService banService;
        private readonly CreditCardService creditCardService;
        private readonly SignInManager signInManager;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IStringLocalizerFactory stringLocalizerFactory;
        private readonly MercadoPagoService mercadoPagoService;

        public PaymentsService(ILogger<PaymentsService> logger, 
            IEmailSender emailSender, 
            ApplicationDbContext applicationDbContext, 
            UserManager userManager, 
            BanService banService, 
            CreditCardService creditCardService, 
            SignInManager signInManager, 
            IWebHostEnvironment hostingEnvironment, 
            IStringLocalizerFactory stringLocalizerFactory,
            MercadoPagoService mercadoPagoService)
        {
            this.logger = logger;
            this.emailSender = emailSender;
            this.applicationDbContext = applicationDbContext;
            this.userManager = userManager;
            this.banService = banService;
            this.creditCardService = creditCardService;
            this.signInManager = signInManager;
            this.hostingEnvironment = hostingEnvironment;
            this.stringLocalizerFactory = stringLocalizerFactory;
            this.mercadoPagoService = mercadoPagoService;
        }

        /// <summary>
        /// For success or pending responses. This is not for notifications update.
        /// </summary>
        public async Task<PurchaseConfirmationResult> PushMercadoPagoPaymentInformationAsync(Guid orderId, long paymentId)
        {
            var paymentData = await mercadoPagoService.QueryPaymentInformationAsync(paymentId);
            if (paymentData == null)
            {
                logger.LogWarning("Payment [{}] not found.", paymentId);
                // payment does not exist
                return new PurchaseConfirmationResult
                {
                    Status = PaymentOrderStatus.Denied
                };
            }

            if (!Guid.TryParse(paymentData.ExternalReference, out var externalReference))
            {
                logger.LogWarning("External reference [{}] for payment [{}] invalid format", paymentData.ExternalReference, paymentId);
                // invalid order id
                return new PurchaseConfirmationResult
                {
                    Status = PaymentOrderStatus.Denied
                };
            } // ?

            var referenceOrder = await GetOrderAsync(externalReference);
            if (referenceOrder == null)
            {
                logger.LogInformation("Order not found [{}]", externalReference);
                return new PurchaseConfirmationResult
                {
                    Status = PaymentOrderStatus.Denied
                };
            }

            PaymentsMercadoPago paymentsMercadoPago = applicationDbContext.PaymentsMercadoPago.FirstOrDefault(x => x.PaymentId == paymentId);
            if (paymentsMercadoPago != null)
            {
                paymentsMercadoPago.LastStatus = MercadoPagoService.PaymentStatusToOrderStatus(paymentData.Status);
                paymentsMercadoPago.UpdatedAt = DateTime.Now;
            }
            else
            {
                paymentsMercadoPago = new PaymentsMercadoPago
                {
                    OrderId = externalReference,
                    PaymentId = paymentId,
                    LastStatus = MercadoPagoService.PaymentStatusToOrderStatus(paymentData.Status),
                    CreatedAt = DateTime.Now
                };
                applicationDbContext.PaymentsMercadoPago.Add(paymentsMercadoPago);
            }

            if (paymentsMercadoPago.LastStatus == PaymentOrderStatus.Paid)
            {
                if (referenceOrder.Status == PaymentOrderStatus.Refunded)
                {
                    await NotAuthorizedRefundAsync(referenceOrder);
                    referenceOrder.UpdatedAt = DateTime.Now;
                }
                else if (referenceOrder.Status != PaymentOrderStatus.Delivered)
                {
                    await DeliverOrderAsync(referenceOrder);
                    referenceOrder.UpdatedAt = DateTime.Now;
                }                
            }

            await applicationDbContext.SaveChangesAsync();
            return new PurchaseConfirmationResult
            {
                Status = paymentsMercadoPago.LastStatus,
                OrderId = externalReference
            };
        }

        public async Task<bool> RefundAsync(PaymentsOrder order, List<int> cardIds)
        {
            applicationDbContext.Database.BeginTransaction();
            try
            {
                var cards = await creditCardService.GetByTransactionIdAsync(order.Id);
                foreach (var card in cards)
                {
                    if (cardIds.All(x => x != card.Id))
                    {
                        throw new BadRequestException($"Card [{card}] does not belong to order [{order.Id}].");
                    }

                    if (card.UsedByGameAccount != null || card.UsedBy != null) // if game account is not null, used by will not be null (must not at least)
                    {
                        throw new BadRequestException($"Card [{card}] of order [{order.Id}] has already been used and cannot be refunded.");
                    }

                    card.CancelledAt = DateTime.Now;
                }

                order.Status = PaymentOrderStatus.Refunded;
                await applicationDbContext.SaveChangesAsync();
                await applicationDbContext.Database.CommitTransactionAsync();
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error while rolling back payment!!");
                await applicationDbContext.Database.RollbackTransactionAsync();
                return false;
            }
        }

        public async Task NotAuthorizedRefundAsync(PaymentsOrder order)
        {
            applicationDbContext.Database.BeginTransaction();
            try
            {
                var cards = await creditCardService.GetByTransactionIdAsync(order.Id);
                foreach (var card in cards)
                {
                    if (card.UsedByGameAccount != null || card.UsedBy != null) // if game account is not null, used by will not be null (must not at least)
                    {
                        await banService.DoBanAsync(null, card.UsedBy.Value, card.UsedByGameAccount ?? 0, REFUND_BAN_TYPE, "Player has been banned due to unauthorized refund."); // Refund by credit card fraud? Refund must call support to avoid ban
                    }

                    card.CancelledAt = DateTime.Now;
                }

                order.Status = PaymentOrderStatus.Refunded;
                await applicationDbContext.SaveChangesAsync();
                await applicationDbContext.Database.CommitTransactionAsync();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error while rolling back payment!!");
                await applicationDbContext.Database.RollbackTransactionAsync();
            }
        }

        public async Task<bool> DeliverOrderAsync(PaymentsOrder order)
        {
            applicationDbContext.Database.BeginTransaction();
            try
            {
                List<CreditCard> cards = new();
                // deliver
                foreach (var item in order.Items)
                {
                    cards.AddRange(await creditCardService.GenerateCreditCardsAsync(order.User, item.Product.CardTypeId, item.Amount, order.Id));
                }

                order.Status = PaymentOrderStatus.Delivered;
                await applicationDbContext.SaveChangesAsync();
                await applicationDbContext.Database.CommitTransactionAsync();

                var claimsPrincipal = await signInManager.CreateUserPrincipalAsync(order.User);
                if (claimsPrincipal == null)
                {
                    logger.LogWarning("Could not build user principal for order {}", order.Id);
                    return true;
                }

                var stringLocalizer = stringLocalizerFactory.GetLocalizer(claimsPrincipal.GetCurrentLocale());

                string userName = claimsPrincipal.FindFirstValue(JwtClaimTypes.Name) ?? order.User.UserName;
                const string tableBodyFormat = "<tr><td>{0}</td><td>{1}</td><td>{2:N2}</td></tr>";
                const string tableFooterFormat = "<tr><td colspan=\"2\"><b>Total:</b></td><td>{0:N2}</td></tr>";
                string tableBody = "";
                foreach (var item in order.Items)
                {
                    tableBody += string.Format(tableBodyFormat, item.Product.Name, item.Amount, item.Price);
                }
                string tableFooter = string.Format(tableFooterFormat, order.Price);

                string emailSubject = stringLocalizer.GetString("EmailPurchaseCompleteTitle");
                string emailBody = stringLocalizer.GetString("EmailPurchaseCompleteContent", userName, tableBody, tableFooter);

                string path = Path.Combine(hostingEnvironment.WebRootPath, "templates", "email", "purchase", $"template.{claimsPrincipal.GetCurrentLocale()}.html");
                if (File.Exists(path))
                {
                    using var reader = new StreamReader(path, Encoding.UTF8);
                    emailBody = reader.ReadToEnd();
                    emailBody = emailBody.Replace("%TABLE_BODY%", tableBody)
                        .Replace("%TABLE_FOOTER%", tableFooter)
                        .Replace("%USER_NAME%", userName);
                }

                await emailSender.SendEmailAsync(order.User.Email, emailSubject, emailBody);
                return true;
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Error on deliver order!!!! {}", ex.Message);
                await applicationDbContext.Database.RollbackTransactionAsync();
                return false;
            }
        }

        public Task<PaymentsOrder> GetOrderAsync(Guid orderId)
        {
            return applicationDbContext.PaymentsOrders
                .Include(x => x.User)
                .Include(x => x.Method)
                .Include(x => x.Items)
                    .ThenInclude(x => x.Product)
                .FirstOrDefaultAsync(x => x.Id == orderId);
        }

        public Task<PaymentsOrder> GetOrderResumeAsync(Guid orderId)
        {
            return applicationDbContext.PaymentsOrders
                .Include(x => x.User)
                .Include(x => x.Method)
                .Include(x => x.Items)
                    .ThenInclude(x => x.Product)
                .FirstOrDefaultAsync(x => x.Id == orderId);
        }

        public async Task<List<PurchaseListResultModel>> GetOrdersByUserAsync(Guid userId)
        {
            List<PurchaseListResultModel> result = new List<PurchaseListResultModel>();
            var orders = await applicationDbContext
                .PaymentsOrders
                .Include(x => x.User)
                .Include(x => x.Method)
                .Where(x => x.UserId == userId)
                .ToListAsync();

            foreach (var order in orders)
            {
                result.Add(new PurchaseListResultModel
                {
                    OrderId = order.Id,
                    Status = (PaymentOrderStatus)order.Status,
                    PurchaseTime = order.CreatedAt,
                    LastUpdateTime = order.UpdatedAt,
                    Total = order.Price
                });
            }

            return result;
        }

        public struct PurchaseConfirmationResult
        {
            public PaymentOrderStatus Status { get; set; }
            public Guid OrderId { get; set; }
        }

        public class PurchaseListResultModel
        {
            public Guid OrderId { get; set; }
            public DateTime PurchaseTime { get; set; }
            public DateTime? LastUpdateTime { get; set; }
            public PaymentOrderStatus Status { get; set; }
            public decimal Total { get; set; }
        }
    }
}
