﻿using MercadoPago.Client.Payment;
using MercadoPago.Resource.Payment;
using Canyon.GM.Panel.Database;
using static Canyon.GM.Panel.Database.Entities.Local.PaymentsOrder;
using Canyon.GM.Panel.Database.Entities.Local;
using Microsoft.EntityFrameworkCore;

namespace Canyon.GM.Panel.Services.Payments
{
    public sealed class MercadoPagoService
    {
        private readonly ILogger<MercadoPagoService> logger;
        private readonly IConfiguration configuration;
        private readonly ApplicationDbContext applicationDbContext;
        private readonly IStringLocalizer stringLocalizer;

        public MercadoPagoService(
            ILogger<MercadoPagoService> logger,
            IConfiguration configuration,
            ApplicationDbContext applicationDbContext,
            IStringLocalizer stringLocalizer
            )
        {
            this.logger = logger;
            this.configuration = configuration;
            this.applicationDbContext = applicationDbContext;
            this.stringLocalizer = stringLocalizer;
        }

        public async Task<Payment> QueryPaymentInformationAsync(long paymentId)
        {
            try
            {
                PaymentClient paymentClient = new();
                return await paymentClient.GetAsync(paymentId);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error when communicating with Mercado Pago. {}", ex.Message);
                return null;
            }
        }

        public Task<PaymentsMercadoPago> QueryMercadoPagoLocalHistoryAsync(Guid orderId)
        {
            return applicationDbContext.PaymentsMercadoPago.FirstOrDefaultAsync(x => x.OrderId == orderId);
        }

        public static PaymentOrderStatus PaymentStatusToOrderStatus(string status)
        {
            switch (status.ToLower())
            {
                case "approved": return PaymentOrderStatus.Paid;
                case "refunded":
                case "charged_back": return PaymentOrderStatus.Refunded;
                case "cancelled": return PaymentOrderStatus.Cancelled;
                case "rejected": return PaymentOrderStatus.Denied;
                default: return PaymentOrderStatus.Pending;
            }
        }
    }
}
