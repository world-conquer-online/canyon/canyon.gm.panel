﻿using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Services.Identity;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace Canyon.GM.Panel.Services
{
    public class CreditCardService
    {
        private readonly ILogger<CreditCardService> logger;
        private readonly ApplicationDbContext applicationDbContext;
        private readonly UserManager userManager;
        private readonly BanService banService;
        private readonly RealmService realmService;
        private readonly GameAccountService gameAccountService;

        public CreditCardService(
            ILogger<CreditCardService> logger,
            ApplicationDbContext applicationDbContext,
            UserManager userManager,
            BanService banService,
            RealmService realmService,
            GameAccountService gameAccountService
            )
        {
            this.logger = logger;
            this.applicationDbContext = applicationDbContext;
            this.userManager = userManager;
            this.banService = banService;
            this.realmService = realmService;
            this.gameAccountService = gameAccountService;
        }

        public Task<CreditCardType> FindCardTypeAsync(int cardType)
        {
            return applicationDbContext.CreditCardTypes.FirstOrDefaultAsync(x => x.Id == cardType);
        }

        public Task<CreditCard> FindCardByUUIDAsync(Guid cardId)
        {
            return applicationDbContext.CreditCards
                .Include(x => x.CardType)
                .FirstOrDefaultAsync(x => x.CardUUID == cardId);
        }

        public Task<List<CreditCard>> GetByTransactionIdAsync(Guid transactionId)
        {
            return applicationDbContext.CreditCards
                .Include(x => x.CardType)
                .Include(x => x.Transaction)
                .Where(x => x.TransactionId == transactionId)
                .ToListAsync();
        }

        public async Task<List<CreditCard>> GenerateCreditCardsAsync(ApplicationUser user, int cardTypeID, int amount, Guid? transactionId = null)
        {
            List<CreditCard> creditCards = new List<CreditCard>();
            if (amount <= 0)
            {
                logger.LogWarning("Invalid number of cards to be created! Must be higher than 0");
                return creditCards;
            }

            if (user == null)
            {
                logger.LogWarning("Cannot create card for null user.");
                return creditCards;
            }

            CreditCardType cardType = await FindCardTypeAsync(cardTypeID);
            if (cardType == null)
            {
                logger.LogError("Invalid card type.");
                return creditCards;
            }

            for (int i = 0; i < amount; i++)
            {
                creditCards.Add(new CreditCard
                {
                    CardTypeId = cardType.Id,
                    TransactionId = transactionId,
                    CardUUID = Guid.NewGuid(),
                    CreatedBy = user.Id,
                    CreatedAt = DateTime.Now,
                });
            }

            applicationDbContext.AddRange(creditCards);
            await applicationDbContext.SaveChangesAsync();
            return creditCards;
        }
    }
}
