﻿using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Local;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Canyon.GM.Panel.Services.Identity
{
    public class ApplicationUserStore : UserStore<
        ApplicationUser,
        ApplicationRole,
        ApplicationDbContext,
        Guid,
        ApplicationUserClaim,
        ApplicationUserRole,
        ApplicationUserLogin,
        ApplicationUserToken,
        ApplicationRoleClaim
    >
    {
        private readonly ILogger<ApplicationUserStore> logger;

        public ApplicationUserStore(ILogger<ApplicationUserStore> logger,
                                    ApplicationDbContext dbContext)
            : base(dbContext)
        {
            this.logger = logger;
        }

        public override async Task<IdentityResult> CreateAsync(ApplicationUser user,
                                                               CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            try
            {
                if (await Context.Users.AnyAsync(usr => usr.NormalizedEmail.Equals(user.NormalizedEmail),
                                                 cancellationToken))
                    return IdentityResult.Failed(new IdentityError
                    {
                        Code = "EMAIL_IN_USE",
                        Description = "EmailInUse"
                    });

                if (await Context.Users.AnyAsync(usr => usr.NormalizedUserName.Equals(user.NormalizedUserName),
                                                 cancellationToken))
                    return IdentityResult.Failed(new IdentityError
                    {
                        Code = "USERNAME_ALREADY_EXISTS",
                        Description = "UserNameInUse"
                    });

                Context.Users.Add(user);
                await Context.SaveChangesAsync(cancellationToken);
                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                return IdentityResult.Failed(new IdentityError
                {
                    Code = ex.HResult.ToString("X08"),
                    Description = ex.InnerException.Message
                });
            }
        }
    }
}