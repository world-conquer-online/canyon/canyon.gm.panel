﻿using System.Security.Claims;
using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Local;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Canyon.GM.Panel.Services.Identity
{
    public class UserManager : UserManager<ApplicationUser>
    {
        private readonly ApplicationDbContext context;

        public UserManager(IUserStore<ApplicationUser> store,
                           IOptions<IdentityOptions> optionsAccessor,
                           IPasswordHasher<ApplicationUser> passwordHasher,
                           IEnumerable<IUserValidator<ApplicationUser>> userValidators,
                           IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators,
                           ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors,
                           IServiceProvider services,
                           ApplicationDbContext dbContext,
                           ILogger<UserManager<ApplicationUser>> logger)
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors,
                   services, logger)
        {
            context = dbContext;
        }

        public override async Task<IList<Claim>> GetClaimsAsync(ApplicationUser user)
        {
            IList<Claim> result = await base.GetClaimsAsync(user);

            Dictionary<string, Claim> dictClaims = result.ToDictionary(x => x.Type);
            // get role
            var roles = await context.UserRoles.Where(x => x.UserId == user.Id).ToListAsync();
            foreach (ApplicationUserRole roleUser in roles)
            {
                Guid idRole = roleUser?.RoleId ?? Guid.Empty;

                // get the claims
                List<ApplicationRoleClaim> roleClaims =
                    await context.RoleClaims.AsQueryable().Where(x => x.RoleId == idRole).ToListAsync();
                foreach (ApplicationRoleClaim roleClaim in roleClaims)
                    if (!dictClaims.ContainsKey(roleClaim.ClaimType))
                        dictClaims.Add(roleClaim.ClaimType, new Claim(roleClaim.ClaimType, roleClaim.ClaimValue));
                    else
                        dictClaims[roleClaim.ClaimType] = new Claim(roleClaim.ClaimType, roleClaim.ClaimValue);
            }

            // then we get the user claims
            List<ApplicationUserClaim> userClaims =
                await context.UserClaims.AsQueryable().Where(x => x.UserId == user.Id).ToListAsync();
            foreach (ApplicationUserClaim userClaim in userClaims)
                if (!dictClaims.ContainsKey(userClaim.ClaimType))
                    dictClaims.Add(userClaim.ClaimType, new Claim(userClaim.ClaimType, userClaim.ClaimValue));
                else
                    dictClaims[userClaim.ClaimType] = new Claim(userClaim.ClaimType, userClaim.ClaimValue);

            return dictClaims.Select(x => x.Value).ToList();
        }

        public Task<List<ApplicationUser>> GetUsersAsync(int page, int ipp,
                                                         CancellationToken cancellationToken = default)
        {
            ThrowIfDisposed();
            cancellationToken.ThrowIfCancellationRequested();

            return context.Users.Skip(Math.Max(0, page - 1) * ipp)
                           .Take(ipp)
                           .ToListAsync(cancellationToken);
        }

        public async Task<List<ApplicationUser>> GetUsersByRoleAsync(Guid roleId, int page = 0, int ipp = 10,
                                                                     CancellationToken cancellationToken = default)
        {
            ThrowIfDisposed();
            cancellationToken.ThrowIfCancellationRequested();
            Guid[] userIds = await context.UserRoles.Where(x => x.RoleId == roleId).Select(x => x.UserId)
                                          .ToArrayAsync(cancellationToken);
            return await context.Users.Where(x => userIds.Any(y => y == x.Id)).ToListAsync(cancellationToken);
        }

        public Task<ApplicationUser> FindByUserNameAsync(string userName,
                                                               CancellationToken cancellationToken = default)
        {
            ThrowIfDisposed();
            cancellationToken.ThrowIfCancellationRequested();
            userName = NormalizeName(userName);
            return context.Users.FirstOrDefaultAsync(x => x.NormalizedUserName.Equals(userName), cancellationToken: cancellationToken);
        }
    }
}