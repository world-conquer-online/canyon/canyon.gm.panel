﻿using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Local;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace Canyon.Backend.Web.Services
{
    public class RealmStatusService
    {
        public const int ServerDown = 0;
        public const int ServerBusy = 1;
        public const int ServerFull = 2;
        public const int ServerUp = 3;

        private readonly ApplicationDbContext dbContext;
        private readonly RealmSocketService realmSocketService;

        public RealmStatusService(
            ApplicationDbContext dbContext,
            RealmSocketService realmSocketService
        )
        {
            this.dbContext = dbContext;
            this.realmSocketService = realmSocketService;
        }

        public LatestRealmStatus GetLatestRealmStatus(Guid realm)
        {
            var realmStatus = dbContext
                .RealmStatuses
                .Where(x => x.RealmID.Equals(realm))
                .Include(x => x.Realm)
                .OrderByDescending(x => x.Id).FirstOrDefault();
            if (realmStatus != null)
            {
                string status = "ServerDown";
                int statusInt = ServerDown;
                int pingLastMinutes = (int)(DateTime.Now - realmStatus.Time).TotalMinutes;

                if (pingLastMinutes < 2)
                {
                    switch (realmStatus.Status)
                    {
                        case ServerBusy:
                            {
                                status = "ServerBusy";
                                statusInt = ServerBusy;
                                break;
                            }
                        case ServerFull:
                            {
                                status = "ServerFull";
                                statusInt = ServerFull;
                                break;
                            }
                        case ServerUp:
                            {
                                status = "ServerUp";
                                statusInt = ServerUp;
                                break;
                            }
                    }
                }

                return new LatestRealmStatus
                {
                    RealmName = realmStatus.Realm?.Name ?? "Error",
                    Status = status,
                    PlayersOnline = realmStatus.PlayersOnline,
                    MaxPlayersOnline = realmStatus.MaxPlayersOnline,
                    StatusInt = statusInt,
                    GmServerConnected = realmSocketService.IsRealmConnected(realm)
                };
            }
            return null;
        }

        public List<LatestRealmStatus> GetLatestRealmStatus()
        {
            List<LatestRealmStatus> result = new();
            var realms = dbContext.Realms
                .Where(x => x.Active)
                .OrderBy(x => x.Name).ToList();
            foreach (var realm in realms)
            {
                var realmStatus = dbContext
                    .RealmStatuses
                    .Where(x => x.RealmID.Equals(realm.RealmID))
                    .Include(x => x.Realm)
                    .OrderByDescending(x => x.Id).FirstOrDefault() ?? new RealmStatus
                    {
                        Realm = realm,
                        RealmID = realm.RealmID,
                        Time = DateTime.MinValue
                    };

                string status = "ServerDown";
                int statusInt = ServerDown;
                int pingLastMinutes = (int)(DateTime.Now - realmStatus.Time).TotalMinutes;

                if (pingLastMinutes < 2)
                {
                    switch (realmStatus.Status)
                    {
                        case ServerBusy:
                            {
                                status = "ServerBusy";
                                statusInt = ServerBusy;
                                break;
                            }
                        case ServerFull:
                            {
                                status = "ServerFull";
                                statusInt = ServerFull;
                                break;
                            }
                        case ServerUp:
                            {
                                status = "ServerUp";
                                statusInt = ServerUp;
                                break;
                            }
                    }
                }

                result.Add(new LatestRealmStatus
                {
                    RealmName = realmStatus.Realm?.Name ?? "Error",
                    Status = status,
                    PlayersOnline = realmStatus.PlayersOnline,
                    MaxPlayersOnline = realmStatus.MaxPlayersOnline,
                    StatusInt = statusInt,
                    GmServerConnected = realmSocketService.IsRealmConnected(realm.RealmID)
                });
            }
            return result;
        }

        public async Task<string> QueryServerStatusesForClientAsync()
        {
            StringBuilder stringBuilder = new();
            List<RealmData> realms = await dbContext.Realms
                .Where(x => x.ProductionRealm && x.Active)
                .ToListAsync();
            foreach (RealmData realm in realms)
            {
                RealmStatus currentStatus = await dbContext
                    .RealmStatuses
                    .OrderByDescending(x => x.Id)
                    .FirstOrDefaultAsync(x => x.RealmID == realm.RealmID);
                if (currentStatus == null || currentStatus.Status == ServerDown || (DateTime.Now - currentStatus.Time).Seconds >= 300)
                {
                    stringBuilder.AppendLine($"{realm.Name} {ServerDown}");
                    continue;
                }

                switch (currentStatus.Status)
                {
                    case ServerBusy:
                    case ServerFull:
                    case ServerUp:
                        {
                            stringBuilder.AppendLine($"{realm.Name} {currentStatus.Status}");
                            break;
                        }
                    default:
                        {
                            stringBuilder.AppendLine($"{realm.Name} {ServerDown}");
                            break;
                        }
                }
            }
            return stringBuilder.ToString();
        }

        public async Task UpdateRealmStatusAsync(Guid realmId, int currentStatus, int currentOnlinePlayers, int maxOnlinePlayers)
        {
            RealmData realm = await dbContext.Realms.FirstOrDefaultAsync(x => x.RealmID == realmId);
            if (realm == null)
            {
                return;
            }

            realm.Status = (byte)currentStatus;
            realm.LastPing = DateTime.Now;

            RealmStatus realmStatus = new()
            {
                Realm = realm,
                Status = (byte)currentStatus,
                PlayersOnline = currentOnlinePlayers,
                MaxPlayersOnline = maxOnlinePlayers,
                Time = DateTime.Now,
            };
            dbContext.RealmStatuses.Add(realmStatus);
            await dbContext.SaveChangesAsync();
        }

        public class LatestRealmStatus
        {
            public string RealmName { get; set; }
            public string Status { get; set; }
            public int StatusInt { get; set; }
            public int PlayersOnline { get; set; }
            public int MaxPlayersOnline { get; set; }
            public int OnlinePlayerLimit { get; set; }
            public bool GmServerConnected { get; set; }
        }
    }
}
