﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc.Localization;
using Canyon.GM.Panel.Services.Core;
using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Services.Logging;
using Canyon.Backend.Web.Services;
using Canyon.GM.Panel.Sockets.Piglet;
using Canyon.GM.Panel.Helpers;
using Quartz;
using Canyon.GM.Panel.Services.Timers;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Canyon.GM.Panel.Services.Payments;
using MercadoPago.Config;

namespace Canyon.GM.Panel.Services
{
    public static class Service
    {
        public static IServiceCollection Register(IServiceCollection service)
        {
            service.AddHttpClient();

            service.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            service.AddTransient(s =>
            {
                IHttpContextAccessor contextAccessor = s.GetService<IHttpContextAccessor>();
                ClaimsPrincipal user = contextAccessor?.HttpContext?.User;
                return user;
            });

            service.AddTransient<IEmailSender, EmailService>();
            service.AddTransient<IStringLocalizer, StringLocalizer>();
            service.AddTransient<IHtmlLocalizer, Core.HtmlLocalizer>();
            service.AddTransient<IPasswordHasher<ApplicationUser>, WhirlpoolPasswordService>();
            service.AddTransient<EventLogger>();

            service.AddTransient<RealmService>();
            service.AddTransient<RealmStatusService>();
            service.AddTransient<GameAccountService>();
            service.AddTransient<BanService>();
            service.AddTransient<TradeHistoryService>();
            service.AddTransient<ArticleService>();
            service.AddTransient<RankingService>();
            service.AddTransient<PurchaseService>();
            service.AddTransient<ProductService>();
            service.AddTransient<CreditCardService>();
            service.AddTransient<PaymentsService>();
            service.AddTransient<MercadoPagoService>();
            service.AddSingleton<RealmSocketService>();
            service.AddSingleton<PigletServer>();
            service.AddSingleton<MaintenanceService>();

            service.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionJobFactory();

                JobKey key = new("SocketPingTimerJob");
                q.AddJob<SocketPingTimer>(key);
                q.AddTrigger(opts => opts
                    .ForJob(key)
                    .WithIdentity("SocketPingTimerJob-Trigger")
                    .WithSchedule(SimpleScheduleBuilder.RepeatSecondlyForever())
                );

                key = new("RankingResetTimerJob");
                q.AddJob<RankingTimer>(key);
                q.AddTrigger(opts => opts.ForJob(key)
                    .WithIdentity("RankingResetTimerJob-Trigger")
                    .WithCronSchedule("0 0 */12 * * ?")
                );

                key = new("PerMinuteTimerJob");
                q.AddJob<MinuteTimer>(key);
                q.AddTrigger(opts => opts.ForJob(key)
                    .WithIdentity("PerMinuteTimerJob-Trigger")
                    .WithSchedule(SimpleScheduleBuilder.RepeatMinutelyForever())
                );
            });

            service.AddQuartzHostedService(q =>
            {
                q.WaitForJobsToComplete = true;
            });

            // warm up
            var serviceProvider = service.BuildServiceProvider();
            var providerLogger = serviceProvider.GetService<ILogger<Program>>();
            providerLogger.LogInformation("Starting service warp up");

            var configuration = serviceProvider.GetRequiredService<IConfiguration>();

            ConfigurationHelper.Configuration = configuration;
            ConfigurationHelper.ServiceProvider = serviceProvider;

            var pigletServer = serviceProvider.GetRequiredService<PigletServer>();
            if (pigletServer != null)
            {
                if (!int.TryParse(configuration["Piglet:ListenPort"]?.ToString(), out var port))
                {
#if !DEBUG
                    port = 4352;
#else
                    port = 4360;
#endif
                }
                var logger = serviceProvider.GetRequiredService<ILogger<PigletServer>>();
                _ = pigletServer.StartAsync(port).ConfigureAwait(false);
                logger.LogInformation("Piglet server listening on port {port}", port);
            }

            var maintenanceManager = serviceProvider.GetRequiredService<MaintenanceService>();
            if (maintenanceManager != null)
            {
                _ = maintenanceManager.InitializeAsync();
                service.Replace(new ServiceDescriptor(typeof(MaintenanceService), maintenanceManager));
            }

            bool isMercadoPagoEnabled = bool.Parse(configuration["Payments:MercadoPago:Enabled"]);
            if (isMercadoPagoEnabled)
            {
                MercadoPagoConfig.AccessToken = configuration["Payments:MercadoPago:PrivateKey"];
            }
            return service;
        }
    }
}
