using Canyon.GM.Panel.Database.Entities.Local;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Canyon.GM.Panel.Services.Identity;
using System.ComponentModel.DataAnnotations;
using Canyon.GM.Panel.Services.Logging;

namespace Canyon.GM.Panel.Pages
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly UserManager userManager;
        private readonly SignInManager signInManager;
        private readonly EventLogger logger;
        private readonly IStringLocalizer localizer;

        public LoginModel(SignInManager signInManager,
            EventLogger logger,
            UserManager userManager,
            IStringLocalizer localizer,
            IEmailSender emailSender)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.logger = logger;
            this.localizer = localizer;
        }

        public class InputModel
        {
            [Required]
            public string UserName { get; set; }
            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }
            public bool RememberMe { get; set; }
        }

        [BindProperty]
        public InputModel LoginInput { get; set; }
        public string ReturnUrl { get; set; }
        [TempData]
        public string ErrorMessage { get; set; }

        public async Task<IActionResult> OnGetAsync(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (string.IsNullOrEmpty(returnUrl))
                    returnUrl = "/";
                return Redirect(returnUrl);
            }

            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl ??= Url.Content("~/");

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            ReturnUrl = returnUrl;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");

            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                ApplicationUser applicationUser = await userManager.FindByNameAsync(LoginInput.UserName);
                if (applicationUser == null)
                {
                    applicationUser = await userManager.FindByEmailAsync(LoginInput.UserName);
                }

#if DEBUG
                const bool lockoutOnFailure = false;
#else
                const bool lockoutOnFailure = true;
#endif

                Microsoft.AspNetCore.Identity.SignInResult result;
                if (applicationUser != null)
                {
                    result = await signInManager.PasswordSignInAsync(applicationUser, LoginInput.Password, LoginInput.RememberMe, lockoutOnFailure);
                }
                else
                {
                    result = await signInManager.PasswordSignInAsync(LoginInput.UserName, LoginInput.Password, LoginInput.RememberMe, lockoutOnFailure);
                }

                if (result.Succeeded)
                {
                    await logger.LogAsync(EventLogger.LogType.AdminSignIn, $"UserSignedIn", applicationUser);
                    return LocalRedirect(returnUrl);
                }
                if (result.RequiresTwoFactor)
                {
                    await logger.LogAsync(EventLogger.LogType.AdminSignIn, $"UserSignedIn2fa", applicationUser);
                    return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, LoginInput.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    await logger.LogAsync(EventLogger.LogType.AdminSignIn, $"UserSignInLockedOut", false, new
                    {
                        Username = LoginInput.UserName
                    });
                    return RedirectToPage("./Lockout");
                }
                else
                {
                    if (applicationUser != null && !applicationUser.EmailConfirmed)
                    {
                        await logger.LogAsync(EventLogger.LogType.AdminSignIn, $"UserSignInNotConfirmed", false, new
                        {
                            Username = LoginInput.UserName
                        });
                        ModelState.AddModelError(string.Empty, localizer["InvalidLoginAttempt2"]);
                    }
                    else
                    {
                        await logger.LogAsync(EventLogger.LogType.AdminSignIn, $"UserSignInWrongPassword", false, new
                        {
                            Username = LoginInput.UserName
                        });
                        ModelState.AddModelError(string.Empty, localizer["InvalidLoginAttempt"]);
                    }
                    return Page();
                }
            }

            return Page();
        }
    }
}
