﻿using Canyon.Backend.Web.Services;
using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Services.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Canyon.GM.Panel.Pages
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext applicationDbContext;
        private readonly RealmStatusService realmStatusService;

        public IndexModel(
            ApplicationDbContext applicationDbContext,
            RealmStatusService realmStatusService
        )
        {
            this.applicationDbContext = applicationDbContext;
            this.realmStatusService = realmStatusService;
        }

        public IList<LogGeneral> GeneralLogs { get; private set; } = new List<LogGeneral>();
        public List<RealmStatusService.LatestRealmStatus> RealmStatus { get; private set; } = new List<RealmStatusService.LatestRealmStatus>();

        public async Task<IActionResult> OnGetAsync()
        {
            GeneralLogs = await applicationDbContext.LogGenerals
                .Include(x => x.Account)
                .Where(x => x.LogType < EventLogger.LogType.AdminEventLimit)
                .OrderByDescending(x => x.Timestamp)
                .Take(10)
                .ToListAsync();

            RealmStatus = realmStatusService.GetLatestRealmStatus();
            return Page();
        }
    }
}