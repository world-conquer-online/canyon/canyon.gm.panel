using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Services.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.Reflection;
using Canyon.GM.Panel.Localization;
using Canyon.GM.Panel.Services.Core;
using Microsoft.AspNetCore.Localization;
using Canyon.GM.Panel.Helpers;
using System.Net;

namespace Canyon.GM.Panel
{
    public class Program
    {
        public static string Version => Assembly.GetEntryAssembly()?
                                                .GetCustomAttribute<AssemblyInformationalVersionAttribute>()?
                                                .InformationalVersion ?? "0.0.0.0";

        public static readonly DateTime BuildTime = GetBuildDate(Assembly.GetExecutingAssembly());

        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Logging.AddSimpleConsole(c =>
            {
                c.TimestampFormat = "[yyyyMMdd HHmmss.fff] ";
            }); ;

            ConfigurationManager configuration = builder.Configuration;
            // Add services to the container.
            var connectionString = configuration.GetConnectionString("DefaultConnection") ?? throw new InvalidOperationException("Connection string 'DefaultConnection' not found.");
            builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)));

            builder.Services.AddDatabaseDeveloperPageExceptionFilter();

            builder.Services.AddDefaultIdentity<ApplicationUser>(opt =>
            {
                opt.Password.RequireDigit = true;
                opt.Password.RequireLowercase = true;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = true;
                opt.Password.RequiredLength = 10;
                opt.Password.RequiredUniqueChars = 5;

                // Lockout settings.
                opt.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(15);
                opt.Lockout.MaxFailedAccessAttempts = 5;
                opt.Lockout.AllowedForNewUsers = true;

                // User settings.
                opt.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                opt.User.RequireUniqueEmail = true;
                opt.SignIn.RequireConfirmedEmail = false;
                opt.SignIn.RequireConfirmedAccount = false;
                opt.SignIn.RequireConfirmedPhoneNumber = false;
            })
                .AddRoles<ApplicationRole>()
                .AddUserManager<UserManager>()
                .AddRoleManager<RoleManager>()
                .AddUserStore<ApplicationUserStore>()
                .AddRoleStore<RoleStore<ApplicationRole, ApplicationDbContext, Guid, ApplicationUserRole, ApplicationRoleClaim>>()
                .AddSignInManager<SignInManager>()
                .AddDefaultTokenProviders();

            builder.Services.ConfigureApplicationCookie(opt =>
            {
                opt.LoginPath = new PathString("/Login");
                opt.LogoutPath = new PathString("/Logout");
                opt.AccessDeniedPath = "/AccessDenied";
                opt.SlidingExpiration = true;

                opt.Cookie.IsEssential = true;
                opt.Cookie.MaxAge = TimeSpan.FromDays(30);
                opt.Cookie.HttpOnly = true;
                opt.Cookie.SameSite = SameSiteMode.Strict;

                opt.Events.OnSigningIn = ctx =>
                {
                    if (ctx.Properties.IsPersistent)
                    {
                        var issued = ctx.Properties.IssuedUtc ?? DateTimeOffset.UtcNow;
                        ctx.Properties.ExpiresUtc = issued.AddDays(30);
                    }
                    return Task.FromResult(0);
                };
            });

            builder.Services.AddAuthentication();
            builder.Services.AddAuthorization();

            builder.Services.AddControllers()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization(options =>
                {
                    options.DataAnnotationLocalizerProvider = (type, factory) =>
                                    factory.Create(typeof(Language));
                });

            Service.Register(builder.Services);
            // Add services to the container.
            builder.Services.AddControllers();
            builder.Services.AddRazorPages();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseRequestLocalization(opt =>
            {
                opt.DefaultRequestCulture = new RequestCulture(LanguageService.AvailableLanguages[0].LanguageCultureName);
                opt.SupportedCultures = LanguageService.AvailableLanguages.Select(x => new CultureInfo(x.LanguageCultureName)).ToList();
                opt.SupportedUICultures = LanguageService.AvailableLanguages.Select(x => new CultureInfo(x.LanguageCultureName)).ToList();

                opt.RequestCultureProviders.Insert(0, new CustomRequestCultureProvider(context =>
                {
                    return Task.FromResult(new ProviderCultureResult(context.User?.GetNullableCurrentLocale()));
                }));
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseStatusCodePagesWithReExecute("/Error/{0}");

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllers();
            app.MapRazorPages();

            app.Run();
        }

        private static DateTime GetBuildDate(Assembly assembly)
        {
            const string BuildVersionMetadataPrefix = "+build";

            var attribute = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();
            if (attribute?.InformationalVersion != null)
            {
                var value = attribute.InformationalVersion;
                var index = value.IndexOf(BuildVersionMetadataPrefix);
                if (index > 0)
                {
                    value = value.Substring(index + BuildVersionMetadataPrefix.Length);
                    if (DateTime.TryParseExact(value, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out var result))
                    {
                        return result;
                    }
                }
            }

            return default;
        }
    }
}