﻿namespace Canyon.GM.Panel.Database.Entities.Views
{
    public class GetTopFamily
    {
        public string FamilyName { get; set; }
        public string LeaderName { get; set; }
        public ulong Money { get; set; }
        public uint Amount { get; set; }
        public byte Level { get; set; }
        public byte StarTower { get; set; }
        public string FamilyMapName { get; set; }
    }
}
