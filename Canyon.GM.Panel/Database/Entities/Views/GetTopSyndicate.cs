﻿namespace Canyon.GM.Panel.Database.Entities.Views
{
    public class GetTopSyndicate
    {
        public string SyndicateName { get; set; }
        public string LeaderName { get; set; }
        public long Money { get; set; }
        public uint ConquerPoints { get; set; }
        public uint Amount { get; set; }
        public uint Level { get; set; }
    }
}
