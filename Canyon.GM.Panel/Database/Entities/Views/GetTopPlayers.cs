﻿namespace Canyon.GM.Panel.Database.Entities.Views
{
    public class GetTopPlayers
    {
        public virtual uint Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Mate { get; set; }
        public virtual byte Level { get; set; }
        public virtual byte Profession { get; set; }
        public virtual byte PreviousProfession { get; set; }
        public virtual byte FirstProfession { get; set; }
        public virtual string SyndicateName { get; set; }
    }
}
