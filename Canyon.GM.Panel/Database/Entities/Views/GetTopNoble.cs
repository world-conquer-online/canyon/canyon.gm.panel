﻿namespace Canyon.GM.Panel.Database.Entities.Views
{
    public class GetTopNoble
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string Mate { get; set; }
        public byte Level { get; set; }
        public ulong PeerageDonation { get; set; }
        public string SyndicateName { get; set; }
    }
}
