﻿namespace Canyon.GM.Panel.Database.Entities.Game
{
    public class RealmFamilyReport
    {
        public virtual uint Id { get; set; }
        public virtual string Name { get; set; }
        public virtual uint LeaderId { get; set; }
        public virtual uint Amount { get; set; }
        public virtual ulong Money { get; set; }
        public virtual byte Level { get; set; }
        public virtual byte StarTower { get; set; }
        public virtual uint? FamilyMap { get; set; }
        public virtual string FamilyMapName { get; set; }
        public virtual uint OccupyDate { get; set; }
    }
}
