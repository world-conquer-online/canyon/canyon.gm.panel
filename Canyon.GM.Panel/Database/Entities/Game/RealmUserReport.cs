﻿namespace Canyon.GM.Panel.Database.Entities.Game
{
    public class RealmUserReport
    {
        public virtual uint Id { get; set; }
        public virtual string Name { get; set; }
        public virtual uint Mate { get; set; }
        public virtual ulong Money { get; set; }
        public virtual uint StorageMoney { get; set; }
        public virtual uint ConquerPoints { get; set; }
        public virtual uint ConquerPointsBound { get; set; }
        public virtual byte Level { get; set; }
        public virtual ulong Experience { get; set; }
        public virtual byte Metempsychosis { get; set; }
        public virtual byte Profession { get; set; }
        public virtual byte PreviousProfession { get; set; }
        public virtual byte FirstProfession { get; set; }
        public virtual ushort Strength { get; set; }
        public virtual ushort Agility { get; set; }
        public virtual ushort Health { get; set; }
        public virtual ushort Soul { get; set; }
        public virtual ushort AttributePoints { get; set; }
        public virtual uint Gift1 { get; set; }
        public virtual uint Gift2 { get; set; }
        public virtual uint Gift3 { get; set; }
        public virtual uint Gift4 { get; set; }
        public virtual uint? SyndicateId { get; set; }
        public virtual uint? FamilyId { get; set; }
        public virtual uint SupermanCount { get; set; }
        public virtual ulong PeerageDonation { get; set; }
    }
}
