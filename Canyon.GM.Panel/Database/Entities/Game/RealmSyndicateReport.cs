﻿namespace Canyon.GM.Panel.Database.Entities.Game
{
    public class RealmSyndicateReport
    {
        public virtual uint Id { get; set; }
        public virtual string Name { get; set; }
        public virtual uint LeaderId { get; set; }
        public virtual uint Amount { get; set; }
        public virtual long Money { get; set; }
        public virtual uint ConquerPoints { get; set; }
        public virtual byte Level { get; set; }
        public virtual byte DelFlag { get; set; }
    }
}
