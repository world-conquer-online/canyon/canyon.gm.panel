﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Game.TradeHistory
{
    public class TradeHistoryDetail
    {
        [Column("id")]
        public virtual uint Id { get; set; }
        [Column("trade_id")]
        public virtual uint TradeId { get; set; }
        [Column("user_id")]
        public virtual uint UserId { get; set; }
        [Column("itemtype")]
        public virtual uint ItemType { get; set; }
        [Column("chksum")]
        public virtual uint Checksum { get; set; }
        [Column("json_data")]
        public virtual string JsonData { get; set; }
        public virtual string ItemName { get; set; }

        public TradeItemData GetItemData()
        {
            if (!string.IsNullOrEmpty(JsonData))
            {
                return JsonConvert.DeserializeObject<TradeItemData>(JsonData);
            }
            return null;
        }

        public class TradeItemData
        {
            /// <summary>
            ///     The unique identification of the item.
            /// </summary>
            public virtual uint Id { get; set; }

            /// <summary>
            ///     The itemtype of the item.
            /// </summary>
            public virtual uint Type { get; set; }

            /// <summary>
            ///     The owner (shop or player) where the player got the item from.
            /// </summary>
            public virtual uint OwnerId { get; set; }

            /// <summary>
            ///     The player who actually owns the item.
            /// </summary>
            public virtual uint PlayerId { get; set; }

            /// <summary>
            ///     The actual durability of the item.
            /// </summary>
            public virtual ushort Amount { get; set; }

            /// <summary>
            ///     The max amount of the durability of the item.
            /// </summary>
            public virtual ushort AmountLimit { get; set; }

            /// <summary>
            ///     Not sure yet.
            /// </summary>
            public virtual byte Ident { get; set; }

            /// <summary>
            ///     The actual position of the item. +200 for warehouses.
            /// </summary>
            public virtual byte Position { get; set; }

            /// <summary>
            ///     The gem on socket 1. 255 for open hole.
            /// </summary>
            public virtual byte Gem1 { get; set; }

            /// <summary>
            ///     The gem on socket 2. 255 for open hole.
            /// </summary>
            public virtual byte Gem2 { get; set; }

            /// <summary>
            ///     The item effect.
            /// </summary>
            public virtual ushort Magic1 { get; set; }

            /// <summary>
            ///     Not sure yet.
            /// </summary>
            public virtual byte Magic2 { get; set; }

            /// <summary>
            ///     The item plus.
            /// </summary>
            public virtual byte Magic3 { get; set; }

            /// <summary>
            ///     Socket progress.
            /// </summary>
            public virtual uint Data { get; set; }

            /// <summary>
            ///     The item blessing.
            /// </summary>
            public virtual byte ReduceDmg { get; set; }

            /// <summary>
            ///     Item enchantment.
            /// </summary>
            public virtual byte AddLife { get; set; }

            /// <summary>
            ///     The green attribute. Not used tho.
            /// </summary>
            public virtual byte AntiMonster { get; set; }

            /// <summary>
            ///     Not sure yet.
            /// </summary>
            public virtual uint ChkSum { get; set; }

            /// <summary>
            ///     Item locking timestamp. If 0, item is not locked, if timestamp is set 1 item is locked. If it has a timestamp, it's
            ///     the unlock time.
            /// </summary>
            [Column("plunder")]
            public virtual uint Plunder { get; set; }


            /// <summary>
            ///     Forbbiden or not?
            /// </summary>
            public virtual uint Specialflag { get; set; }

            /// <summary>
            ///     The color of the item.
            /// </summary>
            public virtual uint Color { get; set; }

            /// <summary>
            ///     The progress of the plus.
            /// </summary>
            public virtual uint AddlevelExp { get; set; }
            /// <summary>
            ///     The kind of item. (Bound, Quest, etc)
            /// </summary>
            public virtual byte Monopoly { get; set; }
            public virtual uint Syndicate { get; set; }
            public virtual int DeleteTime { get; set; }
            public virtual uint SaveTime { get; set; }
            public virtual uint AccumulateNum { get; set; }
        }
    }
}
