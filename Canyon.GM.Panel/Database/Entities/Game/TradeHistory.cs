﻿namespace Canyon.GM.Panel.Database.Entities.Game.TradeHistory
{
    public class TradeHistory
    {
        public virtual uint Id { get; set; }
        public virtual uint CallerId { get; set; }
        public virtual string CallerName { get; set; }
        public virtual uint CallerMoney { get; set; }
        public virtual uint CallerConquerPoints { get; set; }
        public virtual string CallerIpAddress { get; set; }
        public virtual string CallerMacAddress { get; set; }
        public virtual uint TargetId { get; set; }
        public virtual string TargetName { get; set; }
        public virtual uint TargetMoney { get; set; }
        public virtual uint TargetConquerPoints { get; set; }
        public virtual string TargetIpAddress { get; set; }
        public virtual string TargetMacAddress { get; set; }
        public virtual DateTime Timestamp { get; set; }
    }
}
