﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("shop_product_price")]
    public class ShopProductPrice
    {
        [Key]
        public virtual Guid Id { get; set; }
        public virtual Guid ProductId { get; set; }
        public virtual string Currency { get; set; }
        public virtual decimal Price { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
    }
}
