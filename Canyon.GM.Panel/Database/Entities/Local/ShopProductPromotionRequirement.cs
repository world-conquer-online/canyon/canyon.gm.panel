﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("shop_product_promotion_requirement")]
    public class ShopProductPromotionRequirement
    {
        [Key]
        public virtual Guid Id { get; set; }
        public virtual Guid PromotionId { get; set; }
        public virtual int RequirementType { get; set; }
        public virtual long MinValue { get; set; }
        public virtual long MaxValue { get; set; }

        public virtual ShopProductPromotion Promotion { get; set; }
    }
}
