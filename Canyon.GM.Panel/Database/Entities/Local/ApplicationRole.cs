﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("account_roles")]
    public class ApplicationRole : IdentityRole<Guid>
    {
        [Key]
        public override Guid Id
        {
            get => base.Id;
            set => base.Id = value;
        }

        public DateTime CreationDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}