﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("shop_product_reward")]
    public class ShopProductReward
    {
        [Key]
        public virtual Guid Id { get; set; }
        public virtual Guid ProductId { get; set; }
        public virtual uint Money { get; set; }
        public virtual uint ConquerPoints { get; set; }
        public virtual uint ConquerPointsMono { get; set; }
        public virtual uint ItemType { get; set; }
        public virtual uint ItemTypeCount { get; set; }
        public virtual uint ActionId { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
    }
}
