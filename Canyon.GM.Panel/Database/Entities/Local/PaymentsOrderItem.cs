﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("payments_order_item")]
    public class PaymentsOrderItem
    {
        [Key]
        public virtual int Id { get; protected set; }
        public virtual Guid PaymentOrderId { get; set; }
        public virtual Guid ProductId { get; set; }
        public virtual int Amount { get; set; }
        public virtual decimal Price { get; set; }
        public virtual DateTime CreatedAt { get; set; }

        public virtual PaymentsOrder PaymentOrder { get; set; }
        public virtual Product Product { get; set; }
    }
}
