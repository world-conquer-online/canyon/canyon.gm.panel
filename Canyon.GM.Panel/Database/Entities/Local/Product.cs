﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("product")]
    public class Product
    {
        [Key]
        public virtual Guid Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual int? ImageId { get; set; }
        public virtual decimal Price { get; set; }
        public virtual int CardTypeId { get; set; }

        public virtual Image Image { get; set; }
    }
}
