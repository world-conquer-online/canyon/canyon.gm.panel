﻿using Canyon.GM.Panel.Services.Logging;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("log_general")]
    public class LogGeneral
    {
        [Key]
        public virtual ulong Id { get; protected set; }
        public virtual EventLogger.LogType LogType { get; set; }
        public virtual Guid? AccountId { get; set; }
        public virtual string IpAddress { get; set; }
        public virtual string Message { get; set; }
        public virtual string Data { get; protected set; }
        public virtual bool Success { get; set; } = true;
        public virtual DateTime Timestamp { get; set; }

        public virtual ApplicationUser Account { get; set; }

        public void SetData(object data)
        {
            if (data == null)
            {
                Data = "";
                return;
            }
            Data = JsonConvert.SerializeObject(data);
        }

        public T GetData<T>() where T : class
        {
            if (string.IsNullOrEmpty(Data))
            {
                return default;
            }
            return JsonConvert.DeserializeObject<T>(Data);
        }
    }
}
