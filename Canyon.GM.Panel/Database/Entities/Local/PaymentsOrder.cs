﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("payments_order")]
    public class PaymentsOrder
    {
        [Key]
        public virtual Guid Id { get; protected set; }
        public virtual int MethodId { get; set; }
        public virtual string MethodData { get; set; }
        public virtual Guid UserId { get; set; }
        public virtual decimal Price { get; set; }
        public virtual string Comment { get; set; }
        public virtual PaymentOrderStatus Status { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime? UpdatedAt { get; set; }

        public virtual PaymentsMethod Method { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual IList<PaymentsOrderItem> Items { get; set; }
        public virtual IList<CreditCard> CreditCards { get; set; }

        public enum PaymentOrderStatus
        {
            Pending,
            Paid,
            Denied,
            Cancelled,
            Delivered,
            Refunded
        }
    }
}
