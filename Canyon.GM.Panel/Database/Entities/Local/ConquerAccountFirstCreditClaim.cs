﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("conquer_account_first_credit_claim")]
    public class ConquerAccountFirstCreditClaim
    {
        [Key]
        public virtual int Id { get; protected set; }
        public Guid RealmID { get; set; }
        public int AccountID { get; set; }
        public DateTime ClaimDate { get; set; }

        public virtual RealmData Realm { get; set; }
        public virtual ConquerAccount Account { get; set; }
    }
}
