﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("shop_product_promotion")]
    public class ShopProductPromotion
    {
        [Key]
        public virtual Guid Id { get; set; }
        public virtual Guid ProductId { get; set; }
        public virtual double Discount { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }

        public virtual IList<ShopProductPromotionRequirement> Requirements { get; set; }
    }
}
