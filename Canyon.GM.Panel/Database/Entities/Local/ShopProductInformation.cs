﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("shop_product_information")]
    public class ShopProductInformation
    {
        [Key]
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string ImagePath { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }

        public virtual IList<ShopProductPrice> Prices { get; set; }
        public virtual IList<ShopProductPromotion> Promotions { get; set; }
    }
}
