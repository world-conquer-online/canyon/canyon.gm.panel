﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("realm_status")]
    public class RealmStatus
    {
        [Key]
        public virtual ulong Id { get; set; }
        public virtual Guid RealmID { get; set; }
        public virtual byte Status { get; set; }
        public virtual int PlayersOnline { get; set; }
        public virtual int MaxPlayersOnline { get; set; }
        public virtual DateTime Time { get; set; }

        public virtual RealmData Realm { get; set; }
    }
}
