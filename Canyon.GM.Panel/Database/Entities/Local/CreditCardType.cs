﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("credit_card_type")]
    public class CreditCardType
    {
        [Key]
        public virtual uint Id { get; protected set; }
        public virtual byte VipLevel { get; set; }
        public virtual uint VipLevelDuration { get; set; }
        public virtual ulong Silvers { get; set; }
        public virtual uint ConquerPoints { get; set; }
        public virtual bool ConquerPointsMono { get; set; }
        public virtual uint ItemType { get; set; }
        public virtual uint ActionId { get; set; }
    }
}
