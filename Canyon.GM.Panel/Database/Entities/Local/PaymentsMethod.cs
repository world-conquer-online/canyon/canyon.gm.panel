﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("payments_method")]
    public class PaymentsMethod
    {
        [Key]
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual bool Enabled { get; set; }
        public virtual DateTime CreationDate { get; set; }
    }
}
