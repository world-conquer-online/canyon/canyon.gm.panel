﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("conquer_maintenance_schedule")]
    public class ConquerMaintenanceSchedule
    {
        [Key] public virtual int Id { get; protected set; }
        public virtual DayOfWeek Weekday { get; set; }
        public virtual TimeSpan StartTime { get; set; }
        public virtual int WarningMinutes { get; set; }
    }
}
