﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Models.Ranking
{
    [Table("conquer_rank_brush")]
    public class ConquerRankBrush
    {
        [Key]
        public virtual uint Id { get; set; }
        public virtual DateTime Start { get; set; }
        public virtual DateTime End { get; set; }
        public virtual long Ticks { get; set; }
        public virtual Guid ServerId { get; set; }
    }
}
