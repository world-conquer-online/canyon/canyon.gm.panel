﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static Canyon.GM.Panel.Database.Entities.Local.PaymentsOrder;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("payments_mercadopago")]
    public class PaymentsMercadoPago
    {
        [Key]
        public virtual int Id { get; set; }
        public virtual Guid OrderId { get; set; }
        public virtual long PaymentId { get; set; }
        public virtual PaymentOrderStatus LastStatus { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime? UpdatedAt { get; set; }

        public virtual PaymentsOrder Order { get; set; }
    }
}
