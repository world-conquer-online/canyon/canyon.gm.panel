﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Models.Ranking
{
    [Table("conquer_rank_syndicate")]
    public class ConquerRankSyndicate
    {
        [Key]
        public virtual uint Id { get; set; }
        public virtual uint SyndicateId { get; set; }
        public virtual string Name { get; set; }
        public virtual uint LeaderId { get; set; }
        public virtual uint Amount { get; set; }
        public virtual long Money { get; set; }
        public virtual uint ConquerPoints { get; set; }
        public virtual uint Level { get; set; }
        public virtual byte DelFlag { get; set; }
        public virtual Guid RealmId { get; set; }
    }
}
