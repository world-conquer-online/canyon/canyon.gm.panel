﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("credit_card")]
    public class CreditCard
    {
        [Key]
        public virtual int Id { get; set; }
        public virtual uint CardTypeId { get; set; }
        public virtual Guid? TransactionId { get; set; }
        public virtual Guid CardUUID { get; set; }
        public virtual Guid CreatedBy { get; set; }
        public virtual Guid? UsedBy { get; set; }
        public virtual int? UsedByGameAccount { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime? UpdatedAt { get; set; }
        public virtual DateTime? CancelledAt { get; set; }

        public virtual PaymentsOrder Transaction { get; set; }
        public virtual CreditCardType CardType { get; set; }
    }
}
