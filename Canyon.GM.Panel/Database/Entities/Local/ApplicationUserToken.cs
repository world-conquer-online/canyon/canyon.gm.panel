﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("account_tokens")]
    public class ApplicationUserToken : IdentityUserToken<Guid>
    {
        [Key]
        [ForeignKey("FK_AspNetUserTokens_AspNetUsers_UserId")]
        public override Guid UserId
        {
            get => base.UserId;
            set => base.UserId = value;
        }

        [Key]
        public override string LoginProvider
        {
            get => base.LoginProvider;
            set => base.LoginProvider = value;
        }

        [Key]
        public override string Name
        {
            get => base.Name;
            set => base.Name = value;
        }
    }
}