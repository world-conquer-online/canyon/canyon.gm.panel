﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("account_user")]
    public class ApplicationUser : IdentityUser<Guid>
    {
        [Key]
        public override Guid Id
        {
            get => base.Id;
            set => base.Id = value;
        }

        public override string NormalizedEmail
        {
            get => base.NormalizedEmail;
            set => base.NormalizedEmail = value;
        }

        public override string NormalizedUserName
        {
            get => base.NormalizedUserName;
            set => base.NormalizedUserName = value;
        }

        public string Salt { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual IList<ConquerAccount> Accounts { get; set; }
        [ForeignKey("UserId")]
        public virtual IList<ApplicationUserClaim> Claims { get; set; }
    }
}
