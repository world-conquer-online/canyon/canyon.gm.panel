﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Canyon.GM.Panel.Database.Entities.Local
{
    [Table("account_user_claims")]
    public class ApplicationUserClaim : IdentityUserClaim<Guid>
    {
        [Key]
        public new Guid Id { get; set; }

        [ForeignKey("FK_AspNetUserClaims_AspNetUsers_UserId")]
        public override Guid UserId { get; set; }
    }
}