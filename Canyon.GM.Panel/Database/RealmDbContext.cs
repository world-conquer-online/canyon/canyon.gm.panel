﻿using Canyon.GM.Panel.Database.Entities.Game.TradeHistory;
using Canyon.GM.Panel.Database.Entities.Game;
using Microsoft.EntityFrameworkCore;

namespace Canyon.GM.Panel.Database
{
    public class RealmDbContext : DbContext
    {
        private readonly string connectionString;

        public RealmDbContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public virtual DbSet<RealmUserReport> UserReports { get; set; }
        public virtual DbSet<RealmSyndicateReport> SyndicateReports { get; set; }
        public virtual DbSet<RealmFamilyReport> FamilyReports { get; set; }
        public virtual DbSet<ConquerGameCharacter> ConquerGameCharacters { get; set; }
        public virtual DbSet<TradeHistory> TradeHistories { get; set; }
        public virtual DbSet<TradeHistoryDetail> TradeHistoryDetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            if (!string.IsNullOrEmpty(connectionString))
            {
                optionsBuilder.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ConquerGameCharacter>().HasNoKey();
        }
    }
}
