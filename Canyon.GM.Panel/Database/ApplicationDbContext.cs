﻿using Duende.IdentityServer.EntityFramework.Entities;
using Duende.IdentityServer.EntityFramework.Extensions;
using Duende.IdentityServer.EntityFramework.Interfaces;
using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Data.Common;
using System.Data;
using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Models.Ranking;
using Canyon.GM.Panel.Database.Entities.Views;
using Canyon.GM.Panel.Helpers.Converters;

namespace Canyon.GM.Panel.Database
{
    public class ApplicationDbContext : IdentityDbContext<
        ApplicationUser,
        ApplicationRole,
        Guid,
        ApplicationUserClaim,
        ApplicationUserRole,
        ApplicationUserLogin,
        ApplicationRoleClaim,
        ApplicationUserToken
        >, IPersistedGrantDbContext
    {
        private readonly ILogger<ApplicationDbContext> logger;
        private readonly IOptions<OperationalStoreOptions> operationalStoreOptions;

        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options,
            IOptions<OperationalStoreOptions> operationalStoreOptions,
            ILogger<ApplicationDbContext> logger)
            : base(options)
        {
            this.logger = logger;
            this.operationalStoreOptions = operationalStoreOptions;
        }

        /// <summary>
        ///     Gets or sets the <see cref="DbSet{PersistedGrant}" />.
        /// </summary>
        public virtual DbSet<PersistedGrant> PersistedGrants { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="DbSet{DeviceFlowCodes}" />.
        /// </summary>
        public virtual DbSet<DeviceFlowCodes> DeviceFlowCodes { get; set; }
        public virtual DbSet<Key> Keys { get; set; }
        public virtual DbSet<ConquerAccount> GameAccounts { get; set; }
        public virtual DbSet<ConquerMaintenanceSchedule> ConquerMaintenanceSchedules { get; set; }
        public virtual DbSet<GameAccountAuthority> GameAccountAuthorities { get; set; }

        public virtual DbSet<LogGeneral> LogGenerals { get; set; }

        public virtual DbSet<RealmData> Realms { get; set; }
        public virtual DbSet<RealmStatus> RealmStatuses { get; set; }

        public virtual DbSet<ApplicationUserBan> ApplicationUserBans { get; set; }
        public virtual DbSet<ApplicationUserBanReason> ApplicationUserBanReasons { get; set; }
        public virtual DbSet<ApplicationUserBanReasonFactor> ApplicationUserBanReasonsFactors { get; set; }

        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<ArticleContent> ArticleContents { get; set; }
        public virtual DbSet<ArticleCategory> ArticleCategories { get; set; }
        public virtual DbSet<ArticleCategoryType> ArticleCategoryTypes { get; set; }
        public virtual DbSet<ArticleReadToken> ArticleReadTokens { get; set; }
        public virtual DbSet<ArticleComment> ArticleComments { get; set; }

        public virtual DbSet<Image> Images { get; set; }

        public virtual DbSet<ConquerRankBrush> RankBrushes { get; set; }
        public virtual DbSet<ConquerRankUser> RankUsers { get; set; }
        public virtual DbSet<ConquerRankSyndicate> RankSyndicates { get; set; }
        public virtual DbSet<ConquerRankFamily> RankFamilies { get; set; }

        public virtual DbSet<GetTopPlayers> TopPlayers { get; set; }
        public virtual DbSet<GetTopNoble> TopNobles { get; set; }
        public virtual DbSet<GetTopSyndicate> TopSyndicates { get; set; }
        public virtual DbSet<GetTopFamily> TopFamilies { get; set; }
        public virtual DbSet<GetTopSuperman> TopSupermen { get; set; }
        public virtual DbSet<GetTopMoneybag> TopMoneybag { get; set; }

        public virtual DbSet<CreditCard> CreditCards { get; set; }
        public virtual DbSet<CreditCardType> CreditCardTypes { get; set; }
        public virtual DbSet<PaymentsOrder> PaymentsOrders { get; set; }
        public virtual DbSet<PaymentsMercadoPago> PaymentsMercadoPago { get; set; }
        public virtual DbSet<ConquerAccountFirstCreditClaim> ConquerAccountFirstCreditClaims { get; set; }
        public virtual DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ApplicationUserToken>().HasKey(x => new { x.UserId, x.LoginProvider, x.Name });
            builder.Entity<ApplicationUserRole>().HasKey(x => new { x.UserId, x.RoleId });
            builder.Entity<ApplicationUserLogin>().HasKey(x => new { x.LoginProvider, x.ProviderKey });

            builder.ConfigurePersistedGrantContext(operationalStoreOptions.Value);

            builder.Entity<DeviceFlowCodes>().ToTable("account_device_codes").HasKey(x => x.UserCode);
            builder.Entity<PersistedGrant>().ToTable("account_persisted_grants").HasKey(x => x.Key);
            builder.Entity<PersistedGrant>().Property(x => x.Key).ValueGeneratedNever();

            builder.Entity<LogGeneral>().Property(x => x.LogType).HasConversion<uint>();
            builder.Entity<RealmData>().Property(x => x.RealmID).ValueGeneratedNever();
            builder.Entity<ApplicationUserBanReasonFactor>().HasKey(x => new { x.ReasonId, x.Ocurrences });

            builder.Entity<Article>().Property(x => x.Flags).HasConversion<ulong>();

            builder.Entity<GetTopFamily>().HasNoKey();
            builder.Entity<GetTopSyndicate>().HasNoKey();
            builder.Entity<GetTopSuperman>().HasNoKey();
            builder.Entity<GetTopMoneybag>().HasNoKey();

            builder.Entity<ConquerMaintenanceSchedule>().Property(x => x.Weekday).HasConversion<uint>();

            builder.Entity<PaymentsMercadoPago>().Property(x => x.LastStatus).HasConversion<int>();
            builder.Entity<PaymentsOrder>().Property(x => x.Status).HasConversion<int>();
        }

        Task<int> IPersistedGrantDbContext.SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }

        #region CRUDOP

        public async Task<bool> CreateAsync<T>(T entity, CancellationToken cancellationToken = default)
        {
            try
            {
                Add(entity);
                await SaveChangesAsync(cancellationToken);
                return true;
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "CreateAsync has throw: {ExceptionMessage}", ex.Message);
                return false;
            }
        }

        public async Task<bool> CreateAsync<T>(IEnumerable<T> entity, CancellationToken cancellationToken = default)
        {
            try
            {
                AddRange(entity);
                await SaveChangesAsync(cancellationToken);
                return true;
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "CreateAsync has throw: {ExceptionMessage}", ex.Message);
                return false;
            }
        }

        public async Task<bool> SaveAsync<T>(T entity, CancellationToken cancellationToken = default) where T : class
        {
            try
            {
                Update(entity);
                await SaveChangesAsync(cancellationToken);
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                return false;
            }
        }

        public async Task<bool> SaveAsync<T>(List<T> entity, CancellationToken cancellationToken = default)
            where T : class
        {
            try
            {
                UpdateRange(entity);
                await SaveChangesAsync(cancellationToken);
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                return false;
            }
        }

        public async Task<bool> DeleteAsync<T>(T entity, CancellationToken cancellationToken = default) where T : class
        {
            try
            {
                Remove(entity);
                await SaveChangesAsync(cancellationToken);
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                return false;
            }
        }

        public async Task<bool> DeleteAsync<T>(List<T> entity, CancellationToken cancellationToken = default)
            where T : class
        {
            try
            {
                RemoveRange(entity);
                await SaveChangesAsync(cancellationToken);
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                return false;
            }
        }

        public async Task<DataTable> SelectAsync(string query, CancellationToken cancellationToken = default)
        {
            var result = new DataTable();
            DbConnection connection = Database.GetDbConnection();
            ConnectionState state = connection.State;

            try
            {
                if (state != ConnectionState.Open)
                    await connection.OpenAsync(cancellationToken);

                DbCommand command = connection.CreateCommand();
                command.CommandText = query;
                command.CommandType = CommandType.Text;

                await using DbDataReader reader = await command.ExecuteReaderAsync(cancellationToken);
                result.Load(reader);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            finally
            {
                if (state != ConnectionState.Closed)
                    await connection.CloseAsync();
            }

            return result;
        }

        #endregion
    }
}