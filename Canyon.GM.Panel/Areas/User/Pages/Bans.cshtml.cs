using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Models.Bans;
using Canyon.GM.Panel.Models.Rest.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Claims;
using Canyon.GM.Panel.Services.Identity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Canyon.GM.Panel.Services.Logging;
using Canyon.GM.Panel.Services;

namespace Canyon.GM.Panel.Areas.User.Pages
{
    [Authorize(Roles = ROLE_MANAGE_USERS)]
    public class BansModel : PageModel
    {
        private readonly ApplicationDbContext dbContext;
        private readonly SignInManager signInManager;
        private readonly BanService banService;
        private readonly UserManager userManager;
        private readonly GameAccountService gameAccountService;
        private readonly EventLogger eventLogger;

        public BansModel(
            ApplicationDbContext dbContext,
            SignInManager signInManager,
            BanService banService,
            UserManager userManager,
            GameAccountService gameAccountService,
            EventLogger eventLogger
            )
        {
            this.dbContext = dbContext;
            this.signInManager = signInManager;
            this.banService = banService;
            this.userManager = userManager;
            this.gameAccountService = gameAccountService;
            this.eventLogger = eventLogger;
        }

        [BindProperty(SupportsGet = true)]
        public Guid AccountID { get; set; }

        public List<ApplicationUserBanReason> BanReasons { get; private set; } = new();
        public BanReport BanReport { get; set; }

        public ApplicationUser Account { get; set; }
        public ClaimsPrincipal AccountPrincipal { get; set; }
        public List<GameAccountResponse> ConquerAccounts { get; private set; } = new();


        [BindProperty] public UserBanModel InputBan { get; set; }
        [BindProperty] public UserBanModel InputBanGameAccount { get; set; }

        private async Task<IActionResult> OnLoadAsync()
        {
            await eventLogger.LogAsync(EventLogger.LogType.UserBansView, true, new
            {
                AccountID
            });

            Account = await userManager.FindByIdAsync(AccountID.ToString());
            if (Account == null)
            {
                return NotFound();
            }

            BanReasons = await dbContext.ApplicationUserBanReasons.AsQueryable().ToListAsync();
            AccountPrincipal = await signInManager.CreateUserPrincipalAsync(Account);
            BanReport = await banService.GetBanInformationAsync(Account);
            ConquerAccounts = await gameAccountService.GetGameAccountsByParentAsync(AccountID);
            ConquerAccounts = await banService.FillBanInformationAsync(ConquerAccounts, AccountID);
            return Page();
        }

        public Task<IActionResult> OnGetAsync()
        {
            return OnLoadAsync();
        }

        public async Task<IActionResult> OnPostDoBanAsync()
        {
            ModelState.Clear();
            await TryUpdateModelAsync(InputBan);

            if (!ModelState.IsValid)
            {
                return await OnLoadAsync();
            }

            if (await banService.DoBanAsync(User, AccountID, 0, InputBan.ReasonId, InputBan.Description))
            {
                ModelState.AddModelError("success", "UserBanSuccess");
                return RedirectToPage();
            }

            ModelState.AddModelError("error", "UserBanError");
            return await OnLoadAsync();
        }

        public async Task<IActionResult> OnPostDoBanGameAccountAsync()
        {
            ModelState.Clear();
            await TryUpdateModelAsync(InputBanGameAccount);

            if (!ModelState.IsValid)
            {
                return await OnLoadAsync();
            }

            if (await banService.DoBanAsync(User, AccountID, InputBanGameAccount.UserId, InputBanGameAccount.ReasonId, InputBanGameAccount.Description))
            {
                ModelState.AddModelError("success", "UserBanSuccess");
                return RedirectToPage();
            }
            ModelState.AddModelError("error", "UserBanError");
            return await OnLoadAsync();
        }

        public class UserBanModel
        {
            public int UserId { get; set; }
            [Required]
            public int ReasonId { get; set; }
            [Required]
            public string Description { get; set; }
        }
    }
}
