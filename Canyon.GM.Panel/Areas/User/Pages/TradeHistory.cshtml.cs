using Canyon.GM.Panel.Database.Entities.Game.TradeHistory;
using Canyon.GM.Panel.Database.Entities.Local;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using static Canyon.GM.Panel.Models.Rest.Account.GameAccountResponse;
using static Canyon.GM.Panel.Services.RealmService;
using System.Security.Claims;
using Canyon.GM.Panel.Services.Identity;
using Canyon.GM.Panel.Services.Logging;
using Microsoft.AspNetCore.Authorization;

namespace Canyon.GM.Panel.Areas.User.Pages
{
    [Authorize(Roles = ROLE_MANAGE_USERS)]
    public class TradeHistoryModel : PageModel
    {
        private readonly UserManager userManager;
        private readonly SignInManager signInManager;
        private readonly GameAccountService gameAccountService;
        private readonly RealmService realmService;
        private readonly TradeHistoryService tradeHistoryService;
        private readonly IStringLocalizer stringLocalizer;
        private readonly EventLogger eventLogger;

        public TradeHistoryModel(
            UserManager userManager,
            SignInManager signInManager,
            GameAccountService gameAccountService,
            RealmService realmService,
            TradeHistoryService tradeHistoryService,
            IStringLocalizer stringLocalizer,
            EventLogger eventLogger
            )
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.gameAccountService = gameAccountService;
            this.realmService = realmService;
            this.tradeHistoryService = tradeHistoryService;
            this.stringLocalizer = stringLocalizer;
            this.eventLogger = eventLogger;
        }

        [BindProperty(SupportsGet = true)]
        public Guid AccountID { get; set; }

        [BindProperty(SupportsGet = true)]
        public InputModel Input { get; set; }

        public ApplicationUser Account { get; set; }
        public ClaimsPrincipal AccountPrincipal { get; set; }

        public List<RealmPublicListData> Realms { get; set; } = new();
        public List<ConquerGameCharacterResponse> ConquerAccounts { get; private set; } = new();
        public List<TradeHistory> Trades { get; private set; } = new();

        private async Task<IActionResult> OnLoadAsync()
        {
            await eventLogger.LogAsync(EventLogger.LogType.UserTradeHistoryView, true, new
            {
                AccountID
            });

            Account = await userManager.FindByIdAsync(AccountID.ToString());
            if (Account == null)
            {
                return NotFound();
            }

            AccountPrincipal = await signInManager.CreateUserPrincipalAsync(Account);

            Realms = (await realmService.QueryRealmsAsync(0, int.MaxValue)).Where(x => x.Production).ToList();
            if (Input.RealmID.HasValue)
            {
                var accounts = await gameAccountService.GetGameAccountsByParentAsync(AccountID);
                accounts = await gameAccountService.FetchConquerAccountsAsync(accounts);
                accounts = accounts.Where(x => x.Characters.Any(y => y.RealmID == Input.RealmID.Value)).ToList();
                ConquerAccounts = accounts.SelectMany(x => x.Characters.Where(x => x.RealmID == Input.RealmID.Value)).ToList();

                if (Input.CharacterID.HasValue)
                {
                    Trades = await tradeHistoryService.GetTradeHistoryAsync(Input.RealmID.Value, Input.CharacterID.Value, Input.Page ?? 0);
                }
            }
            return Page();
        }

        public Task<IActionResult> OnGetAsync()
        {
            return OnLoadAsync();
        }

        public string GetPlayerName(uint id, string name)
        {
            if (id == 0)
            {
                return stringLocalizer.GetString("DeletedUser", id);
            }
            return name;
        }

        public class InputModel
        {
            public Guid? RealmID { get; set; }
            public uint? CharacterID { get; set; }
            public int? Page { get; set; }
        }
    }
}
