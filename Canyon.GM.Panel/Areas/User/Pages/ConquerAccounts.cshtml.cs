using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Models.Bans;
using Canyon.GM.Panel.Models.Rest.Account;
using Canyon.GM.Panel.Services.Identity;
using Canyon.GM.Panel.Services.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Claims;

namespace Canyon.GM.Panel.Areas.User.Pages
{
    [Authorize(Roles = ROLE_MANAGE_USERS)]
    public class ConquerAccountsModel : PageModel
    {
        private readonly SignInManager signInManager;
        private readonly BanService banService;
        private readonly UserManager userManager;
        private readonly GameAccountService gameAccountService;
        private readonly EventLogger eventLogger;

        public ConquerAccountsModel(
            SignInManager signInManager,
            BanService banService,
            UserManager userManager,
            GameAccountService gameAccountService,
            EventLogger eventLogger
            )
        {
            this.signInManager = signInManager;
            this.banService = banService;
            this.userManager = userManager;
            this.gameAccountService = gameAccountService;
            this.eventLogger = eventLogger;
        }

        [BindProperty(SupportsGet = true)]
        public Guid AccountID { get; set; }

        public BanReport BanReport { get; set; }

        public ApplicationUser Account { get; set; }
        public ClaimsPrincipal AccountPrincipal { get; set; }

        public List<GameAccountResponse> ConquerAccounts { get; private set; } = new();

        private async Task<IActionResult> OnLoadAsync()
        {
            await eventLogger.LogAsync(EventLogger.LogType.UserConquerAccountsView, true, new
            {
                AccountID
            });

            Account = await userManager.FindByIdAsync(AccountID.ToString());
            if (Account == null)
            {
                return NotFound();
            }

            AccountPrincipal = await signInManager.CreateUserPrincipalAsync(Account);
            BanReport = await banService.GetBanInformationAsync(Account);
            ConquerAccounts = await gameAccountService.GetGameAccountsByParentAsync(AccountID);
            await banService.FillBanInformationAsync(ConquerAccounts, AccountID);
            await gameAccountService.FetchConquerAccountsAsync(ConquerAccounts);
            return Page();
        }

        public Task<IActionResult> OnGetAsync()
        {
            return OnLoadAsync();
        }
    }
}
