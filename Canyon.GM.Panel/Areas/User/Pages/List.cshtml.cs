using Canyon.GM.Panel.Database;
using Canyon.GM.Panel.Services.Identity;
using Canyon.GM.Panel.Services.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MySqlConnector;

namespace Canyon.GM.Panel.Areas.User.Pages
{
    [Authorize(Roles = ROLE_MANAGE_USERS)]
    public class ListModel : PageModel
    {
        private const int ipp = 25;

        private readonly UserManager userManager;
        private readonly RealmService realmService;
        private readonly GameAccountService gameAccountService;
        private readonly EventLogger eventLogger;

        public ListModel(
            UserManager userManager,
            RealmService realmService,
            GameAccountService gameAccountService,
            EventLogger eventLogger
            )
        {
            this.userManager = userManager;
            this.realmService = realmService;
            this.gameAccountService = gameAccountService;
            this.eventLogger = eventLogger;
        }

        [BindProperty(SupportsGet = true)]
        public int PageIndex { get; set; }

        [BindProperty(SupportsGet = true)]
        public UserSearchFilter Filter { get; set; }

        public List<UserSearchResponse> Users { get; set; } = new();

        public List<RealmService.RealmPublicListData> Realms { get; set; } = new();

        public async Task<IActionResult> OnGetAsync()
        {
            var realms = await realmService.QueryRealmsAsync(0, int.MaxValue);
            Realms = realms.Where(x => x.Production).ToList();

            Dictionary<int, string> charactersName = new();
            var usersQuery = userManager.Users;
            if (!string.IsNullOrEmpty(Filter.UserName))
            {
                await eventLogger.LogAsync(EventLogger.LogType.UserListFilter, true, new
                {
                    Filter
                });

                usersQuery = usersQuery
                    .Where(x => x.NormalizedUserName.Contains(Filter.UserName.ToUpper()) || x.NormalizedEmail.Contains(Filter.UserName.ToUpper()));
            }
            else if (!string.IsNullOrEmpty(Filter.AccountName))
            {
                await eventLogger.LogAsync(EventLogger.LogType.UserListFilter, true, new
                {
                    Filter
                });

                usersQuery = usersQuery
                    .Where(x => x.Accounts.Any(x => x.UserName.Contains(Filter.AccountName)));
            }
            else if (!string.IsNullOrEmpty(Filter.CharacterName))
            {
                await eventLogger.LogAsync(EventLogger.LogType.UserListFilter, true, new
                {
                    Filter
                });

                var connectionString = realmService.GetRealmConnectionString(Filter.RealmID);
                if (!string.IsNullOrEmpty(connectionString))
                {
                    var nameLike = new MySqlParameter("@nameLike", Filter.CharacterName);
                    using var serverDbContext = new RealmDbContext(connectionString);
                    var characters = serverDbContext.ConquerGameCharacters.FromSqlRaw("call QueryUsersByNameLike(@nameLike)", nameLike).ToList();
                    var accounts = await gameAccountService.FindByMultipleIdAsync(characters.Select(x => (int)x.AccountIdentity).ToArray());
                    usersQuery = usersQuery.Where(u => accounts.Select(x => x.ParentId).Distinct().ToArray().Any(id => id == u.Id));

                    foreach (var character in characters)
                    {
                        charactersName.Add((int)character.AccountIdentity, character.Name);
                    }
                }
            }

            usersQuery = usersQuery
                .Include(x => x.Accounts)
                .Skip(ipp * PageIndex)
                .Take(ipp);

            Users = usersQuery.ToList().Select(x =>
            {
                var result = new UserSearchResponse
                {
                    Id = x.Id,
                    UserName = x.UserName,
                    Email = x.Email,
                    CreationDate = x.CreationDate,
                    Characters = charactersName.Where(c => x.Accounts.Any(z => z.Id == c.Key)).Select(x => x.Value).ToList(),
                };

                if (x.Accounts != null)
                {
                    result.Accounts.AddRange(x.Accounts.Select(x => x.UserName).ToList());
                }
                return result;
            }).ToList();
            return Page();
        }

        public class UserSearchFilter
        {
            public string UserName { get; set; }
            public string AccountName { get; set; }
            public string CharacterName { get; set; }
            public Guid RealmID { get; set; }
        }

        public class UserSearchResponse
        {
            public Guid Id { get; set; }
            public string UserName { get; set; }
            public string Email { get; set; }
            public DateTime CreationDate { get; set; }
            public List<string> Accounts { get; set; } = new();
            public List<string> Characters { get; set; } = new();
        }
    }
}
