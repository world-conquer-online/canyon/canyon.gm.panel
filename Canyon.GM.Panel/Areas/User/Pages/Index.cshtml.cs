using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Models.Rest.Account;
using Canyon.GM.Panel.Services.Identity;
using Canyon.GM.Panel.Services.Logging;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;

namespace Canyon.GM.Panel.Areas.User.Pages
{
    [Authorize(Roles = ROLE_MANAGE_USERS)]
    public class IndexModel : PageModel
    {
        private readonly UserManager userManager;
        private readonly SignInManager signinManager;
        private readonly GameAccountService gameAccountService;
        private readonly EventLogger eventLogger;

        public IndexModel(UserManager userManager,
            SignInManager signinManager,
            GameAccountService gameAccountService,
            EventLogger eventLogger
            )
        {
            this.userManager = userManager;
            this.signinManager = signinManager;
            this.gameAccountService = gameAccountService;
            this.eventLogger = eventLogger;
        }

        [BindProperty(SupportsGet = true)]
        public Guid AccountID { get; set; }

        [BindProperty]
        public InputModel Input{ get; set; }

        public ApplicationUser Account { get; set; }
        public ClaimsPrincipal AccountPrincipal { get; set; }
        public List<GameAccountResponse> ConquerAccounts { get; private set; } = new();

        private async Task<IActionResult> OnLoadAsync()
        {
            await eventLogger.LogAsync(EventLogger.LogType.UserGeneralDataView, true, new 
            {
                AccountID
            });

            Account = await userManager.FindByIdAsync(AccountID.ToString());
            if (Account == null)
            {
                return NotFound();
            }

            AccountPrincipal = await signinManager.CreateUserPrincipalAsync(Account);
            ConquerAccounts = await gameAccountService.GetGameAccountsByParentAsync(AccountID);

            if (Input == null)
            {
                Input = new InputModel
                {
                    UserName = Account.UserName,
                    Email = Account.Email,
                    TwoFactorEnabled = Account.TwoFactorEnabled
                };

                var dateOfBirth = AccountPrincipal.FindFirstValue(JwtClaimTypes.BirthDate);
                if (!string.IsNullOrEmpty(dateOfBirth))
                {
                    Input.BirthDate = DateTime.Parse(dateOfBirth);
                }

                var givenName = AccountPrincipal.FindFirstValue(JwtClaimTypes.GivenName);
                if (!string.IsNullOrEmpty(givenName))
                {
                    Input.GivenName = givenName;
                }

                var familyName = AccountPrincipal.FindFirstValue(JwtClaimTypes.FamilyName);
                if (!string.IsNullOrEmpty(familyName))
                {
                    Input.FamilyName = familyName;
                }

                var locale = AccountPrincipal.FindFirstValue(JwtClaimTypes.Locale);
                if (!string.IsNullOrEmpty(locale))
                {
                    Input.Locale = locale;
                }
            }
            return Page();
        }

        public async Task<IActionResult> OnGetAsync()
        {
            return await OnLoadAsync();
        }

        public async Task<IActionResult> OnPostChangeInformationAsync()
        {
            return await OnLoadAsync();
        }

        public class InputModel
        {
            [Required]
            [StringLength(127, MinimumLength = 6)]
            public string GivenName { get; set; }

            [Required]
            [StringLength(127, MinimumLength = 6)]
            public string FamilyName { get; set; }

            [Required]
            [EmailAddress]
            public string Email { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [StringLength(127, MinimumLength = 6)]
            public string UserName { get; set; }

            [Required]
            public bool Password { get; set; }

            [Required]
            public bool TwoFactorEnabled { get; set; }

            [DataType(DataType.Date)]
            public DateTime? BirthDate { get; set; }

            [DataType(DataType.Text)]
            public string Locale { get; set; }
        }
    }
}
