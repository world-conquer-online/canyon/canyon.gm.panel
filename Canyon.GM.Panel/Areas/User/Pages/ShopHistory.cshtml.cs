using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Services.Identity;
using Canyon.GM.Panel.Services.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Claims;

namespace Canyon.GM.Panel.Areas.User.Pages
{
    [Authorize(Roles = ROLE_MANAGE_USERS)]
    public class ShopHistoryModel : PageModel
    {
        private readonly SignInManager signInManager;
        private readonly BanService banService;
        private readonly UserManager userManager;
        private readonly GameAccountService gameAccountService;
        private readonly EventLogger eventLogger;
        private readonly PurchaseService purchaseService;

        public ShopHistoryModel(
            SignInManager signInManager,
            BanService banService,
            UserManager userManager,
            GameAccountService gameAccountService,
            EventLogger eventLogger,
            PurchaseService purchaseService
            )
        {
            this.signInManager = signInManager;
            this.banService = banService;
            this.userManager = userManager;
            this.gameAccountService = gameAccountService;
            this.eventLogger = eventLogger;
            this.purchaseService = purchaseService;
        }

        [BindProperty(SupportsGet = true)]
        public Guid AccountID { get; set; }
        [BindProperty(SupportsGet = true)]
        public int PageIndex { get; set; }

        public ApplicationUser Account { get; set; }
        public ClaimsPrincipal AccountPrincipal { get; set; }
        public List<PaymentsOrder> PaymentsOrders { get; set; } = new();
        public int PaymentsCount { get; set; }
        public decimal PaymentsTotal { get; set; }
        public decimal PaymentsPending { get; set; }
        public decimal PaymentsRefund { get; set; }

        private async Task<IActionResult> OnLoadAsync()
        {
            await eventLogger.LogAsync(EventLogger.LogType.UserPurchasesView, true, new
            {
                AccountID
            });

            Account = await userManager.FindByIdAsync(AccountID.ToString());
            if (Account == null)
            {
                return NotFound();
            }

            AccountPrincipal = await signInManager.CreateUserPrincipalAsync(Account);
            PaymentsCount = await purchaseService.CountOrdersByUserAsync(Account.Id);
            PaymentsOrders = await purchaseService.QueryUserPurchasesAsync(Account.Id, PageIndex, 25);
            PaymentsTotal = await purchaseService.TotalUserExpensesAsync(Account.Id);
            PaymentsRefund = await purchaseService.TotalUserRefundsAsync(Account.Id);
            PaymentsPending = await purchaseService.TotalUserPendingExpensesAsync(Account.Id);
            return Page();
        }

        public Task<IActionResult> OnGetAsync()
        {
            return OnLoadAsync();
        }
    }
}
