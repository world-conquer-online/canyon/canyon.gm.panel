using Canyon.GM.Panel.Database.Entities.Game.TradeHistory;
using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Services.Identity;
using Canyon.GM.Panel.Services.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Claims;

namespace Canyon.GM.Panel.Areas.User.Pages
{
    [Authorize(Roles = ROLE_MANAGE_USERS)]
    public class TradeHistoryDetailModel : PageModel
    {
        private readonly UserManager userManager;
        private readonly SignInManager signInManager;
        private readonly GameAccountService gameAccountService;
        private readonly RealmService realmService;
        private readonly TradeHistoryService tradeHistoryService;
        private readonly IStringLocalizer stringLocalizer;
        private readonly EventLogger eventLogger;

        public TradeHistoryDetailModel(
            UserManager userManager,
            SignInManager signInManager,
            GameAccountService gameAccountService,
            RealmService realmService,
            TradeHistoryService tradeHistoryService,
            IStringLocalizer stringLocalizer,
            EventLogger eventLogger
        )
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.gameAccountService = gameAccountService;
            this.realmService = realmService;
            this.tradeHistoryService = tradeHistoryService;
            this.stringLocalizer = stringLocalizer;
            this.eventLogger = eventLogger;
        }

        [BindProperty(SupportsGet = true)]
        public Guid AccountID { get; set; }
        [BindProperty(SupportsGet = true)]
        public Guid RealmID { get; set; }
        [BindProperty(SupportsGet = true)]
        public uint TradeID { get; set; }

        public ApplicationUser Account { get; set; }
        public ClaimsPrincipal AccountPrincipal { get; set; }

        public TradeHistory TradeInfo { get; set; }
        public IList<TradeHistoryDetail> TradeHistoryDetails { get; set; } = new List<TradeHistoryDetail>();

        private async Task<IActionResult> OnLoadAsync()
        {
            await eventLogger.LogAsync(EventLogger.LogType.UserTradeHistoryDetailView, true, new
            {
                AccountID,
                TradeID,
                RealmID
            });

            Account = await userManager.FindByIdAsync(AccountID.ToString());
            if (Account == null)
            {
                return NotFound();
            }

            AccountPrincipal = await signInManager.CreateUserPrincipalAsync(Account);

            var accounts = await gameAccountService.GetGameAccountsByParentAsync(AccountID);
            accounts = await gameAccountService.FetchConquerAccountsAsync(accounts);
            accounts = accounts.Where(x => x.Characters.Any(y => y.RealmID == RealmID)).ToList();
            var conquerAccounts = accounts.SelectMany(x => x.Characters.Where(x => x.RealmID == RealmID)).ToList();

            TradeInfo = await tradeHistoryService.GetTradeAsync(RealmID, TradeID);
            if (TradeInfo == null)
            {
                return NotFound();
            }

            if (conquerAccounts.All(x => x.Identity != TradeInfo.TargetId && x.Identity != TradeInfo.CallerId))
            {
                return NotFound();
            }

            TradeHistoryDetails = await tradeHistoryService.GetTradeHistoryDetailsAsync(RealmID, TradeID);
            return Page();
        }

        public Task<IActionResult> OnGetAsync()
        {
            return OnLoadAsync();
        }
    }
}
