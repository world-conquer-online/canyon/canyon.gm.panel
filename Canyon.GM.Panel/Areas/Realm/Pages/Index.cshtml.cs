using Canyon.GM.Panel.Services.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;

namespace Canyon.GM.Panel.Areas.Realm.Pages
{
    [Authorize(Roles = ROLE_MANAGE_USERS)]
    public class IndexModel : PageModel
    {
        private RealmService realmService;
        private readonly EventLogger eventLogger;
        private IStringLocalizer localizer;

        public IndexModel(
            IStringLocalizer localizer, 
            RealmService realmService,
            EventLogger eventLogger
            )
        {
            this.localizer = localizer;
            this.realmService = realmService;
            this.eventLogger = eventLogger;
        }

        [BindProperty(SupportsGet = true)]
        public Guid RealmID { get; set; }

        public RealmService.RealmPublicData Realm { get; set; }

        [BindProperty] public InputModel Input { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            if (RealmID == Guid.Empty)
            {
                Input ??= new InputModel();
                return Page();
            }

            var realm = await realmService.FindByIdAsync(RealmID);
            if (realm == null)
            {
                return RedirectToPage("./Index");
            }

            await eventLogger.LogAsync(EventLogger.LogType.RealmView, true, new
            {
                RealmID
            });

            Realm = realmService.GetRealmData(realm);

            Input ??= new InputModel
            {
                DatabaseHost = Realm.DatabaseHostname,
                DatabasePort = (uint)Realm.DatabasePort,
                DatabaseSchema = Realm.DatabaseSchema,
                DatabaseUser = Realm.DatabaseUsername,

                Username = Realm.RealmUsername,

                GameIPAddress = Realm.GameIPAddress,
                GamePort = Realm.GamePort,

                RpcIPAddress = Realm.RpcIPAddress,
                RpcPort = Realm.RpcPort,

                RealmID = Realm.RealmID,
                RealmName = Realm.RealmName,

                GmToolsHost = Realm.GmToolsAddress,
                GmToolsPort = Realm.GmToolsPort,
                GmToolsUserName = Realm.GmToolsUsername,

                Active = Realm.Active,
                ProductionRealm = Realm.Production
            };

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            EventLogger.LogType logType = EventLogger.LogType.Invalid;
            bool success = false;
            object data = new
            {
                RealmID,
                Input
            };
            try
            {
                if (RealmID == Guid.Empty)
                {
                    logType = EventLogger.LogType.RealmCreation;

                    var duplicate = await realmService.FindByNameAsync(Input.RealmName);
                    if (duplicate != null)
                    {
                        ModelState.AddModelError("Error", "DuplicateRealmName");
                        return Page();
                    }

                    if (string.IsNullOrEmpty(Input.DatabasePass) || string.IsNullOrEmpty(Input.Password))
                    {
                        ModelState.AddModelError("Error", "PasswordsCantBeEmptyOnCreation");
                        return Page();
                    }

                    if (!await realmService.CreateRealmAsync(new RealmService.CreateRealmModel
                    {
                        DatabaseHost = Input.DatabaseHost,
                        DatabasePass = Input.DatabasePass,
                        DatabasePort = Input.DatabasePort.ToString(),
                        DatabaseSchema = Input.DatabaseSchema,
                        DatabaseUser = Input.DatabaseUser,

                        GmToolsAddress = Input.GmToolsHost,
                        GmToolsPort = Input.GmToolsPort,
                        GmToolsUserName = Input.GmToolsUserName,
                        GmToolsPassword = Input.GmToolsPassword,

                        GameIPAddress = Input.GameIPAddress,
                        GamePort = Input.GamePort,
                        RpcIPAddress = Input.RpcIPAddress,
                        RpcPort = Input.RpcPort,

                        RealmName = Input.RealmName,
                        Username = Input.Username,
                        Password = Input.Password,

                        Active = Input.Active,
                        Production = Input.ProductionRealm
                    }))
                    {
                        ModelState.AddModelError("Error", localizer.GetString("CouldNotInsertRealm"));
                        return Page();
                    }

                    success = true;
                    return RedirectToPage("./Realm/List");
                }

                logType = EventLogger.LogType.RealmUpdate;
                if (!await realmService.UpdateRealmAsync(new RealmService.AlterRealmModel
                {
                    DatabaseHost = Input.DatabaseHost,
                    DatabasePass = Input.DatabasePass,
                    DatabasePort = Input.DatabasePort.ToString(),
                    DatabaseSchema = Input.DatabaseSchema,
                    DatabaseUser = Input.DatabaseUser,

                    GmToolsAddress = Input.GmToolsHost,
                    GmToolsPort = Input.GmToolsPort,
                    GmToolsUserName = Input.GmToolsUserName,
                    GmToolsPassword = Input.GmToolsPassword,

                    GameIPAddress = Input.GameIPAddress,
                    GamePort = Input.GamePort,
                    RpcIPAddress = Input.RpcIPAddress,
                    RpcPort = Input.RpcPort,

                    RealmName = Input.RealmName,
                    Username = Input.Username,
                    Password = Input.Password,
                    RealmID = RealmID,

                    Active = Input.Active,
                    Production = Input.ProductionRealm
                }))
                {
                    ModelState.AddModelError("Error", localizer.GetString("CouldNotUpdateRealm"));
                    return Page();
                }

                success = true;
                return RedirectToPage();
            }
            finally
            {
                await eventLogger.LogAsync(logType, success, data);
            }
        }

        public class InputModel
        {
            public Guid RealmID { get; set; }
            [Required]
            [MinLength(4)]
            [MaxLength(16)]
            public string RealmName { get; set; }
            [Required]
            [MaxLength(16)]
            public string GameIPAddress { get; set; }
            [Required]
            [MaxLength(16)]
            public string RpcIPAddress { get; set; } = "127.0.0.1";
            [Required]
            public uint GamePort { get; set; } = 5816;
            [Required]
            public uint RpcPort { get; set; } = 5817;
            [Required]
            [MaxLength(16)]
            public string Username { get; set; }
            [MaxLength(16)]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Required]
            public string DatabaseHost { get; set; } = "localhost";
            [Required]
            public string DatabaseUser { get; set; } = "root";
#if !DEBUG
            [MinLength(20)]
            [MaxLength(100)]
#endif
            [DataType(DataType.Password)]
            public string DatabasePass { get; set; }
            [Required]
            public string DatabaseSchema { get; set; } = "cq";
            [Required]
            public uint DatabasePort { get; set; } = 3306;

            [Required]
            public bool Active { get; set; } = true;
            [Required]
            public bool ProductionRealm { get; set; } = true;

            [Required]
            public string GmToolsHost { get; set; }
            [Required]
            public int GmToolsPort { get; set; } = 4350;
            [Required]
            public string GmToolsUserName { get; set; }
            [DataType(DataType.Password)]
            public string GmToolsPassword { get; set; }

        }
    }
}
