using Canyon.GM.Panel.Sockets.Piglet;
using Canyon.GM.Panel.Sockets.Piglet.Packets;
using Canyon.Network.Packets.Piglet;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;

namespace Canyon.GM.Panel.Areas.Realm.Pages
{
    [Authorize(Roles = ROLE_MANAGE_REALMS)]
    public class ListModel : PageModel
    {
        private readonly RealmService realmService;
        private readonly RealmSocketService realmSocketService;

        public ListModel(RealmService realmService, RealmSocketService realmSocketService)
        {
            this.realmService = realmService;
            this.realmSocketService = realmSocketService;
        }

        public List<RealmData> Realms { get; } = new();

        [BindProperty]
        public RealmAction Input { get; set; }
        

        public async Task<IActionResult> OnGetAsync()
        {
            var realms = await realmService.QueryRealmsAsync(0, int.MaxValue);
            foreach (var realm in realms)
            {
                var realmStatus = realmSocketService.GetServer(realm.RealmID);
                Realms.Add(new RealmData
                {
                    Data = realm,
                    Status = realmStatus?.RealmStatus ?? PigletActor.RealmState.Offline
                });
            }
            return Page();
        }

        public async Task<IActionResult> OnPostStartServerAsync() 
        {
            if (!realmSocketService.IsRealmConnected(Input.RealmID))
            {
                ModelState.AddModelError("Error", "PigletIsNotConnected");
                return await OnGetAsync();
            }

            // todo sanity checks before starting sever
            // todo check if not already starting

            await realmSocketService.SendToRealmAsync(Input.RealmID, new MsgPigletRealmAction
            {
                Data = new MsgPigletRealmAction<PigletActor>.ActionData
                {
                    ActionType = MsgPigletRealmAction<PigletActor>.ActionDataType.StartServer,
                    Strings = new List<string>()
                }
            });
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostAnnounceMaintenanceAsync()
        {
            if (!realmSocketService.IsRealmConnected(Input.RealmID))
            {
                ModelState.AddModelError("Error", "RealmIsNotConnected");
                return await OnGetAsync();
            }

            await realmSocketService.SendToRealmAsync(Input.RealmID, new MsgPigletRealmAnnounceMaintenance
            {
                Data = new MsgPigletRealmAnnounceMaintenance<PigletActor>.AnnounceData
                {
                    WarningMinutes = 5
                }
            });
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostStopServerAsync()
        {
            if (!realmSocketService.IsRealmConnected(Input.RealmID))
            {
                ModelState.AddModelError("Error", "RealmIsNotConnected");
                return await OnGetAsync();
            }

            await realmSocketService.SendToRealmAsync(Input.RealmID, new MsgPigletShutdown
            {
                Data = new MsgPigletShutdown<PigletActor>.ShutdownData
                {
                    Id = int.MaxValue
                }
            });
            return RedirectToPage();
        }

        public class RealmAction
        {
            [Required]
            public Guid RealmID { get; set; }
        }

        public struct RealmData
        {
            public RealmService.RealmPublicListData Data { get; set; }
            public PigletActor.RealmState Status { get; set; }
        }
    }
}
