using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Services.Identity;
using Canyon.GM.Panel.Services.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;

namespace Canyon.GM.Panel.Areas.CreditCard.Pages
{
    [Authorize(Roles = "ManageCreditCards")]
    public class ListModel : PageModel
    {
        private readonly ProductService productService;
        private readonly CreditCardService creditCardService;
        private readonly UserManager userManager;
        private readonly SignInManager signInManager;
        private readonly EventLogger logger;

        public ListModel(
            ProductService productService,
            CreditCardService creditCardService,
            UserManager userManager,
            SignInManager signInManager,
            EventLogger logger
            )
        {
            this.productService = productService;
            this.creditCardService = creditCardService;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.logger = logger;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public List<Product> Products { get; private set; } = new();

        [TempData]
        public Guid? CardId { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            Products = await productService.GetActiveProductsAsync(0, int.MaxValue);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return await OnGetAsync();
            }

            var product = await productService.GetProductByIdAsync(Input.ProductId);
            if (product == null)
            {
                ModelState.AddModelError("Error", "InvalidProduct");
                return await OnGetAsync();
            }

            ApplicationUser currentUser = await userManager.GetUserAsync(User);
            if (currentUser == null)
            {
                await logger.LogAsync(EventLogger.LogType.CreditCardCreationAttempt, false, Input);
                await signInManager.SignOutAsync();
                return RedirectToPage();
            }

            var cards = await creditCardService.GenerateCreditCardsAsync(currentUser, product.CardTypeId, 1);
            if (cards.Count == 0)
            {
                await logger.LogAsync(EventLogger.LogType.CreditCardCreationAttempt, false, Input);
                ModelState.AddModelError("Error", "ErrorGeneratingCard");
                return await OnGetAsync();
            }

            await logger.LogAsync(EventLogger.LogType.CreditCardCreated, true, Input);
            CardId = cards.FirstOrDefault().CardUUID;
            return RedirectToPage();
        }

        public class InputModel
        {
            [Required]
            public Guid ProductId { get; set; }
        }
    }
}
