using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Services.Payments;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Canyon.GM.Panel.Areas.Purchases.Pages
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly PurchaseService purchaseService;
        private readonly MercadoPagoService mercadoPagoService;

        public IndexModel(
            PurchaseService purchaseService,
            MercadoPagoService mercadoPagoService
            )
        {
            this.purchaseService = purchaseService;
            this.mercadoPagoService = mercadoPagoService;
        }

        [BindProperty(SupportsGet = true)]
        public Guid OrderID { get; set; }

        [BindProperty(SupportsGet = true)]
        public string ReturnUrl { get; set; }

        public PaymentsOrder PaymentOrder { get; set; }
        public PaymentsMercadoPago MercadoPagoHistory { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            PaymentOrder = await purchaseService.GetOrderByIdAsync(OrderID);
            if (PaymentOrder == null)
            {
                return NotFound();
            }
            if (PaymentOrder.MethodId == 1) // mercado pago
            {
                MercadoPagoHistory = await mercadoPagoService.QueryMercadoPagoLocalHistoryAsync(OrderID);
            }
            return Page();
        }
    }
}
