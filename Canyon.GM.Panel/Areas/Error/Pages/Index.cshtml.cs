using Canyon.GM.Panel.Models.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Diagnostics;

namespace Canyon.GM.Panel.Areas.Error.Pages
{
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    [IgnoreAntiforgeryToken]
    public class IndexModel : PageModel
    {
        private readonly IStringLocalizer stringLocalizer;

        public IndexModel(
            IStringLocalizer stringLocalizer
            )
        {
            this.stringLocalizer = stringLocalizer;
        }

        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);

        public int? HttpStatus { get; set; }
        public string ErrorPath { get; set; }
        public string ExceptionMessage { get; set; }


        public void OnGet()
        {
            RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;

            var exceptionHandlerPathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            if (exceptionHandlerPathFeature?.Error is not HttpResponseException httpResponseException)
            {
                //ExceptionMessage = $"An unhandled exception has been thrown! Message: {}";
                ExceptionMessage = string.Format(stringLocalizer.GetString("UnknownExceptionThrown"), exceptionHandlerPathFeature?.Error?.Message ?? stringLocalizer.GetString("UnknownError"));
            }
            else
            {
                HttpStatus = httpResponseException.StatusCode;
                ExceptionMessage = httpResponseException.Message;
            }

            if (!string.IsNullOrEmpty(exceptionHandlerPathFeature?.Path))
            {
                ErrorPath = exceptionHandlerPathFeature.Path;
            }
        }
    }
}