using Canyon.GM.Panel.Database.Entities.Local;
using Canyon.GM.Panel.Helpers;
using Canyon.GM.Panel.Models.Articles;
using Canyon.GM.Panel.Services.Core;
using Canyon.GM.Panel.Services.Identity;
using Canyon.GM.Panel.Services.Logging;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Article = Canyon.GM.Panel.Database.Entities.Local.Article;

namespace Canyon.GM.Panel.Areas.Articles.Pages
{
    [Authorize(Roles = ROLE_ARTICLES_MANAGE)]
    public class IndexModel : PageModel
    {
        private readonly ArticleService articleService;
        private readonly UserManager userManager;
        private readonly SignInManager signInManager;
        private readonly IStringLocalizer localizer;
        private readonly EventLogger eventLogger;

        public IndexModel(
            ArticleService articleService, 
            UserManager userManager, 
            SignInManager signInManager, 
            IStringLocalizer localizer,
            EventLogger eventLogger)
        {
            this.articleService = articleService;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.localizer = localizer;
            this.eventLogger = eventLogger;
        }

        [BindProperty] public InputModel Input { get; set; }
        [BindProperty(SupportsGet = true)] public int ArticleId { get; set; }
        [BindProperty(SupportsGet = true)] public string Locale { get; set; }

        public string Author { get; private set; }

        public List<ArticleCategoryModel> Categories { get; private set; } = new();
        public List<ArticleCategoryTypeModel> CategoryTypes { get; private set; } = new();

        public List<Languages> FeaturedLanguages { get; private set; } = new();
        public List<Languages> UnfeaturedLanguages { get; private set; } = new();

        private async Task LoadAsync()
        {
            CategoryTypes = await articleService.QueryActiveArticleCategoryTypeAsync();

            Article article = await articleService.QueryFullArticleByIdAsync(ArticleId);

            string locale = User.GetCurrentLocale();
            if (article == null)
            {
                Input ??= new InputModel
                {
                    ArticleCategoryId = 0,
                    ArticleTypeId = 0,
                    Locale = locale,
                    PublishDate = null
                };

                Author = User.FindFirst(ClaimTypes.Name)?.Value;

                Categories = await articleService.QueryActiveArticleCategoriesByTypeAsync(CategoryTypes.Select(x => x.Id).Min());
            }
            else
            {
                var content =
                    article.Content.FirstOrDefault(x => x.Locale.Equals(locale, StringComparison.InvariantCultureIgnoreCase))
                    ?? article.Content.FirstOrDefault();

                Input ??= new InputModel
                {
                    ArticleCategoryId = article.CategoryId,
                    ArticleTypeId = article.Category.CategoryTypeId,
                    Locale = Locale.Equals(content?.Locale) ? content?.Locale : Locale,
                    Title = content?.Title,
                    Content = content?.Content,
                    PublishDate = article.PublishDate,
                    NotPublished = article.Flags.HasFlag(Article.ArticleFlags.NotPublished),
                    NotListed = article.Flags.HasFlag(Article.ArticleFlags.NotListed)
                };

                if (content?.Writer != null)
                {
                    ClaimsPrincipal principal = await signInManager.CreateUserPrincipalAsync(content.Writer);
                    if (principal != null)
                        Author = principal.FindFirstValue(JwtClaimTypes.Name) ?? localizer.GetString("UserNotFound");
                }
                else
                {
                    Author = User.FindFirst(ClaimTypes.Name)?.Value;
                }

                Categories = await articleService.QueryActiveArticleCategoriesByTypeAsync(Input.ArticleTypeId);
            }

            foreach (Languages language in LanguageService.AvailableLanguages)
            {
                if (article?.Content.Any(x => x.Locale.Equals(language.LanguageCultureName)) == true)
                {
                    FeaturedLanguages.Add(language);
                }
                else
                {
                    UnfeaturedLanguages.Add(language);
                }
            }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            await LoadAsync();

            if (Input == null)
            {
                return RedirectToPage("/Index");
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await LoadAsync();
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (!User.IsInRole(ROLE_ARTICLES_WRITE))
            {
                return Forbid();
            }

            ApplicationUser user = await userManager.GetUserAsync(User);
            (int Result, int ArticleId) result = await articleService.PostArticleAsync(user, ArticleId, Input);

            if (result.Result != StatusCodes.Status200OK || result.ArticleId == 0)
            {
                ModelState.AddModelError("CouldNotSave", localizer.GetString("ErrorSavingArticle"));
                return Page();
            }

            return RedirectToPage(new
            {
                result.ArticleId,
                Input.Locale
            });
        }

        public class InputModel
        {
            [Required]
            [DataType(DataType.Text)] 
            public string Title { get; set; }

            [Required]
            [DataType(DataType.MultilineText)]
            public string Content { get; set; }

            [Required]
            [DataType(DataType.Text)] 
            public string Locale { get; set; }
            [Required] 
            public int ArticleTypeId { get; set; }
            [Required]
            public int ArticleCategoryId { get; set; }
            [DataType(DataType.DateTime)]
            public DateTime? PublishDate { get; set; }
            [Required] 
            public bool NotPublished { get; set; }
            [Required] 
            public bool NotListed { get; set; }
        }
    }
}
