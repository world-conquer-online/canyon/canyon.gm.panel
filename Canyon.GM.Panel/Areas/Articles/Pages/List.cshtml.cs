using Canyon.GM.Panel.Models.Articles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Canyon.GM.Panel.Areas.Articles.Pages
{
    [Authorize]
    public class ListModel : PageModel
    {
        private readonly ArticleService articleService;

        public ListModel(ArticleService articleService)
        {
            this.articleService = articleService;
        }

        public List<ArticleCategoriesType> ArticleCategoriesTypes { get; set; } = new();

        public async Task<IActionResult> OnGetAsync()
        {
            var articleCategorieTypes = await articleService.QueryActiveArticleCategoryTypeAsync();
            foreach (ArticleCategoryTypeModel type in articleCategorieTypes)
            {
                var cat = new ArticleCategoriesType
                {
                    Id = type.Id,
                    Name = type.Name,
                    Categories = new List<ArticleCategory>()
                };

                List<ArticleCategoryModel> categories = await articleService.QueryActiveArticleCategoriesByTypeAsync(type.Id);
                foreach (var category in categories)
                {
                    var childCat = new ArticleCategory
                    {
                        Id = category.Id,
                        Name = category.Name,
                        RealName = category.RealName,
                        Articles = await articleService.QueryByCategoryAsync(category.Id, 0, 25)
                    };
                    cat.Categories.Add(childCat);
                }
                ArticleCategoriesTypes.Add(cat);
            }
            return Page();
        }

        public struct ArticleCategoriesType
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public List<ArticleCategory> Categories { get; set; }
        }

        public struct ArticleCategory
        {
            public int Id { get; set; }
            public string RealName { get; set; }
            public string Name { get; set; }
            public List<AdminArticleListModel> Articles { get; set; }
        }
    }
}
