﻿using Canyon.Network.Security;
using Canyon.Network.Sockets;
using Canyon.Network;
using System.Net.Sockets;
using Canyon.GM.Panel.Helpers;

namespace Canyon.GM.Panel.Sockets.Piglet
{
    public sealed class PigletActor : TcpServerActor
    {
        private static readonly string AesKey;
        private static readonly string AesIV;

        static PigletActor()
        {
            AesKey = ConfigurationHelper.Configuration["Socket:Aes:Key"];
            AesIV = ConfigurationHelper.Configuration["Socket:Aes:IV"];
        }
        
        private readonly PigletServer server;

        public PigletActor(PigletServer server, Socket socket, Memory<byte> buffer, uint partition = 0)
            : base(socket, buffer, 
                  AesCipher.Create(AesKey, AesIV, AesIV), 
                  partition, NetworkDefinition.GM_TOOLS_FOOTER)
        {
            DiffieHellman = DiffieHellman.Create();
            this.server = server;
        }

        public Guid Guid { get; set; }
        public string RealmName { get; set; }
        public RealmState RealmStatus { get; set; }
        public DiffieHellman DiffieHellman { get; }
        public PigletServer Server => server;

        public override Task SendAsync(byte[] packet)
        {
            server.Send(this, packet);
            return Task.CompletedTask;
        }

        public override Task SendAsync(byte[] packet, Func<Task> task)
        {
            server.Send(this, packet, task);
            return Task.CompletedTask;
        }

        public enum ConnectionState
        {
            Disconnected,
            Exchanging,
            Connected
        }

        public enum RealmState
        {
            Offline,
            Online,
            MaintenanceWarning
        }
    }
}