﻿using Canyon.GM.Panel.Sockets.Piglet.Packets;
using Canyon.Network;
using Canyon.Network.Packets;
using Canyon.Network.Sockets;
using System.Net.Sockets;

namespace Canyon.GM.Panel.Sockets.Piglet
{
    public sealed class PigletServer : TcpServerListener<PigletActor>
    {
        private readonly PacketProcessor<PigletActor> packetProcessor;

        public PigletServer(
            ILogger<PigletServer> logger,
            RealmSocketService realmSocketService
        )
            : base(logger, 50, 8192, false, true, NetworkDefinition.GM_TOOLS_FOOTER.Length)
        {
            packetProcessor = new PacketProcessor<PigletActor>(ProcessAsync, 1);
            _ = packetProcessor.StartAsync(CancellationToken.None).ConfigureAwait(false);

            ExchangeStartPosition = 0;
            RealmSocketService = realmSocketService;
        }

        public RealmSocketService RealmSocketService { get; }

        protected override async Task<PigletActor> AcceptedAsync(Socket socket, Memory<byte> buffer)
        {
            var actor = new PigletActor(this, socket, buffer);
            if (socket.Connected)
            {
                await actor.SendAsync(new MsgPigletHandshake(actor.DiffieHellman.PublicKey, actor.DiffieHellman.Modulus, null, null));
                return actor;
            }
            return null;
        }

        protected override bool Exchanged(PigletActor actor, ReadOnlySpan<byte> buffer)
        {
            try
            {
                byte[] packet = buffer.ToArray();
                var length = BitConverter.ToUInt16(packet, 0);
                MsgPigletHandshake msg = new MsgPigletHandshake();
                msg.Decode(buffer[..length].ToArray());

                if (!actor.DiffieHellman.Initialize(msg.Data.PublicKey, msg.Data.Modulus))
                {
                    throw new Exception("Could not initialize Diffie-Helmman!!!");
                }

                actor.Cipher.GenerateKeys(new object[]
                {
                    actor.DiffieHellman.SharedKey.ToByteArrayUnsigned(),
                    msg.Data.EncryptIV,
                    msg.Data.DecryptIV
                });
                return true;
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Error when exchanging GM data. Error: {}", ex.Message);
                return false;
            }
        }

        public override void Send(PigletActor actor, ReadOnlySpan<byte> packet)
        {
            packetProcessor.QueueWrite(actor, packet.ToArray());
        }

        public override void Send(PigletActor actor, ReadOnlySpan<byte> packet, Func<Task> task)
        {
            packetProcessor.QueueWrite(actor, packet.ToArray(), task);
        }

        protected override void Received(PigletActor actor, ReadOnlySpan<byte> packet)
        {
            packetProcessor.QueueRead(actor, packet.ToArray());
        }

        private async Task ProcessAsync(PigletActor actor, byte[] packet)
        {
            // Validate connection
            if (!actor.Socket.Connected)
            {
                return;
            }

            var length = BitConverter.ToUInt16(packet, 0);
            var type = (PacketType)BitConverter.ToUInt16(packet, 2);
            packet = packet.AsSpan()[..length].ToArray();

            try
            {
                MsgBase<PigletActor> msg;
                switch (type)
                {
                    case PacketType.MsgPigletLogin:
                        {
                            msg = new MsgPigletLogin();
                            break;
                        }

                    case PacketType.MsgPigletPing:
                        {
                            msg = new MsgPigletPing();
                            break;
                        }

                    case PacketType.MsgPigletUserCount:
                        {
                            msg = new MsgPigletUserCount();
                            break;
                        }

                    case PacketType.MsgPigletRealmStatus:
                        {
                            msg = new MsgPigletRealmStatus();
                            break;
                        }

                    case PacketType.MsgPigletUserCreditInfo:
                        {
                            msg = new MsgPigletUserCreditInfo();
                            break;
                        }

                    case PacketType.MsgPigletClaimFirstCredit:
                        {
                            msg = new MsgPigletClaimFirstCredit();
                            break;
                        }

                    default:
                        {
                            logger.LogWarning($"Missing packet {type}, Length {length}\n{PacketDump.Hex(packet)}");
                            return;
                        }
                }

                msg.Decode(packet.ToArray());
                await msg.ProcessAsync(actor);
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Error on packet processing!!!");
            }
        }

        protected override void Disconnected(PigletActor actor)
        {
            RealmSocketService.RemoveServer(actor.Guid);
            logger.LogInformation("GM Server '{}' has disconnected", actor.RealmName);
        }
    }
}
