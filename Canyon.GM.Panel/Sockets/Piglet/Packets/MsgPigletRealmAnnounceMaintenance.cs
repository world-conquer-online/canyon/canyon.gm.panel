﻿using Canyon.Network.Packets.Piglet;

namespace Canyon.GM.Panel.Sockets.Piglet.Packets
{
    public sealed class MsgPigletRealmAnnounceMaintenance 
        : MsgPigletRealmAnnounceMaintenance<PigletActor>
    {
        public override Task ProcessAsync(PigletActor client)
        {
            client.RealmStatus = PigletActor.RealmState.MaintenanceWarning;
            return Task.CompletedTask;
        }
    }
}
