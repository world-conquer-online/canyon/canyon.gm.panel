﻿using Canyon.GM.Panel.Helpers;
using Canyon.Network.Packets.Piglet;

namespace Canyon.GM.Panel.Sockets.Piglet.Packets
{
    public sealed class MsgPigletUserCreditInfo : MsgPigletUserCreditInfo<PigletActor>
    {
        private readonly PurchaseService purchaseService;

        public MsgPigletUserCreditInfo()
        {
            purchaseService = ConfigurationHelper.ServiceProvider.GetService<PurchaseService>();
        }

        public override async Task ProcessAsync(PigletActor client)
        {
            // todo must add check to see if player has already claimed first credit gift!!!
            // useridentity is accountid
            bool firstCreditAvailable = purchaseService.IsFirstCreditAvailable((int)Data.UserIdentity, client.Guid);
            var purchases = await purchaseService.QueryAccountPurchasesAsync((int)Data.UserIdentity);

            decimal creditAmount = purchases.Sum(x => x.Price);
            DateTime date30DaysBefore = DateTime.Now.AddDays(-30);
            int creditAmountThisMonth = purchases.Where(x => x.CreatedAt > date30DaysBefore).Count();

            await client.SendAsync(new MsgPigletUserCreditInfoEx
            {
                Data = new MsgPigletUserCreditInfoEx<PigletActor>.FirstCreditData
                {
                    HasFirstCreditToClaim = firstCreditAvailable,
                    UserIdentity = Data.UserIdentity,
                    TotalCreditAmount = creditAmount,
                    TotalPurchases = purchases.Count,
                    TotalPurchasesThisMonth = creditAmountThisMonth
                }
            });
        }
    }
}
