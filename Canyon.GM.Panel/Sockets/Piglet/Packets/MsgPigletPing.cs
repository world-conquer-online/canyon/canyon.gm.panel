﻿using Canyon.Network.Packets.Piglet;

namespace Canyon.GM.Panel.Sockets.Piglet.Packets
{
    public sealed class MsgPigletPing : MsgPigletPing<PigletActor>
    {
        public MsgPigletPing()
        {
            Data = new PingData
            {
                TickCount = Environment.TickCount64
            };
        }

        public override Task ProcessAsync(PigletActor client)
        {
            return Task.CompletedTask;
        }
    }
}
