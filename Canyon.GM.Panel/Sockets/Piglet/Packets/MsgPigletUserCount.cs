﻿using Canyon.Backend.Web.Services;
using Canyon.GM.Panel.Helpers;
using Canyon.Network.Packets.Piglet;

namespace Canyon.GM.Panel.Sockets.Piglet.Packets
{
    public sealed class MsgPigletUserCount : MsgPigletUserCount<PigletActor>
    {
        public override async Task ProcessAsync(PigletActor client)
        {
            var realmService = ConfigurationHelper.ServiceProvider.GetService<RealmStatusService>();
            await realmService.UpdateRealmStatusAsync(client.Guid, RealmStatusService.ServerUp, Data.Current, Data.Max);
        }
    }
}
