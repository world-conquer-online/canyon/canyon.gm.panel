﻿using Canyon.GM.Panel.Helpers;
using Canyon.Network.Packets.Piglet;

namespace Canyon.GM.Panel.Sockets.Piglet.Packets
{
    public sealed class MsgPigletClaimFirstCredit : MsgPigletClaimFirstCredit<PigletActor>
    {
        private readonly PurchaseService purchaseService;

        public MsgPigletClaimFirstCredit()
        {
            purchaseService = ConfigurationHelper.ServiceProvider.GetService<PurchaseService>();
        }

        public override Task ProcessAsync(PigletActor client)
        {
            return purchaseService.SetFirstCreditAsync((int)Data.AccountId, client.Guid);
        }
    }
}
