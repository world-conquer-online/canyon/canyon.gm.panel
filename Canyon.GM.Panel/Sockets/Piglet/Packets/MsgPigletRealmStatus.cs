﻿using Canyon.Backend.Web.Services;
using Canyon.GM.Panel.Helpers;
using Canyon.Network.Packets.Piglet;

namespace Canyon.GM.Panel.Sockets.Piglet.Packets
{
    public sealed class MsgPigletRealmStatus : MsgPigletRealmStatus<PigletActor>
    {
        public override async Task ProcessAsync(PigletActor client)
        {
            var realmStatusService = ConfigurationHelper.ServiceProvider.GetRequiredService<RealmStatusService>();
            await realmStatusService.UpdateRealmStatusAsync(client.Guid, Data.Status, 0, 0);

            switch (Data.Status)
            {
                case RealmStatusService.ServerDown:
                    {
                        client.RealmStatus = PigletActor.RealmState.Offline;
                        break;
                    }
                default:
                    {
                        if (client.RealmStatus == PigletActor.RealmState.MaintenanceWarning)
                        {
                            // server is entering maintenance, no update socket
                            return;
                        }
                        client.RealmStatus = PigletActor.RealmState.Online;
                        break;
                    }
            }
        }
    }
}
