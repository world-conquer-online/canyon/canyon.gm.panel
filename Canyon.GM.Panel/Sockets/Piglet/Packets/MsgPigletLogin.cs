﻿using Canyon.GM.Panel.Helpers;
using Canyon.Network.Packets.Piglet;
using System.Security.Cryptography;

namespace Canyon.GM.Panel.Sockets.Piglet.Packets
{
    public sealed class MsgPigletLogin : MsgPigletLogin<PigletActor>
    {
        private static readonly ILogger logger = ConfigurationHelper.ServiceProvider.GetService<ILogger<MsgPigletLogin>>();

        private readonly RealmService realmService;
        private readonly RealmSocketService realmSocketService;

        public MsgPigletLogin()
        {
            realmService = ConfigurationHelper.ServiceProvider.GetService<RealmService>();
            realmSocketService = ConfigurationHelper.ServiceProvider.GetService<RealmSocketService>();
        }

        public override async Task ProcessAsync(PigletActor client)
        {            
            var realm = await realmService.FindByNameAsync(Data.RealmName);
            if (realm == null)
            {
                logger.LogWarning("Realm [{realmName}] does not exist!", Data.RealmName);
                client.Disconnect();
                return;
            }

            string userName = realmService.DecryptData(realm.GmToolsUserName);
            string password = realmService.DecryptData(realm.GmToolsPassword);

            if (!userName.Equals(Data.UserName)
                || !password.Equals(Data.Password))
            {
                logger.LogWarning("Invalid username or password for {realmName}!", Data.RealmName);
                client.Disconnect();
                return;
            }

            if (realmSocketService.IsRealmConnected(realm.RealmID))
            {
                var connectedRealm = realmSocketService.GetServer(realm.RealmID);
                connectedRealm?.Disconnect();
                client.Disconnect();
                logger.LogWarning("Server [{},{}] was already connected!", realm.RealmID, realm.Name);
                return;
            }

            client.Guid = realm.RealmID;
            client.RealmName = Data.RealmName;
            realmSocketService.AddServer(client);

            logger.LogInformation("Connected with success with GM Server [{realmName}]", Data.RealmName);
            await client.SendAsync(new MsgPigletLoginEx
            {
                Data = new MsgPigletLoginEx<PigletActor>.LoginExData
                {
                    Result = RandomNumberGenerator.GetInt32(int.MaxValue)
                }
            });
        }
    }
}
