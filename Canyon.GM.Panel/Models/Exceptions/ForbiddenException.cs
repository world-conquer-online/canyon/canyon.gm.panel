﻿namespace Canyon.GM.Panel.Models.Exceptions
{
    public sealed class ForbiddenException : HttpResponseException
    {
        public ForbiddenException(object value = null) : base(StatusCodes.Status403Forbidden, value)
        {
        }

        public ForbiddenException(Exception exception) : base(StatusCodes.Status403Forbidden, exception)
        {
        }

        public ForbiddenException(string message) : base(StatusCodes.Status403Forbidden, message)
        {
        }

        public ForbiddenException(string message, Exception exception) : base(StatusCodes.Status403Forbidden, message, exception)
        {
        }

        public ForbiddenException(string message, Exception exception, object value) : base(StatusCodes.Status403Forbidden, message, exception, value)
        {
        }
    }
}
