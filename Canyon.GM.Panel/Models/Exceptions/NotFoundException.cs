﻿namespace Canyon.GM.Panel.Models.Exceptions
{
    public sealed class NotFoundException : HttpResponseException
    {
        public NotFoundException(object value = null) : base(StatusCodes.Status404NotFound, value)
        {
        }

        public NotFoundException(Exception exception) : base(StatusCodes.Status404NotFound, exception)
        {
        }

        public NotFoundException(string message) : base(StatusCodes.Status404NotFound, message)
        {
        }

        public NotFoundException(string message, Exception exception) : base(StatusCodes.Status404NotFound, message, exception)
        {
        }

        public NotFoundException(string message, Exception exception, object value) : base(StatusCodes.Status404NotFound, message, exception, value)
        {
        }
    }
}
