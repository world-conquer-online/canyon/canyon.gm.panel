﻿namespace Canyon.GM.Panel.Models.Exceptions
{
    public abstract class HttpResponseException : Exception
    {
        public HttpResponseException(int statusCode, object value = null)
        {
            StatusCode = statusCode;
            Value = value;
        }

        public HttpResponseException(int statusCode, Exception exception)
            : base(exception.Message, exception)
        {
            StatusCode = statusCode;
        }

        public HttpResponseException(int statusCode, string message)
            : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpResponseException(int statusCode, string message, Exception exception)
            : base(message, exception)
        {
            StatusCode = statusCode;
        }

        public HttpResponseException(int statusCode, string message, Exception exception, object value)
            : base(message, exception)
        {
            StatusCode = statusCode;
            Value = value;
        }

        public int StatusCode { get; }
        public object Value { get; }
    }
}
