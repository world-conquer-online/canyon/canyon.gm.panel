﻿namespace Canyon.GM.Panel.Models.Exceptions
{
    public class InternalServerErrorException : HttpResponseException
    {
        private const int STATUS_CODE = StatusCodes.Status400BadRequest;

        public InternalServerErrorException(object value = null) : base(STATUS_CODE, value)
        {
        }

        public InternalServerErrorException(Exception exception) : base(STATUS_CODE, exception)
        {
        }

        public InternalServerErrorException(string message) : base(STATUS_CODE, message)
        {
        }

        public InternalServerErrorException(string message, Exception exception) : base(STATUS_CODE, message, exception)
        {
        }

        public InternalServerErrorException(string message, Exception exception, object value) : base(STATUS_CODE, message, exception, value)
        {
        }
    }
}
