﻿namespace Canyon.GM.Panel.Models.Exceptions
{
    public sealed class BadRequestException : HttpResponseException
    {
        private const int STATUS_CODE = StatusCodes.Status400BadRequest;

        public BadRequestException(object value = null) : base(STATUS_CODE, value)
        {
        }

        public BadRequestException(Exception exception) : base(STATUS_CODE, exception)
        {
        }

        public BadRequestException(string message) : base(STATUS_CODE, message)
        {
        }

        public BadRequestException(string message, Exception exception) : base(STATUS_CODE, message, exception)
        {
        }

        public BadRequestException(string message, Exception exception, object value) : base(STATUS_CODE, message, exception, value)
        {
        }
    }
}
