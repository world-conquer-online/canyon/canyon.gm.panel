﻿namespace Canyon.GM.Panel.Models.Exceptions
{
    public class UnauthorizedException : HttpResponseException
    {
        public UnauthorizedException(object value = null) : base(StatusCodes.Status401Unauthorized, value)
        {
        }

        public UnauthorizedException(Exception exception) : base(StatusCodes.Status401Unauthorized, exception)
        {
        }

        public UnauthorizedException(string message) : base(StatusCodes.Status401Unauthorized, message)
        {
        }

        public UnauthorizedException(string message, Exception exception) : base(StatusCodes.Status401Unauthorized, message, exception)
        {
        }

        public UnauthorizedException(string message, Exception exception, object value) : base(StatusCodes.Status401Unauthorized, message, exception, value)
        {
        }
    }
}
