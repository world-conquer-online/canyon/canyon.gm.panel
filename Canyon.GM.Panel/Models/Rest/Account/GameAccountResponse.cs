﻿using static Canyon.GM.Panel.Services.GameAccountService;

namespace Canyon.GM.Panel.Models.Rest.Account
{
    public class GameAccountResponse
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int Authority { get; set; }
        public string AuthorityName { get; set; }
        public GameAccountFlag Flag { get; set; }
        public DateTime? UnlockTime { get; set; }
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
        public Guid ParentId { get; set; }

        public List<ConquerGameCharacterResponse> Characters { get; set; } = new();

        public class ConquerGameCharacterResponse
        {
            public Guid RealmID { get; set; }
            public string RealmName { get; set; }
            public uint Identity { get; set; }
            public string Name { get; set; }
            public byte Level { get; set; }
            public byte Metempsychosis { get; set; }
            public ulong Money { get; set; }
            public uint ConquerPoints { get; set; }
            public uint ConquerPointsMono { get; set; }
        }
    }
}
