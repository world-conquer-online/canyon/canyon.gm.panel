﻿namespace Canyon.GM.Panel.Models.Rest.Bans
{
    public class BanListResponse
    {
        public string Type { get; set; }
        public Guid UUID { get; set; }
        public string UserName { get; set; }
        public string BannedBy { get; set; }
        public string Reason { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
