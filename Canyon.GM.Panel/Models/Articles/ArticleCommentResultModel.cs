﻿namespace Canyon.GM.Panel.Models.Articles
{
    public class ArticleCommentResultModel
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string CreatorName { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}