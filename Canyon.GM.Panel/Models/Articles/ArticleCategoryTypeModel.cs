﻿namespace Canyon.GM.Panel.Models.Articles
{
    public class ArticleCategoryTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}