﻿using Canyon.GM.Panel.Services.Payments;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Canyon.GM.Panel.API
{
    [Route("api/payments/[action]")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        private readonly ILogger<PaymentsController> logger;
        private readonly PaymentsService paymentsService;

        public PaymentsController(
            ILogger<PaymentsController> logger,
            PaymentsService paymentsService
            )
        {
            this.logger = logger;
            this.paymentsService = paymentsService;
        }

        [ActionName("mercado-pago/notify/{orderId}")]
        [HttpPost]
        public async Task<IActionResult> OnPostMercadoPagoNotificationAsync([FromRoute] Guid orderId, MercadoPagoNotifyModel input)
        {
            logger.LogInformation("Mercado pago notification [{},{}] received!\n{}", input.Type, input.Action, JsonConvert.SerializeObject(input));
            if (((input.LiveMode is bool liveMode && liveMode) || ("false".Equals(input.LiveMode.ToString())) && "test".Equals(input.Type)))
            {
                logger.LogInformation("Test payment processed! Result OK");
                return Ok();
            }

            if (!"payment".Equals(input.Type))
            {
                logger.LogError("Notification is not of type \"payment\"!");
                return BadRequest("Only payments!");
            }

            logger.LogInformation("Processing payment ID: {}, OrderID: {}", input.Data.Id.ToString(), orderId);
            await paymentsService.PushMercadoPagoPaymentInformationAsync(orderId, long.Parse(input.Data.Id.ToString()));
            return Ok();
        }

        public class MercadoPagoNotifyModel
        {
            [JsonPropertyName("live_mode")]
            public object LiveMode { get; set; }
            [JsonPropertyName("type")]
            public string Type { get; set; }
            [JsonPropertyName("action")]
            public string Action { get; set; }
            [Required]
            [JsonPropertyName("data")]
            public NotificationData Data { get; set; }

            public class NotificationData
            {
                [JsonPropertyName("id")]
                public object Id { get; set; }
            }
        }
    }
}
