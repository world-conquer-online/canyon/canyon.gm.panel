﻿using Canyon.GM.Panel.Models.Articles;
using Canyon.GM.Panel.Services.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Canyon.GM.Panel.API
{
    [Authorize]
    [Route("api/articles/[action]")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
        private readonly ArticleService articleService;
        private readonly UserManager userManager;

        public ArticlesController(ArticleService service, UserManager userManager)
        {
            articleService = service;
            this.userManager = userManager;
        }

        /// <summary>
        ///     Returns a list of results.
        /// </summary>
        /// <remarks>
        ///     This will return a list with the article categories from a specific type. This should not contain much
        ///     data, so this isn't paginated.
        /// </remarks>
        /// <param name="type">The category type to be exibhited.</param>
        /// <response code="200">A list with the article categories.</response>
        /// <response code="500">Internal server error.</response>
        [ActionName("GetActiveArticleCategoriesByType")]
        [HttpGet(Name = "GetActiveArticleCategoriesByType")]
        [ProducesResponseType(typeof(List<ArticleCategoryModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetActiveArticleCategoriesByTypeV1Async(int type)
        {
            return Ok(await articleService.QueryActiveArticleCategoriesByTypeAsync(type));
        }

        [Authorize(Policy = "AdministrateComments")]
        [ActionName("ChangeArticleOrder")]
        [HttpPatch]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ChangeArticleOrderV1Async([FromBody] PatchArticleOrder patch)
        {
            int result = await articleService.ChangeArticleOrderAsync(patch.PostId, patch.Order);
            return StatusCode(result);
        }

        [Authorize(Policy = ROLE_ARTICLES_MANAGE)]
        [ActionName("RebuildOrdering")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> RebuildOrderingAsync()
        {
            await articleService.RebuildArticleOrderAsync();
            return Ok();
        }

        public class PostCommentModel
        {
            public int PostId { get; set; }
            public string Message { get; set; }
            public int Rating { get; set; }
        }

        public class PatchArticleOrder
        {
            public int PostId { get; set; }
            public int Order { get; set; }
        }
    }

}
