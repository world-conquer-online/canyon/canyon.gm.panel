﻿using Canyon.GM.Panel.Services;
using Canyon.GM.Panel.Services.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Canyon.GM.Panel.API
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RealmMaintenanceController : ControllerBase
    {
        private readonly EventLogger logger;

        public RealmMaintenanceController(
            EventLogger logger,
            MaintenanceService maintenanceService
            )
        {
            this.logger = logger;
        }

    }
}
