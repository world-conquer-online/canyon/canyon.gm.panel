﻿namespace Canyon.GM.Panel
{
    public static class GlobalConstants
    {
        public const string TITLE = "Title";
        public const string CONTAINER_IMAGE_NAME = "ContainerImage";
        public const string CONTAINER_TITLE_NAME = "ContainerTitle";
        public const string CONTAINER_SUBTITLE_NAME = "ContainerSubTitle";

        public const string META_DESCRIPTION = "Meta_Description";

        public const string ROLE_MANAGE_USERS = "ManageUsers";
        public const string ROLE_MANAGE_REALMS = "ManageRealms";
        public const string ROLE_ARTICLES_MANAGE = "ManageArticles";
        public const string ROLE_ARTICLES_WRITE = "WriteArticles";
        public const string ROLE_ARTICLES_DELETE = "DeleteArticles";
        public const string ROLE_EXCEED_GAME_ACCOUNT_LIMIT = "ExceedGameAccountLimit";
        public const string ROLE_SUPER_ADMINISTRATOR = "SuperAdministrator";
        public const string ROLE_BAN_USERS = "BanUsers";
        public const string ROLE_LOCK_USERS = "LockUsers";
        public const string ROLE_CANCEL_BAN = "CancelBan";
        public const string ROLE_FORGIVE_BAN = "ForgiveBan";
        public const string ROLE_FALSE_POSITIVE_BAN = "FalsePositiveBan";
    }
}
