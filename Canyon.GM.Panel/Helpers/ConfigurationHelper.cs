﻿namespace Canyon.GM.Panel.Helpers
{
    public static class ConfigurationHelper
    {
        public static IConfiguration Configuration { get; set; }
        public static IServiceProvider ServiceProvider { get; set; }
    }
}
