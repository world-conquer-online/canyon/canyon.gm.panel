﻿namespace Canyon.GM.Panel.Helpers
{
    public static class UnixTimestamp
    {
        public const int TIME_SECONDS_MINUTE = 60;
        public const int TIME_SECONDS_HOUR = 60 * TIME_SECONDS_MINUTE;
        public const int TIME_SECONDS_DAY = 24 * TIME_SECONDS_HOUR;
        public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();

        public static DateTime ToDateTime(uint timestamp)
        {
            return UnixEpoch.AddSeconds(timestamp);
        }

        public static DateTime ToDateTime(int timestamp)
        {
            return UnixEpoch.AddSeconds(timestamp);
        }

        public static int Now => Convert.ToInt32((DateTime.Now - UnixEpoch)
                                   .TotalSeconds);

        public static long NowMs => Convert.ToInt64((DateTime.Now - UnixEpoch)
                                   .TotalMilliseconds);

        public static int Timestamp(DateTime time)
        {
            return Convert.ToInt32((time - UnixEpoch).TotalSeconds);
        }
    }
}
