﻿namespace Canyon.GM.Panel.Helpers
{
    public static class HttpRequestHelper
    {
        public static string GetClientIpAddress(this HttpRequest httpRequest)
        {
            return httpRequest.HttpContext?.Connection?.RemoteIpAddress?.ToString() ?? "Unknown";
        }
    }
}
