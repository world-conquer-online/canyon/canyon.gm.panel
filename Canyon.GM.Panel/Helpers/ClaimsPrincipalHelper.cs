﻿using Canyon.GM.Panel.Services.Identity;
using IdentityModel;
using System.Globalization;
using System.Security.Claims;

namespace Canyon.GM.Panel.Helpers
{
    public static class ClaimsPrincipalHelper
    {
        public static string GetCurrentLocale(this ClaimsPrincipal principal)
        {
            return principal.FindFirstValue(JwtClaimTypes.Locale)
                ?? CultureInfo.CurrentUICulture.Name;
        }

        public static string GetNullableCurrentLocale(this ClaimsPrincipal principal)
        {
            return principal.FindFirstValue(JwtClaimTypes.Locale);
        }

        public static Guid? GetId(this ClaimsPrincipal principal)
        {
            return Guid.TryParse(principal.FindFirstValue(ClaimTypes.NameIdentifier), out var result) ? result : null;
        }

        public static async Task<bool> IsUserConfirmedAsync(this ClaimsPrincipal principal, UserManager userManager)
        {
            var user = await userManager.GetUserAsync(principal);
            if (user == null)
            {
                return false;
            }
            return user.EmailConfirmed;
        }
    }
}
