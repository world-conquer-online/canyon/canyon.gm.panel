﻿namespace Canyon.GM.Panel.Helpers
{
    public static class ItemHelper
    {
        public static bool IsBow(uint type)
        {
            return GetItemSubType(type) == 500;
        }

        public static int GetQuality(uint type)
        {
            return (int)(type % 10);
        }

        public static bool IsArtifact(uint type)
        {
            return type >= 800000 && type < 900000;
        }

        public static bool IsArrowSort(uint type)
        {
            return GetItemtype(type) == 50000 && type != TYPE_JAR && !IsRing(type) && !IsBangle(type);
        }

        public static ItemSort GetItemSort(uint type)
        {
            return (ItemSort)(type % 10000000 / 100000);
        }

        public static int GetItemtype(uint type)
        {
            if (GetItemSort(type) == ItemSort.ItemsortWeaponSingleHand
                || GetItemSort(type) == ItemSort.ItemsortWeaponDoubleHand)
            {
                return (int)(type % 100000 / 1000 * 1000);
            }

            return (int)(type % 100000 / 10000 * 10000);
        }

        public static int GetItemSubType(uint type)
        {
            return (int)(type % 1000000 / 1000);
        }

        public static int GetLevel(uint type)
        {
            return (int)type % 1000 / 10;
        }

        public static bool IsExpend(uint type)
        {
            return IsArrowSort(type)
                   || GetItemSort(type) == ItemSort.ItemsortUsable
                   || GetItemSort(type) == ItemSort.ItemsortUsable2
                   || GetItemSort(type) == ItemSort.ItemsortUsable3
                   || GetItemSort(type) == (ItemSort)30;
        }

        public static bool IsWeaponOneHand(uint type)
        {
            return GetItemSort(type) == ItemSort.ItemsortWeaponSingleHand;
        } // single hand use

        public static bool IsWeaponTwoHand(uint type)
        {
            return GetItemSort(type) == ItemSort.ItemsortWeaponDoubleHand;
        } // two hand use

        public static bool IsWeaponProBased(uint type)
        {
            return GetItemSort(type) == ItemSort.ItemsortWeaponProfBased;
        } // professional hand use

        public static bool IsWeapon(uint type)
        {
            return IsWeaponOneHand(type) || IsWeaponTwoHand(type) || IsWeaponProBased(type);
        }

        public static bool IsHelmet(uint type)
        {
            return type >= 110000 && type < 120000 || type >= 140000 && type < 150000 || type >= 123000 && type < 124000;
        }

        public static bool IsNeck(uint type)
        {
            return type >= 120000 && type < 123000;
        }

        public static bool IsNecklace(uint type)
        {
            return type >= 120000 && type < 121000;
        }

        public static bool IsBag(uint type)
        {
            return type >= 121000 && type < 122000;
        }

        public static bool IsRing(uint type)
        {
            return type >= 150000 && type < 152000;
        }
        public static bool IsBangle(uint type)
        {
            return type >= 152000 && type < 153000;
        }
        public static bool IsShoes(uint type)
        {
            return type >= 160000 && type < 161000;
        }

        public static bool IsGourd(uint type)
        {
            return type >= 2100000 && type < 2200000;
        }

        public static bool IsGarment(uint type)
        {
            return type >= 170000 && type < 200000;
        }

        public static bool IsCrop(uint type)
        {
            return type >= 203000 && type < 204000;
        }

        public static bool IsDefenseTalisman(uint type)
        {
            return type >= 202000 && type < 203000;
        }

        public static bool IsAttackTalisman(uint type)
        {
            return type >= 201000 && type < 202000;
        }

        public static bool IsTalisman(uint type)
        {
            return IsAttackTalisman(type) || IsDefenseTalisman(type) || IsCrop(type);
        }

        public static bool IsShieldAccessory(uint type)
        {
            return GetItemSubType(type) == 380;
        }

        public static bool IsBowAccessory(uint type)
        {
            return GetItemSubType(type) == 370;
        }

        public static bool IsTwoHandedAccessory(uint type)
        {
            return GetItemSubType(type) == 350;
        }

        public static bool IsOneHandedAccessory(uint type)
        {
            return GetItemSubType(type) == 360;
        }

        public static bool IsAccessory(uint type)
        {
            return IsOneHandedAccessory(type) || IsTwoHandedAccessory(type) || IsBowAccessory(type) || IsShieldAccessory(type);
        }

        public static bool IsMountArmor(uint type)
        {
            return type / 1000 == 200;
        }

        public static bool IsMount(uint type)
        {
            return type == 300000;
        }

        public static bool IsEquipment(uint type)
        {
            return IsHelmet(type) || IsNeck(type) || IsRing(type) || IsBangle(type) || IsWeapon(type) || IsArmor(type) || IsShoes(type) || IsShield(type) || IsTalisman(type) || IsHossuType(type);
        }

        public static bool IsBackswordType(uint type)
        {
            uint subType = type / 1000;
            return subType == 421 || subType == 620;
        }

        public static bool IsHossuType(uint type)
        {
            return type / 1000 == 619;
        }

        public static bool IsEquipEnable(uint type)
        {
            return IsEquipment(type) || IsArrowSort(type) || IsGourd(type) || IsGarment(type) || IsTalisman(type) || IsMount(type) || IsMountArmor(type) || IsAccessory(type);
        }

        public static bool IsArmor(uint type)
        {
            return type / 10000 == 13;
        }

        public static bool IsMedicine(uint type)
        {
            return type >= 1000000 && type <= 1049999;
        }

        public static bool IsShield(uint nType)
        {
            return nType / 1000 == 900;
        }

        public static ItemPosition GetPosition(uint type)
        {
            if (IsHelmet(type))
            {
                return ItemPosition.Headwear;
            }

            if (IsNeck(type))
            {
                return ItemPosition.Necklace;
            }

            if (IsRing(type))
            {
                return ItemPosition.Ring;
            }

            if (IsBangle(type))
            {
                return ItemPosition.Ring;
            }

            if (IsWeapon(type))
            {
                return ItemPosition.RightHand;
            }

            if (IsShield(type))
            {
                return ItemPosition.LeftHand;
            }

            if (IsArrowSort(type))
            {
                return ItemPosition.LeftHand;
            }

            if (IsArmor(type))
            {
                return ItemPosition.Armor;
            }

            if (IsShoes(type))
            {
                return ItemPosition.Boots;
            }

            if (IsGourd(type))
            {
                return ItemPosition.Gourd;
            }

            if (IsGarment(type))
            {
                return ItemPosition.Garment;
            }

            return ItemPosition.Inventory;
        }

        public static string ItemFullName(string name, uint type, byte composition, byte gem1, byte gem2, byte blessing)
        {
            if (IsEquipEnable(type))
            {
                string result = name;
                if (GetQuality(type) == 9)
                {
                    result = $"Super {name}";
                }
                else if (GetQuality(type) == 8)
                {
                    result = $"Elite {name}";
                }
                else if (GetQuality(type) == 7)
                {
                    result = $"Unique {name}";
                }
                else if (GetQuality(type) == 6)
                {
                    result = $"Refined {name}";
                }
                else if (GetQuality(type) == 0)
                {
                    result = $"Super {name}";
                }
                
                if (composition > 0)
                {
                    result += $"(+{composition})";
                }

                if (gem1 > 0 && gem2 > 0)
                {
                    result += $" Two-socketed";
                }
                else if (gem1 > 0 || gem2 > 0)
                {
                    result += $" One-socketed";
                }

                if (blessing > 0)
                {
                    result += $" -{blessing}%";
                }
                return result;
            }
            return name;
        }

        public static string GetEquipmentTypeName(uint type)
        {
            if (IsHelmet(type))
            {
                return "Helmet";
            }
            if (IsNecklace(type))
            {
                return "Necklace";
            }
            if (IsBag(type))
            {
                return "Bag";
            }
            if (IsRing(type))
            {
                return "Ring";
            }
            if (IsBangle(type))
            {
                return "Bangle";
            }
            if (IsWeapon(type))
            {
                return "Weapon";
            }
            if (IsArmor(type))
            {
                return "Armor";
            }
            if (IsShoes(type))
            {
                return "Shoes";
            }
            if (IsGourd(type))
            {
                return "Gourd";
            }
            if (IsGarment(type))
            {
                return "Garment";
            }
            if (IsAccessory(type))
            {
                return "Accessory";
            }
            if (IsAttackTalisman(type))
            {
                return "AttackTalisman";
            }
            if (IsDefenseTalisman(type))
            {
                return "DefenseTalisman";
            }
            if (IsCrop(type))
            {
                return "Crop";
            }
            if (IsMount(type))
            {
                return "Mount";
            }
            if (IsMountArmor(type))
            {
                return "MountArmor";
            }
            if (IsExpend(type))
            {
                return "Expend";
            }
            if (IsShield(type))
            {
                return "Shield";
            }
            return "NotMapped";
        }

        public enum ItemSort
        {
            ItemsortFinery = 1,
            ItemsortMount = 3,
            ItemsortWeaponSingleHand = 4,
            ItemsortWeaponDoubleHand = 5,
            ItemsortWeaponProfBased = 6,
            ItemsortUsable = 7,
            ItemsortWeaponShield = 9,
            ItemsortUsable2 = 10,
            ItemsortUsable3 = 12,
            ItemsortAccessory = 3,
            ItemsortTwohandAccessory = 35,
            ItemsortOnehandAccessory = 36,
            ItemsortBowAccessory = 37,
            ItemsortShieldAccessory = 38
        }

        public enum ItemEffect : ushort
        {
            None = 0,
            Poison = 200,
            Life = 201,
            Mana = 202,
            Shield = 203,
            Horse = 100
        }

        public enum SocketGem : byte
        {
            NormalPhoenixGem = 1,
            RefinedPhoenixGem = 2,
            SuperPhoenixGem = 3,

            NormalDragonGem = 11,
            RefinedDragonGem = 12,
            SuperDragonGem = 13,

            NormalFuryGem = 21,
            RefinedFuryGem = 22,
            SuperFuryGem = 23,

            NormalRainbowGem = 31,
            RefinedRainbowGem = 32,
            SuperRainbowGem = 33,

            NormalKylinGem = 41,
            RefinedKylinGem = 42,
            SuperKylinGem = 43,

            NormalVioletGem = 51,
            RefinedVioletGem = 52,
            SuperVioletGem = 53,

            NormalMoonGem = 61,
            RefinedMoonGem = 62,
            SuperMoonGem = 63,

            NormalTortoiseGem = 71,
            RefinedTortoiseGem = 72,
            SuperTortoiseGem = 73,

            NormalThunderGem = 101,
            RefinedThunderGem = 102,
            SuperThunderGem = 103,

            NormalGloryGem = 121,
            RefinedGloryGem = 122,
            SuperGloryGem = 123,

            NoSocket = 0,
            EmptySocket = 255
        }

        public enum ItemPosition : ushort
        {
            Inventory = 0,
            EquipmentBegin = 1,
            Headwear = 1,
            Necklace = 2,
            Armor = 3,
            RightHand = 4,
            LeftHand = 5,
            Ring = 6,
            Gourd = 7,
            Boots = 8,
            Garment = 9,
            AttackTalisman = 10,
            DefenceTalisman = 11,
            Steed = 12,
            RightHandAccessory = 15,
            LeftHandAccessory = 16,
            SteedArmor = 17,
            Crop = 18,
            EquipmentEnd = Crop,

            AltHead = 21,
            AltNecklace = 22,
            AltArmor = 23,
            AltWeaponR = 24,
            AltWeaponL = 25,
            AltRing = 26,
            AltBottle = 27,
            AltBoots = 28,
            AltGarment = 29,
            AltFan = 30,
            AltTower = 31,
            AltSteed = 32,
            AltEquipmentEnd = AltSteed,

            UserLimit = 199,

            /// <summary>
            ///     Warehouse
            /// </summary>
            Storage = 201,

            /// <summary>
            ///     House WH
            /// </summary>
            Trunk = 202,

            /// <summary>
            ///     Sashes
            /// </summary>
            Chest = 203,
            ChestPackage = 204,
            Auction = 210,

            Detained = 250,
            Floor = 254
        }

        public enum ItemColor : byte
        {
            None,
            Black = 2,
            Orange = 3,
            LightBlue = 4,
            Red = 5,
            Blue = 6,
            Yellow = 7,
            Purple = 8,
            White = 9
        }

        public enum ChangeOwnerType : byte
        {
            DropItem,
            PickupItem,
            TradeItem,
            CreateItem,
            DeleteItem,
            ItemUsage,
            DeleteDroppedItem,
            InvalidItemType,
            BoothSale,
            ClearInventory,
            DetainEquipment
        }

        /// <summary>
        /// Item is owned by the holder. Cannot be traded or dropped.
        /// </summary>
        public const int ITEM_MONOPOLY_MASK = 1;
        /// <summary>
        /// Item cannot be stored.
        /// </summary>
        public const int ITEM_STORAGE_MASK = 2;
        /// <summary>
        /// Item cannot be dropped.
        /// </summary>
        public const int ITEM_DROP_HINT_MASK = 4;
        /// <summary>
        /// Item cannot be sold.
        /// </summary>
        public const int ITEM_SELL_HINT_MASK = 8;
        public const int ITEM_NEVER_DROP_WHEN_DEAD_MASK = 16;
        public const int ITEM_SELL_DISABLE_MASK = 32;
        public const int ITEM_STATUS_NONE = 0;
        public const int ITEM_STATUS_NOT_IDENT = 1;
        public const int ITEM_STATUS_CANNOT_REPAIR = 2;
        public const int ITEM_STATUS_NEVER_DAMAGE = 4;
        public const int ITEM_STATUS_MAGIC_ADD = 8;

        //
        public const uint TYPE_DRAGONBALL = 1088000;
        public const uint TYPE_METEOR = 1088001;
        public const uint TYPE_METEORTEAR = 1088002;
        public const uint TYPE_TOUGHDRILL = 1200005;

        public const uint TYPE_STARDRILL = 1200006;

        //
        public const uint TYPE_DRAGONBALL_SCROLL = 720028; // Amount 10
        public const uint TYPE_METEOR_SCROLL = 720027; // Amount 10

        public const uint TYPE_METEORTEAR_PACK = 723711; // Amount 5

        //
        public const uint TYPE_STONE1 = 730001;
        public const uint TYPE_STONE2 = 730002;
        public const uint TYPE_STONE3 = 730003;
        public const uint TYPE_STONE4 = 730004;
        public const uint TYPE_STONE5 = 730005;
        public const uint TYPE_STONE6 = 730006;
        public const uint TYPE_STONE7 = 730007;

        public const uint TYPE_STONE8 = 730008;

        //
        public const uint TYPE_MOUNT_ID = 300000;

        //
        public const uint TYPE_EXP_BALL = 723700;
        public const uint TYPE_EXP_POTION = 723017;

        public static readonly int[] BowmanArrows =
        {
            1050000, 1050001, 1050002, 1050020, 1050021, 1050022, 1050023, 1050030, 1050031, 1050032, 1050033, 1050040,
            1050041, 1050042, 1050043, 1050050, 1050051, 1050052
        };

        public const uint IRON_ORE = 1072010;
        public const uint COPPER_ORE = 1072020;
        public const uint EUXINITE_ORE = 1072031;
        public const uint SILVER_ORE = 1072040;
        public const uint GOLD_ORE = 1072050;

        public const uint OBLIVION_DEW = 711083;
        public const uint MEMORY_AGATE = 720828;

        public const uint PERMANENT_STONE = 723694;
        public const uint BIGPERMANENT_STONE = 723695;

        public const int LOTTERY_TICKET = 710212;
        public const uint SMALL_LOTTERY_TICKET = 711504;

        public const uint TYPE_JAR = 750000;

        public const uint SASH_SMALL = 1100003;
        public const uint SASH_MEDIUM = 1100006;
        public const uint SASH_LARGE = 1100009;

        public const uint PROTECTION_PILL = 3002029;
        public const uint SUPER_PROTECTION_PILL = 3002030;

        public const uint FREE_TRAINING_PILL = 3002926;
        public const uint FAVORED_TRAINING_PILL = 3003124;
        public const uint SPECIAL_TRAINING_PILL = 3003125;
        public const uint SENIOR_TRAINING_PILL = 3003126;
    }
}
