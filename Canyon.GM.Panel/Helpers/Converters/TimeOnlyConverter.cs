﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Globalization;

namespace Canyon.GM.Panel.Helpers.Converters
{
    public class TimeOnlyConverter : ValueConverter<TimeOnly, DateTime>
    {
        public TimeOnlyConverter()
            : base(
                  timeOnly => DateTime.ParseExact(timeOnly.ToString("HH:mm:ss"), "HH:mm:ss", CultureInfo.InvariantCulture),
                  dateTime => TimeOnly.FromDateTime(dateTime)
                  )
        {            
        }
    }
}
